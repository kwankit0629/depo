import React from "react";
import "./CreateJobForm.css";
import "../../css/bootstrap.min.css";
import "../../css/responsive.css";
import "../../css/style.css";
import ReactDOM from "react-dom";
import { useForm, SubmitHandler } from "react-hook-form";
import { createClientJobThunk } from "../../redux/clientJobs/thunk";
import { useSelector, useDispatch } from "react-redux";
import { Form } from "react-bootstrap";
import { IRootState } from "../../redux/store";
import { useHistory } from "react-router";
import Swal from "sweetalert2";

interface ClientJobFormState {
  user_id: number;
  phone_num: string;
  job_name: string;
  job_intro: string;
  job_requirement: string;
  budget: number;
  deadline: number;
}

export default function CreateJobForm() {
  const dispatch = useDispatch();
  const { register, handleSubmit, reset } = useForm<ClientJobFormState>();

  const history = useHistory();

  const routeChange = () => {
    let path = `clientJobs`;
    history.push(path);
  };

  const onSubmit = (data: ClientJobFormState) => {
    dispatch(
      createClientJobThunk(
        user_id,
        data.phone_num,
        data.job_name,
        data.job_intro,
        data.job_requirement,
        data.budget,
        data.deadline
      )
    );

    reset();
    routeChange();
  };

  const user_id: number = useSelector(
    (state: IRootState) => state.auth.user.id
  );

  const jobPhotoHandler = async (e: any) => {
    const jobPhoto = new FormData();
    jobPhoto.append("jobPhoto", e.target.files[0]);
    // await Server.updateJobPhotoHandler(auth.user.id, jobPhoto);
  };

  return (
    <section className="ptb-20">
      <div className="container">
        <>
          <div>
            <div className="checkout-area-bg bg-white">
              <div className="check-heading">
                <h3>Create Your Job Here</h3>
                <h5>Fields marked with an asterisk (*) are required.</h5>
              </div>
              <div className="check-out-form">
                <form onSubmit={handleSubmit(onSubmit)}>
                  <div className="row">
                    <div className="col-lg-6 col-md-12 col-sm-=12 col-12">
                      <div className="form-group">
                        <label htmlFor="fname">
                          Job Name<span className="text-danger">*</span>{" "}
                        </label>
                        <input
                          {...register("job_name", { required: true })}
                          type="text"
                          className="form-control"
                          id="fname"
                          placeholder="Job Name"
                        />
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-12 col-sm-=12 col-12">
                      <div className="form-group">
                        <label htmlFor="fname">
                          Contact Number<span className="text-danger">*</span>{" "}
                        </label>
                        <input
                          {...register("phone_num", { required: true })}
                          type="number"
                          className="form-control"
                          id="fname"
                          placeholder=""
                        />
                      </div>
                    </div>

                    <div className="col-lg-6 col-md-12 col-sm-=12 col-12">
                      <div className="form-group">
                        <label htmlFor="fname">
                          Job Period (days)
                          <span className="text-danger">*</span>{" "}
                        </label>
                        <input
                          {...register("deadline", { required: true })}
                          type="text"
                          className="form-control"
                          id="fname"
                          placeholder=""
                        />
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-12 col-sm-=12 col-12">
                      <div className="form-group">
                        <label htmlFor="fname">
                          Budget (HKD)<span className="text-danger">*</span>{" "}
                        </label>
                        <input
                          {...register("budget", { required: true })}
                          type="text"
                          className="form-control"
                          id="fname"
                          placeholder=""
                        />
                      </div>
                    </div>
                    <div className="col-lg-12 col-md-12 col-sm-=12 col-12">
                      <div className="form-group">
                        <label htmlFor="messages">
                          Job Introduction<span className="text-danger">*</span>
                        </label>
                        <textarea
                          {...register("job_intro", { required: true })}
                          className="form-control"
                          id="messages"
                          placeholder="Job Introduction"
                        ></textarea>
                      </div>
                    </div>
                    <div className="col-lg-12 col-md-12 col-sm-=12 col-12">
                      <div className="form-group">
                        <label htmlFor="messages">
                          Job Requirement<span className="text-danger">*</span>
                        </label>
                        <textarea
                          {...register("job_requirement", { required: true })}
                          className="form-control"
                          id="messages"
                          placeholder="Job Requirement"
                        ></textarea>
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-12 col-sm-=12 col-12">
                      <div className="form-group">
                        <label htmlFor="fname">
                          {/* Photo For Your Job */}
                          <span className="text-danger"></span>{" "}
                        </label>
                        {/* <input  {...register("jobName", { required: true})} type="text" className="form-control" id="fname"
                                            placeholder="easy job" /> */}
                        {/* <input
                          type="file"
                          accept=".jpg, .jpeg, .png"
                          id="inputAvatar"
                          onChange={jobPhotoHandler}
                          className="hidden"
                        /> */}
                      </div>
                    </div>
                  </div>
                  <input
                    type="submit"
                    onClick={() => {
                      handleSubmit(onSubmit);
                      Swal.fire({
                        position: "top-start",
                        icon: "success",
                        title: "Your job has been created!",
                        showConfirmButton: false,
                        timer: 2000,
                      });
                    }}
                  />
                </form>
              </div>
            </div>
          </div>
        </>

        {/* <form onSubmit={handleSubmit(onSubmit)}>
        
        <label>Phone</label>
        <input {...register("phone")} />
        <br/>
        <label>Introduction</label>
        <textarea {...register("introduction")} />
        <br/>
        <label>Work Experience</label>
        <textarea {...register("WorkExperience")} />
        <br/>
        <label>Technology Summary</label>
        <textarea {...register("technologySummary")} />
        <br/>
        <input type="submit" />
        </form> */}
      </div>
    </section>
  );
}
