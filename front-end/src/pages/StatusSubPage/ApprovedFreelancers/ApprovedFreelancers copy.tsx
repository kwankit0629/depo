import { push } from "connected-react-router";
import React, { useEffect } from "react";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { OwnClientJobsState } from "../../../redux/clientJobs/state";
import { getOwnClientJobsThunk } from "../../../redux/clientJobs/thunk";
import { IRootState } from "../../../redux/store";
import { useParams } from "react-router-dom";
import { getOwnClientJobsApplicantsThunk } from "../../../redux/clientJobs/thunk";

export default function ApprovedFreelancers() {
  // INSERT DATA
  const dispatch = useDispatch();

  const user_id: number = useSelector(
    (state: IRootState) => state.auth.user.id
  );

  useEffect(() => {
    if (user_id) {
      console.log("user_id = , user_id", user_id);
      dispatch(getOwnClientJobsThunk(user_id));
    }
    return () => {
      console.log("gone");
    };
  }, [dispatch, user_id]);

  const ownJobs: OwnClientJobsState[] = useSelector(
    (state: IRootState) => state.clientJob.OwnClientJobs
  );
  // SHOW AND HIDE
  const [show, setShow] = useState(true);
  const [showJob, setShowJob] = useState(true);
  const [activeTab, setActiveTab] = useState("");

  //SubApprovedFreelancers
  const jobId = parseInt(useParams<{ id: string }>().id);
  useEffect(() => {
    console.log("Getting Applicants!!!", jobId);
    dispatch(getOwnClientJobsApplicantsThunk(jobId));
  }, []);
  const applicants = useSelector(
    (state: IRootState) => state.clientJob.JobsApplicants
  );
  return (
    <>
      <div
        style={{ justifyContent: "start", display: "flex", padding: "30px" }}
      >
        <div className="row">
          <div className="blog_text col-lg-12">
            <div className="row">
              <div
                onClick={() => setShowJob(!showJob)}
                className="button_dropdown col"
                style={{ marginRight: "10px" }}
              >
                <div className="row">
                  <div>This is the first Job You Created</div>
                  <div>
                    {ownJobs.map((job) => {
                      return (
                        <button
                          key={job.id}
                          onClick={() => {
                            dispatch(
                              push(`/client-jobs-application/${job.id}`)
                            );
                          }}
                        >
                          <div>Job Name: {job.job_name}</div>
                          <div>Deadline: {job.deadline} days</div>
                          <div>Budget: $ {job.budget}</div>
                          <div>Requirement: {job.job_requirement}</div>
                        </button>
                      );
                    })}
                  </div>
                  <div className="col">
                    <i className={"fa fa-arrow-down"}></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* Jobs show dropdown */}
      <div>
        {showJob ? null : (
          <>
            <div
              style={{
                justifyContent: "start",
                display: "flex",
                padding: "30px",
              }}
            >
              <div className="row">
                <div className="col-lg-3">
                  <div className="img_profiles  ">
                    {/* <img src={img1} alt="img" /> */}
                    <img alt="img" />
                  </div>
                </div>

                <div className="blog_text col-lg-9">
                  <div className="row">
                    <div
                      className={
                        activeTab === "approved"
                          ? "active button_click col"
                          : "button_click col"
                      }
                      style={{ marginRight: "10px" }}
                      onClick={() => {
                        setActiveTab("approved");
                      }}
                    >
                      Approved
                    </div>
                    <div
                      className={
                        activeTab === "deny"
                          ? "active button_click col"
                          : "button_click col"
                      }
                      style={{ marginRight: "10px" }}
                      onClick={() => {
                        setActiveTab("deny");
                      }}
                    >
                      Reject
                    </div>
                    <div
                      onClick={() => setShow(!show)}
                      className="button_dropdown col"
                      style={{ marginRight: "10px" }}
                    >
                      <div className="row">
                        <div>freelancer_username_pj</div>
                        <div className="col">
                          <i className={"fa fa-arrow-down"}></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* Freelancers show dropdown */}
            <div>
              {show ? null : (
                <div>
                  {applicants.map((applicant, n) => {
                    return (
                      <div key={n}>
                        <>
                          {/* FREELANCER BASIC INFO */}
                          <div
                            className="container"
                            style={{ backgroundColor: "#C4C4C4" }}
                          >
                            FREELANCER BASIC INFORMATION
                          </div>
                          <div>
                            <div
                              style={{
                                justifyContent: "flex-start",
                                display: "flex",
                                textAlign: "left",
                                backgroundColor: "#F5F4F4",
                                padding: "15px",
                              }}
                            >
                              <p>
                                Name: {applicant.freelancer_name}
                                <br />
                                <br />
                                Age: {applicant.freelancer_age}
                                <br />
                                <br />
                                Country: {applicant.freelancer_country}
                                <br />
                                <br />
                                Phone Number: {applicant.freelancer_phone}
                                <br />
                                <br />
                                Email: {applicant.freelancer_email}
                              </p>
                            </div>
                          </div>
                          <br />

                          {/* INTRODUCTION */}
                          <div
                            className="container"
                            style={{ backgroundColor: "#C4C4C4" }}
                          >
                            INTRODUCTION
                          </div>
                          <div>
                            <div
                              style={{
                                justifyContent: "flex-start",
                                display: "flex",
                                textAlign: "left",
                                backgroundColor: "#F5F4F4",
                                padding: "15px",
                              }}
                            >
                              <p>
                                Introduction:{" "}
                                {applicant.freelancer_introduction}
                              </p>
                            </div>
                          </div>
                          <br />

                          {/* EXPERIENCE */}
                          <div
                            className="container"
                            style={{ backgroundColor: "#C4C4C4" }}
                          >
                            EXPERIENCE
                          </div>
                          <div>
                            <div
                              style={{
                                justifyContent: "flex-start",
                                display: "flex",
                                textAlign: "left",
                                backgroundColor: "#F5F4F4",
                                padding: "15px",
                              }}
                            >
                              <p>
                                Experience: {applicant.freelancer_experience}
                              </p>
                            </div>
                          </div>
                          <br />

                          {/* TECHNOLOGY SUMMARY */}
                          <div
                            className="container"
                            style={{ backgroundColor: "#C4C4C4" }}
                          >
                            TECHNOLOGY SUMMARY
                          </div>
                          <div>
                            <div
                              style={{
                                justifyContent: "flex-start",
                                display: "flex",
                                textAlign: "left",
                                backgroundColor: "#F5F4F4",
                                padding: "15px",
                              }}
                            >
                              <p>
                                Technology Summary:{" "}
                                {applicant.freelancer_technology_sum}
                              </p>
                            </div>
                          </div>
                          <br />

                          {/* REFERENCE LINK */}
                          <div
                            className="container"
                            style={{ backgroundColor: "#C4C4C4" }}
                          >
                            REFERENCE LINK
                          </div>
                          <div>
                            <div
                              style={{
                                justifyContent: "flex-start",
                                display: "flex",
                                textAlign: "left",
                                backgroundColor: "#F5F4F4",
                                padding: "15px",
                              }}
                            >
                              <p>
                                Reference Link: {applicant.freelancer_ref_link}
                              </p>
                            </div>
                          </div>
                          <br />
                        </>
                      </div>
                    );
                  })}
                </div>
              )}
            </div>
          </>
        )}
      </div>
    </>
  );
}
