import { push } from "connected-react-router";
import React, { useEffect } from "react";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { OwnClientJobsState } from "../../../redux/clientJobs/state";
import { getOwnClientJobsThunk } from "../../../redux/clientJobs/thunk";
import { IRootState } from "../../../redux/store";
import { Link, useParams } from "react-router-dom";
import { getOwnClientJobsApplicantsThunk } from "../../../redux/clientJobs/thunk";
// import "./ApprovedFreelancer.scss"

export default function ApprovedFreelancers() {
  // INSERT DATA
  const dispatch = useDispatch();

  const user_id: number = useSelector(
    (state: IRootState) => state.auth.user.id
  );

  useEffect(() => {
    if (user_id) {
      console.log("user_id = , user_id", user_id);
      dispatch(getOwnClientJobsThunk(user_id));
    }
    return () => {
      console.log("gone");
    };
  }, [dispatch, user_id]);

  const ownJobs: OwnClientJobsState[] = useSelector(
    (state: IRootState) => state.clientJob.OwnClientJobs
  );
  // SHOW AND HIDE
  const [show, setShow] = useState(true);
  const [showJob, setShowJob] = useState(true);
  const [activeTab, setActiveTab] = useState("");

  //SubApprovedFreelancers
  const jobId = parseInt(useParams<{ id: string }>().id);
  useEffect(() => {
    console.log("Getting Applicants!!!", jobId);
    dispatch(getOwnClientJobsApplicantsThunk(jobId));
  }, []);
  const applicants = useSelector(
    (state: IRootState) => state.clientJob.JobsApplicants
  );
  return (
    <>
   <section id="blog_grid_area_one" className="ptb-20">
     
   <div className="container">
  
  <div className="row">
{ownJobs.map((job) => {
                  return (
                    <div
                    key={job.id}
                    className="col col-lg-4 col-md-4 col-sm-6 col-12"
                      // onClick={() => {
                      //   dispatch(
                      //     push(`/client-jobs-application/${job.id}`)
                      //   );
                      // }}
                    >
                      <div
                className="blog_one_item "
                style={{ "backgroundColor": "#EFEFEF" ,"margin":"5px","padding":"30px","height":"500px","position":"relative","marginBottom":"30px" }}
                >
                  
                    
                        <div>
                          <div style={{"fontWeight":"bold","display":"inline"}}>Job Name: </div>
                          <div>
                          {job.job_name}
                          </div>
                        </div>
                          <br/>
                        <div>
                          <div style={{"fontWeight":"bold","display":"inline"}}>Deadline: </div>
                          <div>{job.deadline} days</div>
                        </div>
                          <br/>
                        <div>
                          <div style={{"fontWeight":"bold","display":"inline"}}>Budget: </div>
                          <div>$ {job.budget}</div>
                          </div>
                          <br/>
                        <div>
                          <div style={{"fontWeight":"bold","display":"inline"}}>Requirement: </div>
                          <div>{job.job_requirement}</div>
                        </div>
                        <div className="blog_text"
                        style={{ "position":"absolute","bottom":"20px","left":"32px" }}>
                        
                        <Link to={`/client-jobs-application/${job.id}`} className="button" style={{ "textAlign": "center" }}>Check Application Status</Link>
                      
                    </div>

                    



                  </div>

                      
                      
                          

                          
                      
                      

                    </div>
                  );
                })}
   
</div>


</div>
     
     </section> 



      {/* Jobs show dropdown */}
      
    </>
  );
}


