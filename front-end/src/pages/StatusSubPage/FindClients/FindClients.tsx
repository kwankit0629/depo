import React, { useEffect } from "react";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  deleteAppliedClientJobsThunk,
  getOwnAppliedClientJobsInfoThunk,
} from "../../../redux/clientJobs/thunk";
import { IRootState } from "../../../redux/store";

export default function FindClients() {
  const dispatch = useDispatch();

  const freelancerId = useSelector(
    // (state: IRootState) => state.auth.freelancer
    (state: IRootState) => state.auth.freelancer.id
  );

  useEffect(() => {
    dispatch(getOwnAppliedClientJobsInfoThunk(freelancerId));
  }, [freelancerId]);

  const onSubmit = (job: any) => {
    const jobId = job.client_job_id;
    dispatch(deleteAppliedClientJobsThunk(jobId, freelancerId));
  };

  const appliedJobs = useSelector(
    (state: IRootState) => state.clientJob.AppliedJobs
  );
  return (
    <>
      <section id="blog_grid_area_one" className="ptb-20">
        <div className="container">
          <div className="row">
            {appliedJobs.map((job) => {
              return (
                <div
                  key={job.client_job_id}
                  className="col col-lg-4 col-md-4 col-sm-6 col-12"
                  // onClick={() => {
                  //   dispatch(
                  //     push(`/client-jobs-application/${job.id}`)
                  //   );
                  // }}
                >
                  <div
                    className="blog_one_item "
                    style={{
                      backgroundColor: "#EFEFEF",
                      margin: "5px",
                      padding: "30px",
                      height: "500px",
                      position: "relative",
                      marginBottom: "30px",
                    }}
                  >
                    <div>
                      <div style={{ fontWeight: "bold", display: "inline" }}>
                        Project Name:{" "}
                      </div>
                      {job.client_job_name}
                    </div>
                    <br />
                    <div>
                      <div style={{ fontWeight: "bold", display: "inline" }}>
                        Project Intro:{" "}
                      </div>
                      {job.client_job_intro}
                    </div>
                    <br />
                    <div>
                      <div style={{ fontWeight: "bold", display: "inline" }}>
                        Project Requirement:{" "}
                      </div>
                      {job.client_job_requirement}
                    </div>
                    <br />
                    <div>
                      <div style={{ fontWeight: "bold", display: "inline" }}>
                        Budget:{" "}
                      </div>
                      $ {job.client_job_budget}
                    </div>
                    <br />
                    <div>
                      <div style={{ fontWeight: "bold", display: "inline" }}>
                        Deadline:{" "}
                      </div>
                      {job.client_job_deadline} Days
                    </div>
                    <br />
                    <a
                      style={{
                        boxSizing: "border-box",

                        justifyContent: "center",
                        fontWeight: "bold",
                      }}
                      className="a:hover"
                    >
                      Application status:{" "}
                      <span
                        style={{
                          color: "red",
                        }}
                      >
                        {job.application_status}
                      </span>
                    </a>
                    <br />
                    <br />
                    <div className="blog_text">
                      <a
                        className="button"
                        style={{
                          border: "solid grey 1px",
                          position: "absolute",
                          bottom: "20px",
                        }}
                        onClick={() => {
                          console.log("deleting assignee");
                          onSubmit(job);
                          alert(
                            `You have deleted ${job.client_job_name} application!`
                          );
                        }}
                      >
                        Cancel Application
                      </a>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </section>

      {/* Jobs show dropdown */}
    </>
  );
}
