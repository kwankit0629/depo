import React from "react";
import {useState} from 'react'

export default function FindJobs() {
  const [show,setShow]=useState(true)
  const [activeTab , setActiveTab] = useState('')
  return (
    <>
    <div style={{"justifyContent":"start","display":"flex","padding":"30px"}}>
    <div className="row">

      <div className="col-lg-2">
        <div className="img_profiles  " >
        {/* <img src={img1} alt="img" /> */}
        <img  alt="img" />
        </div>
      </div>

      <div className="blog_text col-lg-10">
        <div className="row">
          

            <div className={activeTab  === 'pending' ? "active button_click col" : "button_click col"} style={{"marginRight":"10px"}} onClick={()=>{setActiveTab('pending')}}>Pending</div>
            <div className={activeTab  === 'success' ? "active button_click col" : "button_click col"} style={{"marginRight":"10px"}} onClick={()=>{setActiveTab('success')}}>Success</div>
            <div className={activeTab  === 'cancelRequest' ? "active button_click col" : "button_click col"} style={{"marginRight":"10px"}} onClick={()=>{setActiveTab('cancelRequest')}}>Cancel Request</div>
            <div className={activeTab  === 'canceledByFreelancer' ? "active button_click col" : "button_click col"} style={{"marginRight":"10px"}} onClick={()=>{setActiveTab('canceledByFreelancer')}}>Canceled By Freelancer</div>
            <div onClick={()=>setShow(!show)} className="button_dropdown col" style={{"marginRight":"10px"}} >
              <div className="row">
                  <div >freelancer_username_pj</div>
                  <div className="col"><i className={'fa fa-arrow-down'}></i></div>
              </div>
            </div>


        </div>
      </div>
      
    </div>
  
    </div>

    {/* show dropdown */}
    <div>
    {
       show?null:
       <div>
         <>
         {/* WORK EXPERIENCE */}
         <div className="container" style={{"backgroundColor":"#C4C4C4"}}>WORK EXPERIENCE</div>
         <div>                       
            <div  style={{"justifyContent":"flex-start","display":"flex","textAlign":"left","backgroundColor":"#F5F4F4","padding":"15px"}}>
                <p>
                    ABC Company - Develop, maintain and support application programs for adminstrative
                    <br/>
                    <br/>
                    DEF Company - analysize database information via DBmongo
                    <br/>
                    <br/>
                    GHI Company - Dog Nose Identify Application
                </p>
            </div>
          </div>
          <br/>
            
         {/* TECHNOLOGY SUMMARY */}
         <div className="container" style={{"backgroundColor":"#C4C4C4"}}>TECHNOLOGY SUMMARY</div>
         <div>                       
            <div  style={{"justifyContent":"flex-start","display":"flex","textAlign":"left","backgroundColor":"#F5F4F4","padding":"15px"}}>
                <p>
                    SKILL A - 1111111111
                    <br/>
                    <br/>
                    SKILL B - 2222222222
                    <br/>
                    <br/>
                    SKILL C - 3333333333
                </p>
            </div>
          </div>
          <br/>
            
         {/* JOB PLAN */}
         <div className="container" style={{"backgroundColor":"#C4C4C4"}}>JOB PLAN</div>
         <div>                       
            <div  style={{"justifyContent":"flex-start","display":"flex","textAlign":"left","backgroundColor":"#F5F4F4","padding":"15px"}}>
                <p>
                    PERIOD - 10DAYS
                </p>
            </div>
          </div>
          <br/>
            
         {/* JOB BUDGET */}
         <div className="container" style={{"backgroundColor":"#C4C4C4"}}>JOB BUDGET</div>
         <div>                       
            <div  style={{"justifyContent":"flex-start","display":"flex","textAlign":"left","backgroundColor":"#F5F4F4","padding":"15px"}}>
                <p>
                    BUDGET - $100
                    
                </p>
            </div>
          </div>
          <br/>
            
         
          </>
       </div>
     }
     </div>
    </>
  )
}