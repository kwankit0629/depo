import React from "react";
import {useState} from 'react'


export default function ApprovedClients() {
  const [show,setShow]=useState(true)
  const [activeTab , setActiveTab] = useState('')
  return (
    <>
    <div style={{"justifyContent":"start","display":"flex","padding":"30px"}}>
    <div className="row">

      <div className="col-lg-3">
        <div className="img_profiles  " >
        {/* <img src={img1} alt="img" /> */}
        <img  alt="img" />
        </div>
      </div>

      <div className="blog_text col-lg-9">
        <div className="row">
          

            <div className={activeTab  === 'approved' ? "active button_click col" : "button_click col"} style={{"marginRight":"10px"}} onClick={()=>{setActiveTab('approved')}}>Approved</div>
            <div className={activeTab  === 'deny' ? "active button_click col" : "button_click col"} style={{"marginRight":"10px"}} onClick={()=>{setActiveTab('deny')}}>Deny</div>
            <div onClick={()=>setShow(!show)} className="button_dropdown col" style={{"marginRight":"10px"}} >
              <div className="row">
                  <div >client_username_pj</div>
                  <div className="col"><i className={'fa fa-arrow-down'}></i></div>
              </div>
            </div>


        </div>
      </div>
      
    </div>
  
    </div>

    {/* show dropdown */}
    <div>
    {
       show?null:
       <div>
         <>
         {/* JOB NAME */}
         <div className="container" style={{"backgroundColor":"#C4C4C4"}}>JOB NAME</div>
         <div>                       
            <div  style={{"justifyContent":"flex-start","display":"flex","textAlign":"left","backgroundColor":"#F5F4F4","padding":"15px"}}>
                <p>
                    ABC PROJECT
                </p>
            </div>
          </div>
          <br/>

         {/* JOB INTRODUCTION */}
         <div className="container" style={{"backgroundColor":"#C4C4C4"}}>JOB INTRODUCTION</div>
         <div>                       
            <div  style={{"justifyContent":"flex-start","display":"flex","textAlign":"left","backgroundColor":"#F5F4F4","padding":"15px"}}>
                <p>
                    I want to start a online business and want to create a website which I can create post and see the response of the client. Also, I also want to a website design like HKTV Mall.
                </p>
            </div>
          </div>
          <br/>
            
         {/* JOB REQUIREMENT */}
         <div className="container" style={{"backgroundColor":"#C4C4C4"}}>JOB REQUIREMENT</div>
         <div>                       
            <div  style={{"justifyContent":"flex-start","display":"flex","textAlign":"left","backgroundColor":"#F5F4F4","padding":"15px"}}>
                <p>
                    1 - Web Design
                    <br/>
                    2 - I can make post in the front end
                    <br/>
                    3 - I can have a dashboard to show the selling condition or data
                    <br/>
                    4 - Custom can make massage or comment in the front end
                </p>
            </div>
          </div>
          <br/>
            
         {/* JOB PERIOD */}
         <div className="container" style={{"backgroundColor":"#C4C4C4"}}>JOB PERIOD</div>
         <div>                       
            <div  style={{"justifyContent":"flex-start","display":"flex","textAlign":"left","backgroundColor":"#F5F4F4","padding":"15px"}}>
                <p>
                    PERIOD - 30DAYS
                </p>
            </div>
          </div>
          <br/>
            
         {/* JOB BUDGET */}
         <div className="container" style={{"backgroundColor":"#C4C4C4"}}>JOB BUDGET</div>
         <div>                       
            <div  style={{"justifyContent":"flex-start","display":"flex","textAlign":"left","backgroundColor":"#F5F4F4","padding":"15px"}}>
                <p>
                    BUDGET - $20000
                    
                </p>
            </div>
          </div>
          <br/>
            
         
          </>
       </div>
     }
     </div>
    </>
  )
}