import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useParams } from "react-router-dom";
import {
  deleteClientJobsAssigneeThunk,
  getOwnClientJobsApplicantsThunk,
  rejectOtherClientJobsAssigneeThunk,
  updateClientJobsAssigneeThunk,
  updateClientJobsStatusThunk,
} from "../../../redux/clientJobs/thunk";
import { IRootState } from "../../../redux/store";
import { useState } from "react";

export default function SubApprovedFreelancers() {
  const dispatch = useDispatch();
  const jobId = parseInt(useParams<{ id: string }>().id);
  useEffect(() => {
    console.log("Getting Applicants!!!", jobId);
    dispatch(getOwnClientJobsApplicantsThunk(jobId));
  }, []);
  const applicants = useSelector(
    (state: IRootState) => state.clientJob.JobsApplicants
  );

  // button
  const [activeTab, setActiveTab] = useState("");

  const onSubmit = (applicant: any) => {
    const freelancer_id = applicant.freelancer_id;
    dispatch(updateClientJobsAssigneeThunk(jobId, freelancer_id));
    dispatch(updateClientJobsStatusThunk(jobId));
    dispatch(rejectOtherClientJobsAssigneeThunk(jobId, freelancer_id));
  };

  const onSubmit2 = (applicant: any) => {
    const freelancer_id = applicant.freelancer_id;
    dispatch(deleteClientJobsAssigneeThunk(jobId, freelancer_id));
  };
  return (
    <div>
      <div>
        {applicants.map((applicant, n) => {
          return (
            <div className="container" style={{ paddingBottom: "25px" }}>
              <div key={n}>
                <>
                  {/* FREELANCER BASIC INFO */}
                  <div
                    className="container"
                    style={{ backgroundColor: "#C4C4C4" }}
                  >
                    FREELANCER BASIC INFORMATION
                  </div>
                  <div>
                    <div
                      style={{
                        justifyContent: "flex-start",
                        display: "flex",
                        textAlign: "left",
                        backgroundColor: "#F5F4F4",
                        padding: "15px",
                      }}
                    >
                      <p>
                        Name: {applicant.freelancer_name}
                        <br />
                        <br />
                        Age: {applicant.freelancer_age}
                        <br />
                        <br />
                        Country: {applicant.freelancer_country}
                        <br />
                        <br />
                        Phone Number: {applicant.freelancer_phone}
                        <br />
                        <br />
                        Email: {applicant.freelancer_email}
                      </p>
                    </div>
                  </div>
                  <br />

                  {/* INTRODUCTION */}
                  <div
                    className="container"
                    style={{ backgroundColor: "#C4C4C4" }}
                  >
                    INTRODUCTION
                  </div>
                  <div>
                    <div
                      style={{
                        justifyContent: "flex-start",
                        display: "flex",
                        textAlign: "left",
                        backgroundColor: "#F5F4F4",
                        padding: "15px",
                      }}
                    >
                      <p>Introduction: {applicant.freelancer_introduction}</p>
                    </div>
                  </div>
                  <br />

                  {/* EXPERIENCE */}
                  <div
                    className="container"
                    style={{ backgroundColor: "#C4C4C4" }}
                  >
                    EXPERIENCE
                  </div>
                  <div>
                    <div
                      style={{
                        justifyContent: "flex-start",
                        display: "flex",
                        textAlign: "left",
                        backgroundColor: "#F5F4F4",
                        padding: "15px",
                      }}
                    >
                      <p>Experience: {applicant.freelancer_experience}</p>
                    </div>
                  </div>
                  <br />

                  {/* TECHNOLOGY SUMMARY */}
                  <div
                    className="container"
                    style={{ backgroundColor: "#C4C4C4" }}
                  >
                    TECHNOLOGY SUMMARY
                  </div>
                  <div>
                    <div
                      style={{
                        justifyContent: "flex-start",
                        display: "flex",
                        textAlign: "left",
                        backgroundColor: "#F5F4F4",
                        padding: "15px",
                      }}
                    >
                      <p>
                        Technology Summary:{" "}
                        {applicant.freelancer_technology_sum}
                      </p>
                    </div>
                  </div>
                  <br />

                  {/* REFERENCE LINK */}
                  <div
                    className="container"
                    style={{ backgroundColor: "#C4C4C4" }}
                  >
                    REFERENCE LINK
                  </div>
                  <div>
                    <div
                      style={{
                        justifyContent: "flex-start",
                        display: "flex",
                        textAlign: "left",
                        backgroundColor: "#F5F4F4",
                        padding: "15px",
                      }}
                    >
                      <p>Reference Link: {applicant.freelancer_ref_link}</p>
                    </div>
                  </div>
                  <br />
                </>
              </div>
                <div className="row">
              <div className="col-lg-2">

                  <div className="blog_text">
                    <div className="button">
                    <div
                    // className={
                    //   activeTab === "approve"
                    //     ? "active button_click col"
                    //     : "button_click col"
                    // }
                    style={{ marginRight: "10px" }}
                    onClick={() => {
                      console.log("confirming job");
                      onSubmit(applicant);
                      alert(
                        `You have selected ${applicant.freelancer_name} to do your job!`
                      );
                    }}
                  >
                    Confirm
                  </div>
                      </div>
                    </div>
              </div>  

              <div className="col-lg-2">
                  <div className="blog_text">
                    <div className="button">
                  <div
                    // className={
                      
                    //   activeTab === "deny"
                    //     ? "active button_click col"
                    //     : "button_click col"
                    // }
                    onClick={() => {
                      console.log("deleting assignee");
                      onSubmit2(applicant);
                      alert(
                        `You have rejected ${applicant.freelancer_name} from your job!`
                      );
                    }}
                  >
                    Reject
                  </div>
                  
                </div>
                      </div>
              </div>
              
              <div className="col-lg-2">
                  <div className="blog_text">
                    <div className="button">
                    
                  <Link to="/status" style={{ color: "black",  }}>
                  Back
              
            </Link>                  
                  
                  
                </div>
                      </div>
              </div>
                    

              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}
