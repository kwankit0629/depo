import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { IRootState } from "../../redux/store";
import { useEffect } from "react";
import { useParams } from "react-router-dom";
import { getFreelancerPostByIdThunk } from "../../redux/job/thunk";
import { Button, Card, Col, Container, Nav, Row } from "react-bootstrap";
import TimeAgo from "react-timeago";
import { Link } from "react-router-dom";
import "./FreelancerJobsDatail.css"
export default function FreelancerJobsDatail() {
  return (
    <div>
    {/* {freelancerPost.map((post) => {
        return ( */}
            <div>
                <Container>
                    <Row className="justify-content-md-center">
                        <Col sm={3}>
                            <div className="card-container">
                                {/* <img className="round" src={`http://localhost:8080/upload/${post.photo}`} alt="user" /> */}
                                {/* <h3><Link to={`/freelancerprofile/${post.user_id}`} style={{ color: "white", textDecoration: "none" }}>{post.name}</Link></h3> */}
                                <h3>post.name</h3>
                                <h6>post.country</h6>
                                <div className="card-content">Email:post.email</div>
                                <div className="card-content">Experience:post.experience</div>
                                {/* <div className="card-content">Created account:<TimeAgo date=post.createAccount></TimeAgo></div> */}
                                <div className="card-content">Created account:post.createAccount</div>
                                <div className="buttons">
                                </div>

                            </div>
                            <div className="small-card-container">
                                <h5>Introduction</h5>
                                <span>post.introduction</span>
                            </div>

                            <div className="small-card-container">
                                <h5>Contact</h5>
                                <span>post.phone_num</span>
                            </div>
                            {/* <span className="bigBox">
                            <span className="planBox">
                                <div>Basic Plan:{post.introduction}</div>
                                <div>Fee {post.project_fee}HKD</div>
                            </span>
                        </span> */}
                        </Col>
                        <Col sm={5}>
                            <div className="big-card-container">
                                {/* <img src={`http://localhost:8080/upload/${post.postsImage}`}></img> */}
                                <div>Experience:</div>
                                <div>post.experience</div>
                                <div>Technology Summary:</div>
                                <div>post.technology_sum</div>
                                <div>What services can prove : </div>
                                <div>post.content</div>
                                <Button>Confirm to Send Request to Client</Button>


                            </div>


                        </Col>
                    </Row>

                </Container>

            </div >
        {/* );
    })} */}
</div >
  );
}
