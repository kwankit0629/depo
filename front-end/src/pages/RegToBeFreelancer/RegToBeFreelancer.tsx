import React from "react";
import "./RegToBeFreelancer.css";
import "../../css/bootstrap.min.css";
import "../../css/responsive.css";
import "../../css/style.css";
import ReactDOM from "react-dom";
import { useForm, SubmitHandler } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { IRootState } from "../../redux/store";
import { regToBeAFreelancerThunk } from "../../redux/auth/thunks";
import { useHistory } from "react-router-dom";
// import { regToBeAFreelancerThunk } from "../../redux/auth/thunks";

interface RegisterFormState {
  age: number;
  email: string;
  phone_num: string;
  photo: string;
  country: string;
  experience: string;
  introduction: string;
  technology_sum: string;
  ref_link: string;
  user_id: number | string;
}

export default function RegToBeFreelancer() {
  const dispatch = useDispatch();
  const user_id: number = useSelector(
    (state: IRootState) => state.auth.user.id
  );

  const history = useHistory();

  const routeChange = () => {
    let path = `clientJobs`;
    history.push(path);
  };

  const { register, handleSubmit, reset } = useForm<RegisterFormState>();
  const onSubmit = (data: RegisterFormState) => {
    dispatch(
      regToBeAFreelancerThunk(
        data.age,
        data.email,
        data.phone_num,
        data.photo,
        data.country,
        data.experience,
        data.introduction,
        data.technology_sum,
        data.ref_link,
        user_id
      )
    );
    reset();
    routeChange();
  };

  return (
    <section className="ptb-20">
      <div className="container">
        <>
          <div>
            <div className="checkout-area-bg bg-white">
              <div className="check-heading">
                <h3>Register To Be Our Developer</h3>
              </div>
              <div className="check-out-form">
                <form onSubmit={handleSubmit(onSubmit)}>
                  <div className="row">
                    <div className="col-lg-6 col-md-12 col-sm-=12 col-12">
                      <div className="form-group">
                        <label htmlFor="fname">
                          Age<span className="text-danger">*</span>{" "}
                        </label>
                        <input
                          {...register("age", { required: true })}
                          type="text"
                          className="form-control"
                          id="fname"
                          placeholder=""
                        />
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-12 col-sm-=12 col-12">
                      <div className="form-group">
                        <label htmlFor="fname">
                          Email<span className="text-danger">*</span>{" "}
                        </label>
                        <input
                          {...register("email", { required: true })}
                          type="text"
                          className="form-control"
                          id="fname"
                          placeholder=""
                        />
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-12 col-sm-=12 col-12">
                      <div className="form-group">
                        <label htmlFor="fname">
                          Phone Number<span className="text-danger">*</span>{" "}
                        </label>
                        <input
                          {...register("phone_num", { required: true })}
                          type="text"
                          className="form-control"
                          id="fname"
                          placeholder="12345678"
                        />
                      </div>
                    </div>
                    <div className="col-lg-12 col-md-12 col-sm-=12 col-12">
                      <div className="form-group">
                        <label htmlFor="messages">
                          photo<span className="text-danger">*</span>
                        </label>
                        <input
                          {...register("photo", { required: true })}
                          type="file"
                        />
                      </div>
                    </div>
                    <div className="col-lg-12 col-md-12 col-sm-=12 col-12">
                      <div className="form-group">
                        <label htmlFor="messages">
                          Introduction<span className="text-danger">*</span>
                        </label>
                        <textarea
                          {...register("introduction", { required: true })}
                          className="form-control"
                          id="messages"
                          placeholder="Introduction"
                        ></textarea>
                      </div>
                    </div>
                    <div className="col-lg-12 col-md-12 col-sm-=12 col-12">
                      <div className="form-group">
                        <label htmlFor="messages">
                          Work Experience<span className="text-danger">*</span>
                        </label>
                        <textarea
                          {...register("experience", { required: true })}
                          className="form-control"
                          id="messages"
                          placeholder="Work Experience"
                        ></textarea>
                      </div>
                    </div>
                    <div className="col-lg-12 col-md-12 col-sm-=12 col-12">
                      <div className="form-group">
                        <label htmlFor="messages">
                          Technology Summary
                          <span className="text-danger">*</span>
                        </label>
                        <textarea
                          {...register("technology_sum", { required: true })}
                          className="form-control"
                          id="messages"
                          placeholder="Technology Summary"
                        ></textarea>
                      </div>
                    </div>
                    <div className="col-lg-12 col-md-12 col-sm-=12 col-12">
                      <div className="form-group">
                        <label htmlFor="messages">
                          Country
                          <span className="text-danger">*</span>
                        </label>
                        <textarea
                          {...register("country", { required: true })}
                          className="form-control"
                          id="messages"
                          placeholder="Country"
                        ></textarea>
                      </div>
                    </div>
                    <div className="col-lg-12 col-md-12 col-sm-=12 col-12">
                      <div className="form-group">
                        <label htmlFor="messages">
                          Git reference link
                          <span className="text-danger">*</span>
                        </label>
                        <textarea
                          {...register("ref_link", { required: true })}
                          className="form-control"
                          id="messages"
                          placeholder="URL"
                        ></textarea>
                      </div>
                    </div>
                  </div>
                  <input
                    type="submit"
                    onClick={() => {
                      handleSubmit(onSubmit);
                      alert(
                        `Welcome !! Now lets go apply some jobs from the clients!`
                      );
                    }}
                  />
                </form>
              </div>
            </div>
          </div>
        </>
      </div>
    </section>
  );
}
