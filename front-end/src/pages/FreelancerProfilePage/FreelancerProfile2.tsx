import React, { useEffect } from "react";
import "../../css/bootstrap.min.css";
import "../../css/responsive.css";
import "../../css/style.css";
import "../../css/animate.min.css";
import "../../css/color.css";
import YourFreelancerPlan from "../ProfileSubPage/YourFreelancerPlan/YourFreelancerPlan";
import JobsOpened from "../ProfileSubPage/JobsOpened/JobsOpened";
import JobsTook from "../ProfileSubPage/JobsTook/JobsTook";
import FavoriteJobs from "../ProfileSubPage/FavoriteJobs/FavoriteJobs";
import FavoriteFreelancers from "../ProfileSubPage/FavoriteFreelancers/FavoriteFreelancers";
import { useSelector, useDispatch } from "react-redux";
import { IRootState } from "../../redux/store";

export default function FreelancerProfile2() {
  const username = useSelector((state: IRootState) => state.auth.user.name);

  const freelancerInfo = useSelector(
    (state: IRootState) => state.auth.freelancer
  );

  return (
    <>
      <div className="container">
        <section id="hot_Product_area" className="ptb-20">
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                <div className="tabs_center_button">
                  <ul className="nav nav-tabs">
                    <li>
                      <a data-toggle="tab" href="#all" className="active">
                        ALL
                      </a>
                    </li>
                    <li>
                      <a data-toggle="tab" href="#your_freelancer_plan">
                        YOUR DEVELOPER PLAN
                      </a>
                    </li>
                    <li>
                      <a data-toggle="tab" href="#jobs_opened">
                        JOBS CREATED
                      </a>
                    </li>
                    <li>
                      <a data-toggle="tab" href="#jobs_took">
                        APPLIED JOBS
                      </a>
                    </li>
                    <li>
                      <a data-toggle="tab" href="#favorite_jobs">
                        FAVORITE JOBS
                      </a>
                    </li>
                    <li>
                      <a data-toggle="tab" href="#favorite_freelancers">
                        FAVORITE DEVELOPERS
                      </a>
                    </li>
                  </ul>
                </div>
              </div>

              <br />
              <br />

              {/* profo-info */}
              <div className="col-lg-4">
                <div className="left_side_contact side_card">
                  <ul>
                    <li className="address_location">
                      <div
                        className="contact_widget"
                        style={{ justifyContent: "center", display: "flex" }}
                      >
                        <div className="img_profiles">
                          <img
                            src={`http://localhost:8080/upload/${freelancerInfo.photo}`}
                            alt="img"
                          />
                        </div>
                      </div>
                      <br />
                      <div
                        className="contact_widget"
                        style={{ justifyContent: "center", display: "flex" }}
                      >
                        {/* <Link to="/">andshop.com</Link> */}
                        <p style={{"fontWeight":"bold"}}>{username}</p>
                      </div>
                    </li>

                    <li className="address_location">
                      <div
                        className="contact_widget"
                        style={{ justifyContent: "center", display: "flex" }}
                      >
                        <h4>Introduction</h4>
                      </div>
                      <br />
                      <div
                        className="contact_widget"
                        style={{ justifyContent: "center", display: "flex" }}
                      >
                        {/* <p>My Name is Benny. I am good at coding. I worked at some international conpany last year.</p> */}
                        <p style={{"fontWeight":"bold"}}>{freelancerInfo.introduction}</p>
                      </div>
                    </li>

                    <li className="address_location">
                      <div
                        className="contact_widget"
                        style={{ justifyContent: "center", display: "flex" }}
                      >
                        <h4>Work Experience</h4>
                      </div>
                      <br />
                      <div
                        className="contact_widget"
                        style={{ justifyContent: "center", display: "flex" }}
                      >
                        {/* <p>
                                            ABC Company - Develop, maintain and support application programs for adminstrative
                                            <br/>
                                            <br/>
                                            DEF Company - analysize database information via DBmongo
                                            <br/>
                                            <br/>
                                            GHI Company - Dog Nose Identify Application
                                        </p> */}
                        <p style={{"fontWeight":"bold"}}>{freelancerInfo.experience}</p>
                      </div>
                    </li>

                    <li className="address_location">
                      <div
                        className="contact_widget"
                        style={{ justifyContent: "center", display: "flex" }}
                      >
                        <h4>Technology Summary</h4>
                      </div>
                      <br />
                      <div
                        className="contact_widget"
                        style={{ justifyContent: "center", display: "flex" }}
                      >
                        {/* <p>Java, C, C++/CLI, Python, Visual C++, MySQL, J2EE, C, JavaScript, AngularJS, JQuery, Bootstrap, CSS/LESS/Sass, ActionScript, Perl, PHP, VB.Net, SQL Server, ASP.Net, HTML, XML, SAP, ActiveX, Unix, Linux, AIX, Windows Server 2016/2012, Mac OS X</p> */}
                        <p style={{"fontWeight":"bold"}}>{freelancerInfo.technology_sum}</p>
                      </div>
                    </li>

                    <li className="address_location">
                      <div
                        className="contact_widget"
                        style={{ justifyContent: "center", display: "flex" }}
                      >
                        <h4>Contact</h4>
                      </div>
                      <br />
                      <div
                        className="contact_widget"
                        style={{ justifyContent: "center", display: "flex" }}
                      >
                        <i className="fa fa-phone"></i>
                        {/* <div>&ensp;+1 (833) 287-3077</div> */}
                        <div style={{"fontWeight":"bold"}}>&ensp;{freelancerInfo.phone_num}</div>
                      </div>

                      <div
                        className="contact_widget"
                        style={{ justifyContent: "center", display: "flex" }}
                      >
                        <i className="fa fa-envelope"></i>
                        {/* <Link to="/">demo@gmail.com</Link> */}
                        {/* <div>&ensp;demo@gmail.com</div> */}
                        <div style={{"fontWeight":"bold"}}>&ensp;{freelancerInfo.email}</div>
                      </div>
                    </li>

                    <li className="address_location">
                      <div
                        className="contact_widget"
                        style={{ justifyContent: "center", display: "flex" }}
                      >
                        <h4>My Website</h4>
                      </div>
                      <br />
                      <div
                        className="contact_widget"
                        style={{ justifyContent: "center", display: "flex" }}
                      >
                        {/* <p>Java, C, C++/CLI, Python, Visual C++, MySQL, J2EE, C, JavaScript, AngularJS, JQuery, Bootstrap, CSS/LESS/Sass, ActionScript, Perl, PHP, VB.Net, SQL Server, ASP.Net, HTML, XML, SAP, ActiveX, Unix, Linux, AIX, Windows Server 2016/2012, Mac OS X</p> */}
                        <p style={{"fontWeight":"bold"}}>{freelancerInfo.ref_link}</p>
                      </div>
                    </li>

                    <li className="address_location">
                      <div
                        className="contact_widget"
                        style={{ justifyContent: "center", display: "flex" }}
                      >
                        <h4>Country</h4>
                      </div>
                      <br />
                      <div
                        className="contact_widget"
                        style={{ justifyContent: "center", display: "flex" }}
                      >
                        {/* <p>Java, C, C++/CLI, Python, Visual C++, MySQL, J2EE, C, JavaScript, AngularJS, JQuery, Bootstrap, CSS/LESS/Sass, ActionScript, Perl, PHP, VB.Net, SQL Server, ASP.Net, HTML, XML, SAP, ActiveX, Unix, Linux, AIX, Windows Server 2016/2012, Mac OS X</p> */}
                        <p style={{"fontWeight":"bold"}}>{freelancerInfo.country}</p>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>

              {/* 1,2,3,4,5,6 */}
              <div className="col-lg-8">
                <div className="tabs_el_wrapper">
                  <div className="tab-content">
                    {/* ALL */}
                    <div id="all" className="tab-pane fade show in active">
                      <div className="row">
                        <h5 style={{ backgroundColor: "grey" }}>ALL</h5>
                        <div>
                          <br />
                        </div>
                        <h5 style={{ backgroundColor: "grey" }}>
                          YOUR DEVELOPER PLAN
                        </h5>
                        <YourFreelancerPlan />
                        <div>
                          <br />
                        </div>
                        <h5 style={{ backgroundColor: "grey" }}>
                          JOBS CREATED BY YOU
                        </h5>
                        <JobsOpened />
                        <div>
                          <br />
                        </div>
                        <h5 style={{ backgroundColor: "grey" }}>
                          JOBS Applied
                        </h5>
                        <JobsTook />
                        <div>
                          <br />
                        </div>
                        <h5 style={{ backgroundColor: "grey" }}>
                          FAVORITE JOBS
                        </h5>
                        <FavoriteJobs />
                        <div>
                          <br />
                        </div>
                        <h5 style={{ backgroundColor: "grey" }}>
                          FAVORITE DEVELOPERS
                        </h5>
                        <FavoriteFreelancers />
                      </div>
                    </div>
                    {/* ALL */}

                    {/* 2 */}
                    <div id="your_freelancer_plan" className="tab-pane fade">
                      <div className="row">
                        <h5 style={{ backgroundColor: "grey" }}>
                          YOUR DEVELOPER PLAN
                        </h5>
                        {/* Freelancer Post Form */}
                        {/* Freelancer Post Form */}
                        <YourFreelancerPlan />
                        {/* Freelancer Post Form */}
                        {/* Freelancer Post Form */}
                      </div>
                    </div>
                    {/* 3 */}
                    <div id="jobs_opened" className="tab-pane fade">
                      <div className="row">
                        <h5 style={{ backgroundColor: "grey" }}>
                          JOBS CREATED BY YOU
                        </h5>
                        {/* Freelancer Post Form */}
                        {/* Freelancer Post Form */}
                        <JobsOpened />
                        {/* Freelancer Post Form */}
                        {/* Freelancer Post Form */}
                      </div>
                    </div>
                    {/* 4 */}
                    <div id="jobs_took" className="tab-pane fade">
                      <div className="row">
                        <h5 style={{ backgroundColor: "grey" }}>
                          JOBS APPLIED
                        </h5>
                        {/* Freelancer Post Form */}
                        {/* Freelancer Post Form */}
                        <JobsTook />
                        {/* Freelancer Post Form */}
                        {/* Freelancer Post Form */}
                      </div>
                    </div>
                    {/* 5 */}
                    <div id="favorite_jobs" className="tab-pane fade">
                      <div className="row">
                        <h5 style={{ backgroundColor: "grey" }}>
                          FAVORITE JOBS
                        </h5>
                        {/* Freelancer Post Form */}
                        {/* Freelancer Post Form */}
                        <FavoriteJobs />
                        {/* Freelancer Post Form */}
                        {/* Freelancer Post Form */}
                      </div>
                    </div>
                    {/* 6 */}
                    <div id="favorite_freelancers" className="tab-pane fade">
                      <div className="row">
                        <h5 style={{ backgroundColor: "grey" }}>
                          FAVORITE DEVELOPERS
                        </h5>
                        {/* Freelancer Post Form */}
                        {/* Freelancer Post Form */}
                        <FavoriteFreelancers />
                        {/* Freelancer Post Form */}
                        {/* Freelancer Post Form */}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </>
  );
}
