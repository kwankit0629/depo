import React from "react";
import ApprovedClients from "../StatusSubPage/ApprovedClients/ApprovedClients";
import ApprovedFreelancers from "../StatusSubPage/ApprovedFreelancers/ApprovedFreelancers";
import FindClients from "../StatusSubPage/FindClients/FindClients";
import FindJobs from "../StatusSubPage/FindJobs/FindJobs";

export default function Status() {
  return (
    <>
      <div className="container">
        <section id="hot_Product_area" className="ptb-20">
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                <div className="tabs_center_button">
                  <ul className="nav nav-tabs">
                    <li>
                      <a
                        data-toggle="tab"
                        href="#all_status"
                        className="active"
                      >
                        ALL STATUS
                      </a>
                    </li>
                    {/* <li>
                      <a data-toggle="tab" href="#approved_clients">
                        APPROVED CLIENTS
                      </a>
                    </li> */}
                    <li>
                      <a data-toggle="tab" href="#approved_freelancers ">
                        OWN JOBS
                      </a>
                    </li>
                    <li>
                      <a data-toggle="tab" href="#find_clients">
                        JOBS APPLIED
                      </a>
                    </li>
                    {/* <li>
                      <a data-toggle="tab" href="#find_freelancers">
                        FIND FREELANCERS
                      </a>
                    </li> */}
                  </ul>
                </div>
              </div>

              <br />
              <br />

              {/* 1,2,3,4,5,6 */}
              <div className="col-lg-12">
                <div style={{ marginRight: "10px" }}>
                  <div className="tab-content">
                    {/* ALL */}
                    <div
                      id="all_status"
                      className="tab-pane fade show in active"
                    >
                      <div>
                        <h5 style={{ backgroundColor: "grey" }}>ALL STATUS</h5>
                        <div>
                          <br />
                        </div>
                        {/* <h5 style={{ backgroundColor: "grey" }}>
                          APPROVED CLIENTS
                        </h5> */}
                        {/* <ApprovedClients /> */}
                        {/* <div>
                          <br />
                        </div> */}
                        <h5 style={{ backgroundColor: "grey" }}>OWN JOBS</h5>
                        <ApprovedFreelancers />
                        {/* <div>
                          <br />
                        </div> */}
                        <h5 style={{ backgroundColor: "grey" }}>
                          JOBS APPLIED
                        </h5>
                        <FindClients />
                        {/* <div>
                          <br />
                        </div> */}
                        {/* <h5 style={{ backgroundColor: "grey" }}>
                          FIND FREELANCERS
                        </h5> */}
                        {/* <FindJobs /> */}
                      </div>
                    </div>
                    {/* ALL */}

                    {/* 2 */}
                    <div id="approved_clients" className="tab-pane fade">
                      <div>
                        {/* <h5 style={{ backgroundColor: "grey" }}>
                          APPROVED CLIENTS
                        </h5> */}
                        {/* Freelancer Post Form */}
                        {/* Freelancer Post Form */}
                        {/* <ApprovedClients /> */}
                        {/* Freelancer Post Form */}
                        {/* Freelancer Post Form */}
                      </div>
                    </div>
                    {/* 3 */}
                    <div id="approved_freelancers" className="tab-pane fade">
                      <div>
                        <h5 style={{ backgroundColor: "grey" }}>OWN JOBS</h5>
                        {/* Freelancer Post Form */}
                        {/* Freelancer Post Form */}
                        <ApprovedFreelancers />
                        {/* Freelancer Post Form */}
                        {/* Freelancer Post Form */}
                      </div>
                    </div>
                    {/* 4 */}
                    <div id="find_clients" className="tab-pane fade">
                      <div>
                        <h5 style={{ backgroundColor: "grey" }}>
                          JOBS APPLIED
                        </h5>
                        {/* Freelancer Post Form */}
                        {/* Freelancer Post Form */}
                        <FindClients />
                        {/* Freelancer Post Form */}
                        {/* Freelancer Post Form */}
                      </div>
                    </div>
                    {/* 5 */}
                    <div id="find_freelancers" className="tab-pane fade">
                      <div>
                        {/* <h5 style={{ backgroundColor: "grey" }}>
                          FIND FREELANCERS
                        </h5> */}
                        {/* Freelancer Post Form */}
                        {/* Freelancer Post Form */}
                        {/* <FindJobs /> */}
                        {/* Freelancer Post Form */}
                        {/* Freelancer Post Form */}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </>
  );
}
