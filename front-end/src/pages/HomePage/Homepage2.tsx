import React from "react";
// import { Form } from "react-bootstrap";
import { useForm } from "react-hook-form";
import { useSelector, useDispatch } from "react-redux";
import { IRootState } from "../../redux/store";
import { useEffect } from "react";
import { getAllCategoriesThunk } from "../../redux/category/thunk";
import { Link } from "react-router-dom";
import Button from "react-bootstrap/Button";
import { push } from "connected-react-router";


export default function Homepage2() {
    const dispatch = useDispatch();
    useEffect(() => {
      dispatch(getAllCategoriesThunk());
    }, []);
  
    const categories = useSelector(
      (state: IRootState) => state.category.categories
    );

    return (
        <>
        <section id="blog_grid_area_one" className="ptb-20">
            <div className="container"> 
            <div className="row">
                
                    {categories.map((cat) => {
                        return (
                            <div
                            className="col-lg-4 col-md-4 col-sm-6 col-12"
                            style={{"width":"450px","height":"450px"}}
                            key={cat.id}
                            onClick={() => {
                                dispatch(push(`/jobs/category/${cat.id}`));
                            }}
                            >
                                {/* card */}
                                <div className="blog_one_item img-zoom-hover">
                        <div className="blog_one_img">
                            <Link to="/">
                                {/* <img src={props.img} alt="img" /> */}
                                <img
                    // src={cat.photo}
                    src={`http://localhost:8080/upload/${cat.photo}`}
                    alt="card__image"
                    style={{"border":"solid #EFEFEF 2px"}}
                  ></img>
                            </Link>
                        </div>
                        <div className="blog_text">
                            <h4 className="heading">
                                {/* <Link to="/">{props.title}</Link> */}
                                {cat.name}
                            </h4>
                            
                            
                            
                            <Link to="/" className="button">see more<i className={'fa fa-arrow-right'}></i></Link>
                        </div>
                                </div>
                                {/* card */}
                            </div>
                            );
                        })}
                
            </div>           
            </div>
        </section>

    </>
    );
  }
  