import React, { useState } from "react";
import { Form } from "react-bootstrap";
import { useForm } from "react-hook-form";
import { useSelector, useDispatch } from "react-redux";
import { IRootState } from "../../redux/store";
import { useEffect } from "react";
import {
  applyClientJobsThunk,
  getAllClientJobsThunk,
} from "../../redux/clientJobs/thunk";
import "./ClientJobs.scss";
import { Link, useParams } from "react-router-dom";
import { createClientJobBookmarkThunk } from "../../redux/job/thunk";
// import Like from "../../likeComponents/Like";
import bookmark from "../../likeComponents/bookmark-svgrepo-com.svg";
import Swal from "sweetalert2";

interface applyCLientJobsState {
  client_jobs_id: number;
  freelancer_id: number;
}
interface JobBookmarkState {
  freelancer_id: number;
  id: number;
}

export default function ClientJobs() {
  const dispatch = useDispatch();

  const clientJobs = useSelector(
    (state: IRootState) => state.clientJob.ClientJobs
  );

  const freelancerState: { id: number } = useSelector(
    // (state: IRootState) => state.auth.freelancer
    (state: IRootState) => state.auth.freelancer
  );

  const userId = useSelector((state: IRootState) => state.auth.user);

  const onSubmit = (job: any) => {
    if (userId["id"] == job.user_id) {
      Swal.fire({
        position: "center",
        icon: "error",
        title: "Seems like this is your own job!! Please apply another job!",
        showConfirmButton: false,
        timer: 2000,
      });

      return;
    }
    if (freelancerState && freelancerState["id"]) {
      const clientJobId = job.id;
      dispatch(applyClientJobsThunk(clientJobId, freelancerState["id"]));
      Swal.fire({
        position: "center",
        icon: "success",
        title: `You have applied ${job.job_name}! Let's wait for respond from client!`,
        showConfirmButton: false,
        timer: 2000,
      });
    }
  };

  useEffect(() => {
    dispatch(getAllClientJobsThunk());
  }, []);

  const onSubmit2 = (id: number) => {
    console.log("posts_id=", id);
    dispatch(createClientJobBookmarkThunk(freelancerState["id"], id));
  };

  return (
    <>
      <section id="blog_grid_area_one" className="ptb-20">
        <h3
          className="catName"
          style={{
            textAlign: "center",
            textDecoration: "underline overline black",
          }}
        >
          JOBS FROM CLIENT
        </h3>
        <br />

        <div className="container">
          <div className="row">
            {clientJobs.map((job) => {
              return (
                <div key={job.id} className="col-lg-4 col-md-4 col-sm-6 col-12">
                  <div
                    className="blog_one_item img-zoom-hover"
                    style={{
                      backgroundColor: "#EFEFEF",
                      padding: "12px",
                      height: "400px",
                      position: "relative",
                    }}
                  >
                    <div className="blog_one_img">{job.photo_num}</div>
                    <div className="blog_text">
                      <div className="heading">
                        <div style={{ fontWeight: "bold", display: "inline" }}>
                          PROJECT:{" "}
                        </div>
                        {job.job_name}
                      </div>
                      <div className="heading">
                        <div style={{ fontWeight: "bold", display: "inline" }}>
                          REQUIREMENT:{" "}
                        </div>
                        {job.job_requirement}
                      </div>
                      <div className="heading">
                        <div style={{ fontWeight: "bold", display: "inline" }}>
                          BUDGET:{" "}
                        </div>
                        $ {job.budget}
                      </div>
                      <br />

                      <div className="date_area">
                        <div style={{ fontWeight: "bold", display: "inline" }}>
                          DEADLINE:{" "}
                        </div>
                        {job.deadline} DAYS
                      </div>

                      {/* <Link
                        to="/"
                        className="button"
                        onClick={() => {
                          setClientJobsId(jobs.id);
                          handleSubmit(onSubmit);
                        }}
                      >
                        Apply this Job<i className={"fa fa-arrow-right"}></i>
                      </Link> */}
                      <div
                        className="button-box"
                        style={{ position: "absolute", bottom: "10px" }}
                      >
                        {/* <input
                          value="Apply this Job"
                          type="submit"
                          className="button"
                          onClick={() => {
                            setClientJobsId(job.id);
                            handleSubmit(onSubmit);
                          }}


                        /> */}

                        <button
                          type="button"
                          onClick={() => {
                            onSubmit2(job.id!);
                            Swal.fire({
                              position: "center",
                              icon: "success",
                              title: "Bookmark Successfully!",
                              showConfirmButton: false,
                              timer: 2000,
                            });
                          }}
                        >
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="40"
                            height="40"
                            fill="currentColor"
                            className="bi bi-bookmark"
                            viewBox="0 0 16 16"
                          >
                            <path d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v13.5a.5.5 0 0 1-.777.416L8 13.101l-5.223 2.815A.5.5 0 0 1 2 15.5V2zm2-1a1 1 0 0 0-1 1v12.566l4.723-2.482a.5.5 0 0 1 .554 0L13 14.566V2a1 1 0 0 0-1-1H4z" />
                          </svg>
                        </button>

                        <button
                          type="button"
                          className="button"
                          style={{
                            position: "absolute",
                            left: "250px",
                            bottom: "5px",
                          }}
                          onClick={() => {
                            console.log("applying job");
                            onSubmit(job);
                            // alert(
                            //   `You have applied ${job.job_name}! Let's wait for respond from client!`
                            // );
                          }}
                        >
                          Apply this Job
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </section>
    </>
  );
}
