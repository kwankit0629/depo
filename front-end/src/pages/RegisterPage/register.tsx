import { Component, useState, useEffect } from "react"
import { Form } from "react-bootstrap"
import { useForm } from "react-hook-form"
import { useDispatch, useSelector } from 'react-redux'
import { loginThunk, registerThunk } from '../../redux/auth/thunks'
import { IRootState } from "../../redux/store"
import "./register.styles.scss"
import {useLocation} from "react-router-dom"



interface FormState {
    name: string,
    password: string,
    email: string,
}

export function Register() {

    const [currentView, setCurrentView] = useState("signUp")
    const search = useLocation().search

    useEffect(() => {
        const activeTab:string | null = new URLSearchParams(search).get('active');
        if (activeTab && ["logIn", "signUp"].includes(activeTab)){
                changeView(activeTab)
        }
    },[])
    const dispatch = useDispatch()
    const { register, handleSubmit, reset } = useForm<FormState>({
        defaultValues: {
            name: "",
            password: "",
            email: "",
        }
    })

    const authState = useSelector((state: IRootState) => state.auth)
    useEffect(() =>{
        if(authState.msg ==='register success'){
            changeView('logIn')
        }
    },[authState.msg])
    const changeView = (view: string) => {
        reset()
        setCurrentView(view)
    }
    const registerSubmit = (data: FormState) => {
        console.log(data)
        dispatch(registerThunk(
            data.name,
            data.password,
            data.email,
        ))


    }
    const loginSubmit = (data: FormState) => {
        console.log(data)
        dispatch(loginThunk(
            data.name,
            data.password,
        ))

    }
    const getSection = () => {
        switch (currentView) {
            case "signUp":
                return (
                    <Form onSubmit={handleSubmit(registerSubmit)}>
                        <h2>Sign Up!</h2>
                        <fieldset>
                            <legend>Create Account</legend>
                            <ul>

                                <li>
                                    <Form.Group>
                                        <Form.Label>Username</Form.Label>
                                        <Form.Control
                                            style={{width:300}}
                                            type="text"
                                            {...register('name', {})}
                                            required
                                        />
                                    </Form.Group>
                                </li>
                                <li>
                                    <Form.Group>
                                        <Form.Label>Email</Form.Label>
                                        <Form.Control
                                            style={{width:300}}
                                            type="email"
                                            {...register('email', {})}
                                            required
                                        />
                                    </Form.Group>
                                </li>
                                <li>
                                    <Form.Group>
                                        <Form.Label>Password</Form.Label>
                                        <Form.Control
                                            style={{width:300}}
                                            type="password"
                                            {...register('password', {})}
                                            required
                                        />
                                    </Form.Group>
                                </li>
                                    <div className="error-message-container">{authState.error}</div>

                            </ul>
                        </fieldset>
                        <button>Submit</button>
                        <button type="button" onClick={() => changeView("logIn")}>Have an Account?</button>
                    </Form>
                )
                break
            case "logIn":
                return (
                    <Form onSubmit={handleSubmit(loginSubmit)}>
                        <h2>Welcome Back!</h2>
                        <fieldset>
                            <legend>Log In</legend>
                            <ul>
                                <li>
                                <Form.Group>
                                        <Form.Label>Username</Form.Label>
                                        <Form.Control
                                            style={{width:300}}
                                            type="text"
                                            {...register('name', {})}
                                            required
                                        />
                                    </Form.Group>
                                </li>
                                <li>
                                <Form.Group>
                                        <Form.Label>Password</Form.Label>
                                        <Form.Control
                                            style={{width:300}}
                                            type="password"
                                            {...register('password', {})}
                                            required
                                        />
                                    </Form.Group>
                                </li>
                            </ul>
                        </fieldset>
                        <button>Login</button>
                        <button type="button" onClick={() => changeView("signUp")}>Create an Account</button>
                    </Form>
                )
                break
            // case "PWReset":
            //     return (
            //         <form>
            //             <h2>Reset Password</h2>
            //             <fieldset>
            //                 <legend>Password Reset</legend>
            //                 <ul>
            //                     <li>
            //                         <em>A reset link will be sent to your inbox!</em>
            //                     </li>
            //                     <li>
            //                         <label htmlFor="email">Email:</label>
            //                         <input type="email" id="email" required />
            //                     </li>
            //                 </ul>
            //             </fieldset>
            //             <button>Send Reset Link</button>
            //             <button type="button" onClick={() => changeView("logIn")}>Go Back</button>
            //         </form>
            //     )
            // default:
            //     break
        }
    }

    

    return (
        <section id="entry-page">
            {getSection()}
        </section>
    )
}

