
import { useSelector, useDispatch } from "react-redux";
import { IRootState } from "../../redux/store";
import { useEffect } from "react";
import { useParams } from "react-router-dom";
import { getFreelancerPostByIdThunk } from "../../redux/job/thunk";
// import "./JobsDetails.css"
import { Button, Card, Col, Container, Nav, Row } from "react-bootstrap";
import TimeAgo from "react-timeago";
import { Link } from "react-router-dom";

<a href="https://icons8.com/icon/qePzjQLJYgjF/bookmark">Bookmark icon by Icons8</a>


export default function FreelancerPost() {
    const dispatch = useDispatch();
    const jobId = parseInt(useParams<{ id: string }>().id);
    useEffect(() => {
        dispatch(getFreelancerPostByIdThunk(jobId));
    }, []);

    const freelancerPost = useSelector(
        (state: IRootState) => state.job.jobsPost
    );
    const jobName = useSelector(
        (state: IRootState) => state.job.jobs
    );

    return (
        <>
        <div className="ptb-20">
        <div>
            {freelancerPost.map((post) => {
                return (
                    <div>
                        <Container >
                            <Row className="justify-content-md-center"
                            style={{"backgroundColor":"white"}}>
                                <Col sm={3} 
                                // style={{"backgroundColor":"green"}}
                                >
                                    <div className="card-container" style={{"backgroundColor":"#F7F7F7","padding":"10px"}}>
                                        <img className="round" src={`http://localhost:8080/upload/${post.photo}`} alt="user" />
                                        <h3>{post.name}</h3>
                                        {/* <h3><Link to={`/freelancerprofile/${post.user_id}`} style={{ color: "white", textDecoration: "none" }}>{post.name}</Link></h3> */}
                                        <h6>{post.country}</h6>

                                        <div className="card-content" style={{"color":"black"}}>
                                            <div style={{"fontWeight":"bold","display":"inline"}}>Email: </div>
                                            {post.email}
                                        </div>

                                        {/* <div className="card-content" style={{"color":"black"}}>
                                            <div  style={{"fontWeight":"bold","display":"inline"}}>Experience: </div>
                                            {post.experience}
                                        </div> */}

                                        <div className="card-content" style={{"color":"black"}}>
                                            <div style={{"fontWeight":"bold","display":"inline"}}>Created account:</div>
                                            <TimeAgo date={post.createAccount}></TimeAgo></div>

                                        

                                    </div>
                                    <div className="small-card-container" style={{"backgroundColor":"#F7F7F7","padding":"10px"}}>
                                        <h5>Introduction</h5>
                                        <span style={{"color":"black"}}>{post.introduction}</span>
                                    </div>

                                    <div className="small-card-container" style={{"backgroundColor":"#F7F7F7","padding":"10px"}}>
                                        <h5>Contact</h5>
                                        <span style={{"color":"black"}}>{post.phone_num}</span>
                                    </div>
                                    {/* <span className="bigBox">
                                    <span className="planBox">
                                        <div>Basic Plan:{post.introduction}</div>
                                        <div>Fee {post.project_fee}HKD</div>
                                    </span>
                                </span> */}
                                </Col>
                                <Col sm={5}>
                                    <div className="big-card-container" style={{"backgroundColor":"#F7F7F7","paddingTop":"0px"}}>
                                        <img src={`http://localhost:8080/upload/${post.postsImage}`}></img>
                                        <div style={{"padding":"10px"}}>
                                        <h5 style={{"color":"black"}}>Experience:</h5>
                                        <div style={{"color":"black"}}>{post.experience}</div>
                                        <br/>
                                        <h5 style={{"color":"black"}}>Technology Summary:</h5>
                                        <div style={{"color":"black"}}>{post.technology_sum}</div>
                                        <br/>
                                        <h5 style={{"color":"black"}}>What services can prove : </h5>
                                        <div style={{"color":"black"}}>{post.content}</div>
                                        {/* <Button>Comfirm to Send Request to Freelancer</Button> */}
                                            </div>


                                    </div>


                                </Col>
                            </Row>

                        </Container>

                    </div >
                );
            })}
        </div >
        </div>
        </>

    );
}
