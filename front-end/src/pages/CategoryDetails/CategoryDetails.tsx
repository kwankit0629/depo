import React from "react";
import { Col, Container, Form, Row } from "react-bootstrap";
import { useForm } from "react-hook-form";
import { useSelector, useDispatch } from "react-redux";
import { IRootState } from "../../redux/store";
import { useEffect } from "react";
import {
  getAllCategoriesThunk,
  getCatByIdThunk,
} from "../../redux/category/thunk";
import { Link, useParams } from "react-router-dom";
import "./CategoryDetails.scss";
import TimeAgo from "react-timeago";
import Button from "react-bootstrap/Button";
import { push } from "connected-react-router";
import { createJobBookmarkThunk } from "../../redux/job/thunk";

interface CreateJobBookmarkState {
  user_id: number;
  posts_id: number;
}

export default function CategoryDetails() {
  const dispatch = useDispatch();
  const { register, handleSubmit, reset } = useForm<CreateJobBookmarkState>();
  const catId = parseInt(useParams<{ id: string }>().id);
  useEffect(() => {
    dispatch(getCatByIdThunk(catId));
    dispatch(getAllCategoriesThunk());
  }, []);

  useEffect(() => {
    dispatch(getCatByIdThunk(catId));
  }, [catId]);

  const categories = useSelector(
    (state: IRootState) => state.category.categories
  );

  const freelancerByCat = useSelector(
    (state: IRootState) => state.category.CategoryPosts
  );

  const freelancerByCatB10000 = useSelector((state: IRootState) =>
    state.category.CategoryPosts.filter((cat) => cat.project_fee <= 10000)
  );

  const freelancerByCatA10000 = useSelector((state: IRootState) =>
    state.category.CategoryPosts.filter((cat) => cat.project_fee > 10000)
  );

  const catName = useSelector((state: IRootState) => state.category.categories);

  const user_id: number = useSelector(
    (state: IRootState) => state.auth.user.id
  );

  console.log("user_id:", user_id);

  const onSubmit = (posts_id: number) => {
    console.log("posts_id=", posts_id);
    dispatch(createJobBookmarkThunk(user_id, posts_id));
  };

  return (
    <div>
      <br/>
      <br/>
      {/* <h3 className="catName"
      style={{"textAlign": "center","textDecoration":"underline overline black"}}>{catName[catId - 1]?.name}</h3> */}

      <Container>
        <Row>
          <Col sm={3}>
            <div className="catnamelist">
              {categories.map((name) => {
                return (
                  <div
                    key={name.id}
                    onClick={() => {
                      dispatch(push(`/jobs/category/${name.id}`));
                    }}
                    style={{ cursor: "pointer", margin: "10px" }}
                  >
                    {/* <br/> */}
                    <a className="a:hover"
                    style={{"fontWeight":"lighter"}}>
                    {name.name}
                    </a>
                    {/* <br/> */}
                  </div>
                );
              })}
            </div>
          </Col>

          <Col sm={9}>
          
      <h3 className="catName"
      style={{"textAlign": "start","textDecoration":"underline overline black"}}>{catName[catId - 1]?.name}</h3>
            <div className="containerJobList">
              {freelancerByCat.map((post) => {
                return (
                  <div className="card"
                  style={{"width":"320px", "height":"550px"}}
                  >
                    <div className="card__header">
                      <img
                        src={`http://localhost:8080/upload/${post.postsImage}`}
                        alt="card__image"
                        className="card__image"
                        width="600"
                      ></img>
                    </div>

                    <div className="card__body">
                      <span className="tag tag-blue">{post.category_name}</span>
                      <h4>{post.content}</h4>
                      {/* <p className="price">Starting at ${post.project_fee}</p> */}
                      {/* <Link
                        to={`/jobs/jobdetails/${post.posts_id}`}
                        style={{ color: "black", textDecoration: "none" }}
                      >
                        <Button>See More</Button>
                      </Link> */}
                    </div>

                    

                    

                    <div className="see__more"
                    // style={{"position":"absolute","bottom":"2px"}}
                    >
                      <div>
                      <div className="row">
                        <div className="col-12" style={{"marginBottom":"5px","marginLeft":"8px"}}>
                        <p className="price">Starting at ${post.project_fee}</p>
                        </div>
                        
                          <div className="col-9">
                            <div style={{"display":"flex"}}>
                          <img
                          src={`http://localhost:8080/upload/${post.photo}`}
                          alt="user__image"
                          className="user__image"
                        ></img>

                        <div className="user__info">
                          <h5>
                            <Link
                              to={`/freelancerprofile/${post.user_id}`}
                              style={{ color: "black", textDecoration: "none" }}
                            >
                              {post.name}
                            </Link>
                          </h5>

                          <p>
                            <TimeAgo date={post.created_at} />
                          </p>
                        </div>
                              </div>
                          </div>
                          <div className="col-3">
                          <div className="user">
                          <button
                            type="button"
                            onClick={() => {
                              onSubmit(post.posts_id!);
                              alert(`Bookmark Success!`);
                            }}
                          >
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              width="40"
                              height="40"
                              fill="currentColor"
                              className="bi bi-bookmark"
                              viewBox="0 0 16 16"
                            >
                              <path d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v13.5a.5.5 0 0 1-.777.416L8 13.101l-5.223 2.815A.5.5 0 0 1 2 15.5V2zm2-1a1 1 0 0 0-1 1v12.566l4.723-2.482a.5.5 0 0 1 .554 0L13 14.566V2a1 1 0 0 0-1-1H4z" />
                            </svg>
                          </button>
                        </div>
                          </div>

                          <div className="co1-12"
                            style={{"marginTop":"10px", "marginLeft":"8px"}}
                          >
                          <Link
                            to={`/jobs/jobdetails/${post.posts_id}`}
                            style={{ color: "black", textDecoration: "none" }}
                          >
                            <Button>See More</Button>
                          </Link>
                          </div>
                        
                        
                      </div>
                    </div>
                  
                    </div>


                  </div>
                );
              })}
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
