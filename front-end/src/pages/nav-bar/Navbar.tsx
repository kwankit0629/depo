
import React from 'react'
import { Dropdown, DropdownButton, Navbar, NavDropdown,Container,Nav } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import { IRootState } from '../../redux/store';



export default function NavBar() {
  const username = useSelector((state: IRootState) => state.auth.user.name)
  const logout = () => {
      window.localStorage.clear();
    }

  return (
    <>
    <Navbar collapseOnSelect expand="lg" bg="light" variant="light">
  <Container>
  <Navbar.Brand href="/">TECLANCER</Navbar.Brand>
  <Navbar.Toggle aria-controls="responsive-navbar-nav" />
  <Navbar.Collapse id="responsive-navbar-nav">
    <Nav className="me-auto">
        <Nav.Link href="/clientJobs">Jobs (clients)</Nav.Link>
        <Nav.Link href="/jobs/category/1">Jobs (developers)</Nav.Link>
        {username? <Nav.Link href="/create-job-form">Create Job</Nav.Link> : <Nav/>}
        {username? <Nav.Link href="/reg-to-be-freelancer">To Be Our Developers</Nav.Link>: <Nav/>}
        {username? <Nav.Link href="/status">Job Status</Nav.Link>: <Nav/>}  
        {username? <Nav.Link href="/freelancer-profile">Profile</Nav.Link>: <Nav />}
    </Nav>
    <Nav>
      {username?<NavDropdown title={username} id="basic-nav-dropdown">
          <NavDropdown.Item href="/freelancer-profile">Profile</NavDropdown.Item>
          <NavDropdown.Item onClick={logout} href="/">Log Out</NavDropdown.Item>
        </NavDropdown>
        :<Nav.Link href="/register">Login/Register</Nav.Link>}
    </Nav>
  </Navbar.Collapse>
  </Container>
</Navbar>
    </>
  );
}

// <div style={{
    //   display: 'block', width: 3000, paddingLeft: 100
    // }}>
    //   <Navbar>
    //     <a style={{"paddingRight":"50px"}} href="/homepage2" className="navbar-brand logo">
    //       Teclancer
    //     </a>
    //     <a style={{"paddingRight":"20px"}} href="/reg-to-be-freelancer">To Be Our Freelancer</a>
    //     <a style={{"paddingRight":"20px"}} href="/create-job-form">Create Job</a>
    //     <a style={{"paddingRight":"20px"}} href="/status">Job Status</a>
        
    //     {username ? <DropdownButton
    //       variant="outline-secondary"
    //       title={username}
    //       id="input-group-dropdown-1"
    //     >
    //       <Dropdown.Item href="/freelancer-profile">Profile</Dropdown.Item>
    //       <Dropdown.Item onClick={logout} href="/">Log Out</Dropdown.Item>
    //     </DropdownButton> : <a href="/register">Login/Register</a>}
    //   </Navbar>
    // </div>