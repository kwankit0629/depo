import React from "react";
import { Form } from "react-bootstrap";
import { useForm } from "react-hook-form";
import { useSelector, useDispatch } from "react-redux";
import { useEffect } from "react";
import { Link, useParams } from "react-router-dom";
// wrong redux path
import { IRootState } from "../../../redux/store";
import { OwnClientJobsState } from "../../../redux/clientJobs/state";
import { getOwnClientJobsThunk } from "../../../redux/clientJobs/thunk";
import { getOwnClientJobsApplicantsThunk } from "../../../redux/clientJobs/thunk";


export default function JobsOpened() {
  // INSERT DATA
  const dispatch = useDispatch();

  const user_id: number = useSelector(
    (state: IRootState) => state.auth.user.id
  );

  useEffect(() => {
    if (user_id) {
      console.log("user_id = , user_id", user_id);
      dispatch(getOwnClientJobsThunk(user_id));
    }
    return () => {
      console.log("gone");
    };
  }, [dispatch, user_id]);

  const ownJobs: OwnClientJobsState[] = useSelector(
    (state: IRootState) => state.clientJob.OwnClientJobs
  );
 

  //SubApprovedFreelancers
  const jobId = parseInt(useParams<{ id: string }>().id);
  useEffect(() => {
    console.log("Getting Applicants!!!", jobId);
    dispatch(getOwnClientJobsApplicantsThunk(jobId));
  }, []);
  const applicants = useSelector(
    (state: IRootState) => state.clientJob.JobsApplicants
  );
  return (
    <>
      <section id="blog_grid_area_one" className="ptb-20">
        <div className="container">
          <div className="row">
            {ownJobs.map((job)=>{
              return(
                <div key={job.id} className="col-lg-4 col-md-4 col-sm-6 col-12">
                {/* //card */}
                {/* <div className="blog_one_item img-zoom-hover"> */}
                        {/* <div className="blog_one_img">
                          
                            {jobs.photo_num}
                          
                        </div> */}
                        <div 
                        className="blog_one_item"
                        style={{"backgroundColor":"#EFEFEF","margin":"5px","padding":"30px","height":"500px","marginBottom":"30px"}}>
                            <div className="heading">
                              <div style={{"fontWeight":"bold","display":"inline"}}>JOB NAME: </div>
                              {job.job_name}
                            </div>
                            <br/>
                              
                            <div className="heading">
                              <div style={{"fontWeight":"bold","display":"inline"}}>BUDGET: </div>
                               $ {job.budget}
                            </div>
                            <br/>

                            <div className="date_area">
                              <div style={{"fontWeight":"bold","display":"inline"}}>REQUIREMENT: </div>
                              {job.job_requirement}
                            </div>
                            <br/>
                            <div className="date_area">
                              <div style={{"fontWeight":"bold","display":"inline"}}>DEADLINE: </div>
                              {job.deadline} DAYS
                            </div>
                            {/* <Link to="/client-jobs-application/5" className="button">See More<i className={'fa fa-arrow-right'}></i></Link> */}
                        </div>
                    {/* </div> */}
                {/* //card */}
                </div>
              )
            })}

          </div>
        </div>
      </section>
    </>
  )
}