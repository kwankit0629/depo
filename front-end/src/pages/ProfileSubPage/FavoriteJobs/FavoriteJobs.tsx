import React, { useState } from "react";
import { Form } from "react-bootstrap";
import { useForm } from "react-hook-form";
import { useSelector, useDispatch } from "react-redux";
import { useEffect } from "react";
import { Link, useParams } from "react-router-dom";
// wrong redux path
import {
  applyClientJobsThunk,
  getAllClientJobsThunk,
  getOwnAppliedClientJobsInfoThunk,
} from "../../../redux/clientJobs/thunk";
import { IRootState } from "../../../redux/store";
// import { createClientJobBookmarkThunk, createJobBookmarkThunk, gotClientJobBookmarkThunk } from "../../../redux/job/thunk";
import Like from "../../../likeComponents/Like";
import {
  createClientJobBookmarkThunk,
  createJobBookmarkThunk,
  deleteClientJobBookmarkThunk,
  gotClientJobBookmarkThunk,
} from "../../../redux/job/thunk";
import { fetchDeleteClientJobBookmark } from "../../../api/job";
import Swal from "sweetalert2";

export default function JobsOpened() {
  const dispatch = useDispatch();
  const freelancerState: { id: number } = useSelector(
    // (state: IRootState) => state.auth.freelancer
    (state: IRootState) => state.auth.freelancer
  );
  useEffect(() => {
    if (freelancerState["id"]) {
      console.log("freelancer_id = freelancer_id", freelancerState["id"]);
      dispatch(gotClientJobBookmarkThunk(freelancerState["id"]));
      dispatch(getOwnAppliedClientJobsInfoThunk(freelancerState["id"]));
    }
  }, [dispatch, freelancerState["id"]]);

  const onSubmit = (clientJobId: number) => {
    console.log("posts_id=", clientJobId);
    dispatch(deleteClientJobBookmarkThunk(freelancerState["id"], clientJobId));
  };

  const onSubmit2 = (jobs: any) => {
    if (freelancerState && freelancerState["id"]) {
      const clientJobId = jobs.id;
      dispatch(applyClientJobsThunk(clientJobId, freelancerState["id"]));
      dispatch(
        deleteClientJobBookmarkThunk(freelancerState["id"], clientJobId)
      );
    }
  };

  const [liked, setLiked] = useState(false);

  const clientJobs = useSelector(
    (state: IRootState) => state.job.gotClientJobBookmark
  );
  return (
    <>
      <section id="blog_grid_area_one" className="ptb-20">
        <div className="container">
          <div className="row">
            {clientJobs.map((jobs) => {
              return (
                <div
                  key={jobs.id}
                  className="col-lg-4 col-md-4 col-sm-6 col-12"
                >
                  {/* //card */}
                  <div
                    className="blog_one_item "
                    style={{ position: "relative", marginBottom: "30px" }}
                  >
                    {/* <div className="blog_one_img">

                      <button type="button" onClick={() => {

                        onSubmit(jobs.id!)
                      }}>
                        X
                      </button>

                    </div> */}
                    <div
                      className="blog_text"
                      style={{
                        backgroundColor: "#EFEFEF",
                        margin: "5px",
                        padding: "30px",
                        height: "500px",
                      }}
                    >
                      <div className="blog_one_img">
                        <button
                          type="button"
                          onClick={() => {
                            onSubmit(jobs.id!);
                          }}
                        >
                          X
                        </button>
                      </div>

                      <div className="heading">
                        <div style={{ fontWeight: "bold", display: "inline" }}>
                          JOB NAME:{" "}
                        </div>
                        {jobs.job_name}
                      </div>
                      <div className="heading">
                        <div style={{ fontWeight: "bold", display: "inline" }}>
                          BUDGET:{" "}
                        </div>{" "}
                        $ {jobs.budget}
                      </div>
                      <div className="heading">
                        <div style={{ fontWeight: "bold", display: "inline" }}>
                          REQUIREMENT:
                        </div>
                        {jobs.job_requirement}
                      </div>
                      <br />
                      <div className="date_area">
                        <div style={{ fontWeight: "bold", display: "inline" }}>
                          DEADLINE:{" "}
                        </div>
                        {jobs.deadline} DAYS
                      </div>
                      <br />
                      {/* <Like toggleLike={()=>{
                        console.log("toggleLike");
                        setLiked(!liked)
                        
                        }} isLiked={liked}/> */}

                      <div
                        className="button"
                        style={{
                          position: "absolute",
                          bottom: "20px",
                          textAlign: "center",
                          left: "50px",
                        }}
                        onClick={() => {
                          console.log("applying job");
                          onSubmit2(jobs);
                          Swal.fire({
                            position: "center",
                            icon: "success",
                            title: `You have applied ${jobs.job_name}! Let's wait for respond from client!`,
                            showConfirmButton: false,
                            timer: 2000,
                          });
                        }}
                      >
                        APPLY NOW
                      </div>
                    </div>
                  </div>
                  {/* //card */}
                </div>
              );
            })}
          </div>
        </div>
      </section>
    </>
  );
}
