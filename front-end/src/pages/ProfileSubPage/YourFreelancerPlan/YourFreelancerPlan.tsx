import React from "react";
// import "./YourFreelancerPlan.css"
// import "../../../css/bootstrap.min.css"
// import "../../../css/responsive.css"
// import "../../../css/style.css"
// import "../../../css/animate.min.css"

import ReactDOM from "react-dom";
import { useForm, SubmitHandler } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import {
  createFreelancerJobThunk,
  gotOwnFreelancerPostsThunk,
} from "../../../redux/job/thunk";
import { IRootState } from "../../../redux/store";
import { useEffect } from "react";
import Swal from "sweetalert2";

interface CreateFreelancerJobState {
  freelancer_id: number;
  category_id: number;
  content: string;
  project_fee: number;
  photo: string;
}
export default function CreateFreelancerJobForm() {
  const dispatch = useDispatch();
  const { register, handleSubmit, reset } = useForm<CreateFreelancerJobState>();

  const freelancerState: { id: number } = useSelector(
    // (state: IRootState) => state.auth.freelancer
    (state: IRootState) => state.auth.freelancer
  );

  const onSubmit = (data: CreateFreelancerJobState) => {
    if (freelancerState && freelancerState["id"]) {
      const freelancer_id = freelancerState["id"];

      dispatch(
        createFreelancerJobThunk(
          freelancer_id,
          data.category_id,
          data.content,
          data.project_fee,
          data.photo
        )
      );
      reset();
    }
  };

  useEffect(() => {
    if (freelancerState && freelancerState["id"]) {
      const freelancer_id = freelancerState["id"];
      dispatch(gotOwnFreelancerPostsThunk(freelancer_id));
    }
  }, [dispatch, freelancerState]);

  const ownFreelancerPosts = useSelector(
    (state: IRootState) => state.job.ownFreelancerPost
  );

  const jobPhotoHandler = async (e: any) => {
    const jobPhoto = new FormData();
    jobPhoto.append("jobPhoto", e.target.files[0]);
    // await Server.updateJobPhotoHandler(auth.user.id, jobPhoto);
  };

  return (
    <section className="ptb-20">
      <div className="container">
        <>
          <div>
            <div className="checkout-area-bg bg-white">
              <div className="check-heading">
                <h3>Crate Your Plan</h3>
                <h6>Fields marked with an asterisk (*) are required.</h6>
              </div>
              <div className="check-out-form">
                <form onSubmit={handleSubmit(onSubmit)}>
                  <div className="row">
                    <div className="col-lg-12 col-md-12 col-sm-=12 col-12">
                      <div className="form-group">
                        {/* dropdown */}
                        <label htmlFor="messages">
                          Type Of Jobs<span className="text-danger">*</span>
                        </label>
                        <select id="message" {...register("category_id")}>
                          <option value="choose">Choose</option>
                          <option value="1">WordPress</option>
                          <option value="2">Website Builders & CMS</option>
                          <option value="3">Game Development</option>
                          <option value="4">Development for Streamers</option>
                          <option value="5">Web Programming</option>
                          <option value="6">E-Commerce Development</option>
                          <option value="7">Mobile Apps</option>
                          <option value="8">Desktop Applications</option>
                          <option value="9">Chatbots</option>
                          <option value="10">Support & IT</option>
                          <option value="11">
                            Cybersecurity & Data Protection
                          </option>
                          <option value="12">Electronics Engineering</option>
                          <option value="13">Convert Files</option>
                          <option value="14">User Testing</option>
                          <option value="15">QA & Review</option>
                          <option value="16">
                            Blockchain & Cryptocurrency
                          </option>
                          <option value="17">Databases</option>
                          <option value="18">Data Processing</option>
                          <option value="19">Data Engineering</option>
                          <option value="20">Data Science</option>
                        </select>
                      </div>
                    </div>

                    <div className="col-lg-12 col-md-12 col-sm-=12 col-12">
                      <div className="form-group">
                        <label htmlFor="messages">
                          What do you want to tell your future client ? (will be
                          shown in the developers page)
                          <span className="text-danger">*</span>
                        </label>
                        <textarea
                          {...register("content")}
                          className="form-control"
                          id="messages"
                          placeholder="Please Enter your service introduction."
                        ></textarea>
                      </div>
                    </div>
                    {/* 
                                <div className="col-lg-12 col-md-12 col-sm-=12 col-12">
                                    <div className="form-group">
                                        <label htmlFor="messages">Plan Description<span className="text-danger">*</span></label>
                                        <textarea {...register("yourFreelancerPlan", {maxLength:5})}  className="form-control" id="messages"
                                            placeholder="I have been worked on web design field in 5 years. I hope I can help you ."></textarea>
                                    </div>
                                </div> */}

                    <div className="col-lg-12 col-md-12 col-sm-=12 col-12">
                      <div className="form-group">
                        <label htmlFor="messages">
                          Price Range<span className="text-danger">*</span>
                        </label>
                        <input
                          {...register("project_fee", { required: true })}
                          type="text"
                          className="form-control"
                          id="fname"
                          placeholder="Enter Price Range"
                        />
                      </div>
                    </div>

                    <div className="col-lg-12 col-md-12 col-sm-=12 col-12">
                      <div className="form-group">
                        <label htmlFor="messages">
                          photo<span className="text-danger">*</span>
                        </label>
                        <input
                          {...register("photo", { required: true })}
                          type="file"
                        />
                      </div>
                    </div>

                    {/* <div className="col-lg-12 col-md-12 col-sm-=12 col-12">
                                        <div className="form-group">
                                            <label htmlFor="messages">Around Price<span className="text-danger">*</span></label>
                                            <input  {...register("aroundDate", { required: true})} type="text" className="form-control" id="fname"
                                                placeholder="4-20Days" />
                                        </div>
                                    </div> */}
                  </div>

                  {/* <div style={{ "justifyContent": "space-between", "display": "flex" }}>
                                        <button type="submit" style={{ "borderStyle": "ridge", "borderRadius": "4px", "border": "1px solid #DDDDDD", "backgroundColor": "white" }}>UPDATE</button>
                                        <button type="submit" style={{ "borderStyle": "ridge", "borderRadius": "4px", "border": "1px solid #DDDDDD", "backgroundColor": "white" }}>SUBMIT</button>
                                        <button type="submit" style={{ "borderStyle": "ridge", "borderRadius": "4px", "border": "1px solid #DDDDDD", "backgroundColor": "white" }}>DELETE ALL</button>
                                    </div> */}
                  <input
                    type="submit"
                    onClick={() => {
                      handleSubmit(onSubmit);
                      Swal.fire({
                        position: "center",
                        icon: "success",
                        title: `Submit Success`,
                        showConfirmButton: false,
                        timer: 2000,
                      });
                    }}
                  />
                </form>
              </div>
            </div>
            <br />
            <div>
              <div className="">
                <div className="row">
                  {ownFreelancerPosts.map((post) => {
                    return (
                      <div
                        key={post.id}
                        className="col-lg-4 col-md-4 col-sm-6 col-12"
                      >
                        <div
                          className="blog_one_item"
                          style={{
                            backgroundColor: "#EFEFEF",
                            margin: "5px",
                            padding: "30px",
                            height: "500px",
                            marginBottom: "30px",
                          }}
                        >
                          <div
                            className="contact_widget"
                            style={{
                              justifyContent: "center",
                              display: "flex",
                            }}
                          >
                            <img
                              alt="img"
                              src={`http://localhost:8080/upload/${post.postsImage}`}
                            />
                          </div>

                          <div className="" style={{ padding: "5px" }}>
                            <div className="heading">
                              <div
                                style={{
                                  fontWeight: "bold",
                                  display: "inline",
                                }}
                              >
                                Service:
                              </div>{" "}
                              {post.content}
                            </div>
                            <br />

                            <div className="heading">
                              <div
                                style={{
                                  fontWeight: "bold",
                                  display: "inline",
                                }}
                              >
                                Service Category:
                              </div>{" "}
                              {post.category_name}
                            </div>
                            <br />

                            <div className="heading">
                              <div
                                style={{
                                  fontWeight: "bold",
                                  display: "inline",
                                }}
                              >
                                Service fee:{" "}
                              </div>{" "}
                              ${post.project_fee}
                            </div>
                          </div>
                        </div>
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
        </>
      </div>
    </section>
  );
}
