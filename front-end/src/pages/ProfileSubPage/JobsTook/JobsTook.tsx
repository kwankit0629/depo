import React from "react";
import { Form } from "react-bootstrap";
import { useForm } from "react-hook-form";
import { useSelector, useDispatch } from "react-redux";
import { useEffect } from "react";
import { Link, useParams } from "react-router-dom";
// wrong redux path
import {
  getAllClientJobsThunk,
  getOwnAppliedClientJobsInfoThunk,
} from "../../../redux/clientJobs/thunk";
import { IRootState } from "../../../redux/store";

export default function JobsOpened() {
  const dispatch = useDispatch();
  const freelancerId = useSelector(
    // (state: IRootState) => state.auth.freelancer
    (state: IRootState) => state.auth.freelancer.id
  );

  useEffect(() => {
    dispatch(getOwnAppliedClientJobsInfoThunk(freelancerId));
  }, [freelancerId]);

  const appliedJobs = useSelector(
    (state: IRootState) => state.clientJob.AppliedJobs
  );
  return (
    <>
      <section id="blog_grid_area_one" className="ptb-20">
        <div className="container">
          <div className="row">
            {appliedJobs.map((jobs) => {
              return (
                <div
                  key={jobs.client_job_id}
                  className="col-lg-4 col-md-4 col-sm-6 col-12"
                >
                  {/* //card */}
                  <div className="blog_one_item"
                  style={{"backgroundColor": "#EFEFEF","margin":"5px","padding":"30px","height":"500px","marginBottom":"30px" }}>
                    {/* <div className="blog_one_img">{jobs.client_job_phone}</div> */}
                    <div className="">
                      <div className="heading">
                        <div style={{"fontWeight":"bold","display":"inline"}}>Project Name: </div>
                        {jobs.client_job_name}
                      </div>
                      <br/>
                              
                      <div className="heading">
                        <div style={{"fontWeight":"bold","display":"inline"}}>Project Budget: </div>
                        ${jobs.client_job_budget}
                      </div>
                      <br />

                      <div className="date_area">
                        <div  style={{"fontWeight":"bold","display":"inline"}}>Deadline: </div>
                        {jobs.client_job_deadline} DAYS
                      </div>

                      {/* <Link to="/" className="button">
                        See More<i className={"fa fa-arrow-right"}></i>
                      </Link> */}
                    </div>
                  </div>
                  {/* //card */}
                </div>
              );
            })}
          </div>
        </div>
      </section>
    </>
  );
}
