import React from "react";
import { Form } from "react-bootstrap";
import { useForm } from "react-hook-form";
import { useSelector, useDispatch } from "react-redux";
import { useEffect } from "react";
import { Link, useParams } from "react-router-dom";
// wrong redux path
import { getAllClientJobsThunk } from "../../../redux/clientJobs/thunk";
import { IRootState } from "../../../redux/store";
import { createJobBookmarkThunk, deleteJobBookmarkThunk, gotFreelancerJobBookmarkThunk } from "../../../redux/job/thunk";

export default function JobsOpened() {
  const dispatch = useDispatch();
  const user_id: number = useSelector(
    (state: IRootState) => state.auth.user.id
  );
  useEffect(() => {
    if (user_id) {
      console.log("user_id = , user_id", user_id);
      dispatch(gotFreelancerJobBookmarkThunk(user_id));
    }
  }, [dispatch, user_id]);
  const Jobs = useSelector(
    (state: IRootState) => state.job.gotFreelancerJobBookmark
  );

  const onSubmit = (posts_id: number) => {
    console.log("posts_id=", posts_id)
    dispatch(deleteJobBookmarkThunk(
      user_id,
      posts_id
    ))
  }

  return (
    <>
      <section id="blog_grid_area_one" className="ptb-20">
        <div className="container">
          <div className="row">
            {Jobs.map((jobs) => {
              return (
                <div key={jobs.posts_id} className="col-lg-4 col-md-4 col-sm-6 col-12">
                  {/* //card */}
                  
                  <div
                    className="blog_one_item "
                    style={{ "backgroundColor": "#EFEFEF" ,"margin":"5px","padding":"30px","height":"500px","position":"relative","marginBottom":"30px" }}
                    >
                      <button type="button" onClick={() => {

                        onSubmit(jobs.posts_id!)
                        }}>
                        x
                        </button>
                    <div className="" style={{ "justifyContent": "center", "display": "flex" }}>
                      <div className="img_profiles" >
                        <img alt="img" src={`http://localhost:8080/upload/${jobs.photo}`} />
                      </div>
                    </div>
                    <br />
                    <div style={{ "justifyContent": "left", "display": "flex" }}>
                      {/* <Link to="/">andshop.com</Link> */}
                      <div>
                        <div style={{"fontWeight":"bold","display":"inline"}}>NAME: </div>
                        {jobs.name}
                        </div>
                    </div>

                    <div className="blog_text">
                      <div className="heading" style={{ "textAlign": "left" }} >
                        <div style={{"fontWeight":"bold","display":"inline"}}>JOB CONTENT: </div>
                        {jobs.content}
                      </div>
                      {/* <h5 className="date_area" style={{"textAlign":"center"}}>
                              {jobs.deadline} DAYS LEAVE
                            </h5> */}
                      <div className="heading" style={{ "textAlign": "left" }}>
                        <div style={{"fontWeight":"bold","display":"inline"}}>BUDGET: </div>
                        ${jobs.project_fee}
                      </div>
                      <div style={{ "justifyContent": "left", "display": "flex","position":"absolute","bottom":"20px","left":"45px" }}>
                        <Link to={`/jobs/jobdetails/${jobs.posts_id}`} className="button" style={{ "textAlign": "center" }}>See More</Link>
                      </div>
                      {/* <Link to="/">
                        <i className="fa-solid fa-bookmark fa-2x" style={{ "display": "flex", "justifyContent": "left", "paddingTop": "5px" }}></i>
                      </Link> */}
                    </div>




                  </div>
                  {/* //card */}
                </div>
              )
            })}

          </div>
        </div>
      </section>
    </>
  )
}