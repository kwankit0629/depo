import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useParams } from "react-router-dom";
import { IRootState } from "../../redux/store";
import { getAllJobThunk } from "../../redux/job/thunk";
import "./joblistpage.css"
import TimeAgo from "react-timeago";
import { push } from "connected-react-router";
import { getCatByIdThunk } from "../../redux/category/thunk";
import { Button, Col, Container, Row } from "react-bootstrap";


export default function Jobs() {
  const dispatch = useDispatch();
  const catId = parseInt(useParams<{ id: string }>().id);
  useEffect(() => {
    dispatch(getAllJobThunk());
    dispatch(getCatByIdThunk(catId));
  }, []);

  const jobs = useSelector(
    (state: IRootState) => state.job.jobs
  );

  const categories = useSelector(
    (state: IRootState) => state.category.categories
  );

  useEffect(() => {
    dispatch(getCatByIdThunk(catId));
  }, [catId]);

  const catName = useSelector((state: IRootState) => state.category.categories);



  return (
    <div>
      <h1 className="catName" style={{ textAlign: "center" }}>
        {catName[catId - 1]?.name}
      </h1>

      <Container>
        <Row>
          <Col sm={3}>
            <div className="catnamelist">
              {categories.map((name) => {
                return (
                  <div
                    key={name.id}
                    onClick={() => {
                      dispatch(push(`/jobs/category/${name.id}`));
                    }}
                  >
                    {name.name}
                  </div>
                );
              })}
            </div>
          </Col>

          <Col sm={9}>
            <div className="containerJobList">
              {jobs.map((post) => {
                return (
                  <div className="card">
                    <div className="card__header">
                      <img
                        src={`http://localhost:8080/upload/${post.postsImage}`}
                        alt="card__image"
                        className="card__image"
                        width="600"
                      ></img>
                    </div>
                    <div className="card__body">
                      <span className="tag tag-blue">{post.category_name}</span>
                      <h4>
                        {post.content}
                      </h4>
                      <p className="price">Starting at ${post.project_fee}</p>
                      <Link
                        to={`/jobs/jobdetails/${post.posts_id}`}
                        style={{ color: "black", textDecoration: "none" }}
                      >
                        <Button>See More</Button>
                      </Link>
                    </div>
                    <div className="card__footer">
                      <div className="user">
                        <img
                          src={`http://localhost:8080/upload/${post.photo}`}
                          alt="user__image"
                          className="user__image"
                        ></img>
                        <div className="user__info">
                          <h5>
                            <Link
                              to={`/freelancerprofile/${post.user_id}`}
                              style={{ color: "black", textDecoration: "none" }}
                            >
                              {post.name}
                            </Link>
                          </h5>

                          <p>
                            <TimeAgo date={post.created_at} />
                          </p>
                        </div>
                        <div className="user">
                          <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor" className="bookmark" viewBox="0 0 16 16">
                            <path d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v13.5a.5.5 0 0 1-.777.416L8 13.101l-5.223 2.815A.5.5 0 0 1 2 15.5V2zm2-1a1 1 0 0 0-1 1v12.566l4.723-2.482a.5.5 0 0 1 .554 0L13 14.566V2a1 1 0 0 0-1-1H4z" />
                          </svg>
                        </div>
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
