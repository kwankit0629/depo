const { REACT_APP_API_SERVER } = process.env;

export async function fetchGetAllCategories() {
  const res = await fetch("http://localhost:8080/category", {
    headers: {
      "Content-Type": "application/json",
    },
  });
  return res;
}

export async function fetchCategoryById(id: number) {
  const res = await fetch(`http://localhost:8080/category/${id}`);
  return res;
}
