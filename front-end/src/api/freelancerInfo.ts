const { REACT_APP_API_SERVER } = process.env;

export async function fetchGetAllFreelancer() {
  const res = await fetch("http://localhost:8080/freelancer", {
    headers: {
      "Content-Type": "application/json",
    },
  });
  return res;
}
export async function fetchGetAllFreelancerPosts() {
  const res = await fetch("http://localhost:8080/freelancerPost", {
    headers: {
      "Content-Type": "application/json",
    },
  });
  return res;
}
export async function fetchGetOwnPosts() {
  const res = await fetch("http://localhost:8080/ownFreelancerPosts", {
    headers: {
      "Content-Type": "application/json",
    },
  });
  return res;
}
export async function fetchGetOtherFreelancerPosts() {
  const res = await fetch("http://localhost:8080/otherFreelancerPosts", {
    headers: {
      "Content-Type": "application/json",
    },
  });
  return res;
}
export async function fetchRegToBeAFreelancer() {
  const res = await fetch("http://localhost:8080/register", {
    headers: {
      "Content-Type": "application/json",
    },
  });
  return res;
}

export async function fetchGetFreelancerById(userId: number) {
  const res = await fetch(`http://localhost:8080/freelancerId/${userId}`);
  return res;
}
