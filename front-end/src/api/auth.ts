const { REACT_APP_API_SERVER } = process.env;

export async function fetchLogin(name: string, password: string) {
  const res = await fetch(`${REACT_APP_API_SERVER}/api/users/login`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      name,
      password,
    }),
  });
  return res;
}

export async function fetchUserInfo() {
  const res = await fetch(`${REACT_APP_API_SERVER}/api/users/me`);
  return res;
}

export async function fetchRegister(
  name: string,
  password: string,
  email: string
) {
  const res = await fetch(`${REACT_APP_API_SERVER}/api/users/newuser`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      name,
      password,
      email,
    }),
  });
  return res;
}

export async function fetchRegToBeAFreelancer(
  age: number,
  email: string,
  phone_num: string,
  photo: string,
  country: string,
  experience: string,
  introduction: string,
  technology_sum: string,
  ref_link: string,
  user_id: number | string
) {
  let formData = new FormData();
  formData.append("age", age.toString());
  formData.append("email", email.toString());
  formData.append("phone_num", phone_num.toString());
  formData.append("image", photo[0]);
  formData.append("country", country.toString());
  formData.append("experience", experience.toString());
  formData.append("introduction", introduction.toString());
  formData.append("technology_sum", technology_sum.toString());
  formData.append("ref_link", ref_link.toString());
  formData.append("user_id", user_id.toString());
  const res = await fetch("http://localhost:8080/register", {
    method: "POST",
    body: formData,
  });
  return res;
}

export async function fetchFreelancerById(user_id: number) {
  const res = await fetch(`http://localhost:8080/freelancerId/${user_id}`);
  return res;
}

// export async function fetchFacebookLogin(accessToken: string) {
//     console.log(accessToken)
//     const res = await fetch(`${REACT_APP_API_SERVER}/api/users/loginFacebook`, {
//         method: "POST",
//         headers : {
//             "Content-Type": "application/json",
//         },
//         body: JSON.stringify({
//             accessToken
//         })
//     })
//     return res;
// }
