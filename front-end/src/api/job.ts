const { REACT_APP_API_SERVER } = process.env;

export async function fetchGetAllJob() {
  const res = await fetch(`http://localhost:8080/api/jobs/freelancerpost`, {
    headers: {
      "Content-Type": "application/json",
    },
  });
  return res;
}

export async function fetchFreelancerPostById(id: number) {
  const res = await fetch(
    `http://localhost:8080/api/jobs/freelancerpost/${id}`
  );
  return res;
}

export async function fetchFreelancerOwnPostById(freelancerId: number) {
  const res = await fetch(
    `http://localhost:8080/api/jobs/freelancerpostbyfreelancerid/${freelancerId}`
  );
  return res;
}

export async function fetchCreateFreelancerJob(
  freelancer_id: number,
  category_id: number,
  content: string,
  project_fee: number,
  photo: string
) {
  let formData = new FormData();
  formData.append("freelancer_id", freelancer_id.toString());
  formData.append("category_id", category_id.toString());
  formData.append("content", content);
  formData.append("project_fee", project_fee.toString());
  formData.append("image", photo[0]);
  const res = await fetch(
    `http://localhost:8080/api/jobs/createfreelancerjob/`,
    {
      method: "POST",
      body: formData,
    }
  );
  return res;
}

export async function fetchCreateJobBookmark(
  user_id: number,
  posts_id: number
) {
  console.log(user_id);
  const res = await fetch("http://localhost:8080/api/users/jobsbookmark/", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      user_id,
      posts_id,
    }),
  });
  return res;
}

export async function fetchDeleteJobBookmark(
  user_id: number,
  posts_id: number
) {
  console.log(user_id);
  const res = await fetch(
    "http://localhost:8080/api/users/deletejobsbookmark",
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        user_id,
        posts_id,
      }),
    }
  );
  return res;
}

export async function fetchDeleteClientJobBookmark(
  freelancer_id: number,
  clientJobId: number
) {
  const res = await fetch("http://localhost:8080/deleteclientjobsbookmark", {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      freelancer_id,
      clientJobId,
    }),
  });
  return res;
}

export async function fetchFreelancerJobBookmark(user_id: number) {
  console.log("bookmarkapi", user_id);
  const res = await fetch(
    `http://localhost:8080/api/users/freelancerjobbookmarked/${user_id}`
  );
  return res;
}

export async function fetchClientJobBookmark(freelancer_id: number) {
  console.log("bookmarkapi", freelancer_id);
  const res = await fetch(
    `http://localhost:8080/api/users/clientjobbookmarked/${freelancer_id}`
  );
  return res;
}

export async function fetchCreateClientJobBookmark(
  freelancer_id: number,
  id: number
) {
  const res = await fetch("http://localhost:8080/clientjobbookmark/", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      freelancer_id,
      id,
    }),
  });
  return res;
}
