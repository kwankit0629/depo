export async function fetchGetAllClientJobs() {
  const res = await fetch("http://localhost:8080/clientJobs", {
    headers: {
      "Content-Type": "application/json",
    },
  });
  return res;
}

export async function fetchCreateClientJob(
  user_id: number,
  phone_num: string,
  job_name: string,
  job_intro: string,
  job_requirement: string,
  budget: number,
  deadline: number
) {
  console.log(user_id);
  const res = await fetch("http://localhost:8080/newClientJob", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      user_id,
      phone_num,
      job_name,
      job_intro,
      job_requirement,
      budget,
      deadline,
    }),
  });
  return res;
}

export async function fetchApplyClientJobs(
  client_jobs_id: number,
  freelancer_id: number
) {
  const res = await fetch("http://localhost:8080/application", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      client_jobs_id,
      freelancer_id,
    }),
  });
  return res;
}

export async function fetchOwnClientJobsById(user_id: number) {
  const res = await fetch(`http://localhost:8080/clientId/${user_id}`);
  return res;
}

export async function fetchAllClientJobsApplicants(jobId: number) {
  const res = await fetch(
    `http://localhost:8080/client-jobs-application/${jobId}`
  );
  return res;
}
export async function fetchAllAppliedClientJobsInfo(freelancerId: number) {
  const res = await fetch(
    `http://localhost:8080/client-jobs-applied/${freelancerId}`
  );
  return res;
}

export async function updateClientJobsAssignee(
  client_jobs_id: number,
  freelancer_id: number
) {
  const res = await fetch("http://localhost:8080/client-jobs-assignee", {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      client_jobs_id,
      freelancer_id,
    }),
  });
  return res;
}

export async function deleteClientJobsAssignee(
  client_jobs_id: number,
  freelancer_id: number
) {
  const res = await fetch("http://localhost:8080/client-jobs-assignee", {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      client_jobs_id,
      freelancer_id,
    }),
  });
  return res;
}

export async function deleteAppliedClientJobs(
  client_jobs_id: number,
  freelancer_id: number
) {
  const res = await fetch("http://localhost:8080/applied-client-jobs", {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      client_jobs_id,
      freelancer_id,
    }),
  });
  return res;
}

export async function updateClientJobsStatus(client_jobs_id: number) {
  const res = await fetch("http://localhost:8080/client-jobs-status", {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      client_jobs_id,
    }),
  });
  return res;
}

export async function rejectOtherClientJobsAssignee(
  client_jobs_id: number,
  freelancer_id: number
) {
  const res = await fetch("http://localhost:8080/reject-assignee", {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      client_jobs_id,
      freelancer_id,
    }),
  });
  return res;
}
