import { useSelector } from "react-redux"
import { Redirect, Route, RouteProps } from "react-router"
import { IRootState } from "../redux/store"

export default function PrivateRoute({ component, ...rest }: RouteProps) {
    // const user = useSelector((state: IRootState) => state.auth.user)
    // const isAuthenticated = Object.keys(user ).length === 0
    const isAuthenticate = useSelector((state: IRootState) => state.auth.isAuthenticate)

    const Component = component
    if (Component == null) {
        return null
    }

    let render: (props: any) => JSX.Element;

    // Main Logic
    if (isAuthenticate) {
        render = (props: any) => <Component {...props}></Component>
    } else {
        render = (props: any) =>
            <Redirect
                to={{
                    pathname: "/register",
                    state: { from: props.location },
                }}
            />
    }
    // 
    return (
        <Route {...rest} render={render} />
        // <div>
        //     PrivateRoute {String(isAuthenticated)}
        // </div>
    )
}