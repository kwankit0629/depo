import {
  createStore,
  combineReducers,
  Action,
  applyMiddleware,
  compose,
} from "redux";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage"; // defaults to localStorage for web

import thunk, { ThunkDispatch } from "redux-thunk";
import logger from "redux-logger";
import {
  RouterState,
  connectRouter,
  routerMiddleware,
  CallHistoryMethodAction,
} from "connected-react-router";
import { createBrowserHistory } from "history";
import { IAuthState } from "./auth/state";
import { IAuthAction } from "./auth/actions";
import { ICategoryState } from "./category/state";
// import { authReducers } from "./auth/reducers";
import { authReducers } from "./auth/reducers";
import { categoryReducers } from "./category/reducers";
import { IJobState } from "./job/state";
import { jobReducers } from "./job/reducers";
import { IClientJobsState } from "./clientJobs/state";
import { clientJobsReducers } from "./clientJobs/reducers";

export const history = createBrowserHistory();

export type IRootThunkDispatch = ThunkDispatch<IRootState, null, IRootAction>;

declare global {
  /* tslint:disable:interface-name */
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any;
  }
}

export interface IRootState {
  router: RouterState;
  auth: IAuthState;
  category: ICategoryState;
  job: IJobState;
  clientJob: IClientJobsState;
  // freelancerInfo: IFreelancerInfoState;
}

type IRootAction = CallHistoryMethodAction | IAuthAction;

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const persistConfig = {
  key: "root",
  storage,
};
// function rootReducer<T, U, V, W>(rootReducer: any, arg1: any) {
//   throw new Error("Function not implemented.");
// }
const rootReducer = combineReducers<IRootState>({
  router: connectRouter(history),
  auth: authReducers,
  category: categoryReducers,
  job: jobReducers,
  clientJob: clientJobsReducers,
  // freelancerInfo: freelancerInfoReducers,
});
const persistedReducer = persistReducer(persistConfig, rootReducer);
const store = createStore<any, Action<any>, {}, {}>(
  persistedReducer,
  composeEnhancers(
    applyMiddleware(logger),
    applyMiddleware(thunk),
    applyMiddleware(routerMiddleware(history))
  )
);
let persistor = persistStore(store);
// export default store;
export default { store, persistor };
