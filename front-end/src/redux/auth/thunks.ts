import { Dispatch } from "redux";
import {
  fetchLogin,
  fetchRegister,
  fetchUserInfo,
  fetchFreelancerById,
  fetchRegToBeAFreelancer,
} from "../../api/auth";
import {
  authFail,
  gotFreelancerInfo,
  gotUserInfo,
  IAuthAction,
} from "./actions";
// import { CallHistoryMethodAction, push } from "connected-react-router";
import { loginSuccess, loadToken, registerSuccess } from "./actions";
import { history } from "../store";
import { IAuthState } from "./state";
import { push } from "connected-react-router";
export function loginThunk(username: string, password: string) {
  return async (dispatch: Dispatch<IAuthAction>) => {
    const res = await fetchLogin(username, password);
    const result = await res.json();
    if (res.ok) {
      localStorage.setItem("token", JSON.stringify(result));
      dispatch(gotUserInfo(result));
      console.log("login after toke : ", result);

      dispatch(loginSuccess());
      // dispatch(loadToken(result));
      history.push("/");
    }
  };
}
export function registerThunk(
  username: string,
  password: string,
  email: string
) {
  return async (dispatch: Dispatch<IAuthAction>) => {
    const res = await fetchRegister(username, password, email);
    const result = await res.json();
    if (result.success) {
      console.log("register after toke : ", result);

      // localStorage.setItem("token", result);
      dispatch(registerSuccess());
      history.push("/register?active=logIn");
    } else {
      console.log("error case");
      dispatch(authFail(result.message));
    }
  };
}
export function regToBeAFreelancerThunk(
  age: number,
  email: string,
  phone_num: string,
  photo: string,
  country: string,
  experience: string,
  introduction: string,
  technology_sum: string,
  ref_link: string,
  user_id: number
) {
  return async (dispatch: Dispatch<IAuthAction>) => {
    console.log(user_id);
    const res = await fetchRegToBeAFreelancer(
      age,
      email,
      phone_num,
      photo,
      country,
      experience,
      introduction,
      technology_sum,
      ref_link,
      user_id
    );
    if (res.ok) {
      const res = await fetchFreelancerById(user_id);
      const result = await res.json();
      dispatch(gotFreelancerInfo(result));
    }
  };
}

function reset() {
  throw new Error("Function not implemented.");
}

// export function loginFacebookThunk(accessToken: string) {
//     return async (dispatch: Dispatch<IAuthAction | CallHistoryMethodAction>) => {
//         const res = await fetchFacebookLogin(accessToken)
//         const result = await res.json()
//         if (res.ok) {
//             console.log(result)
//             localStorage.setItem('token', result)
//             dispatch(loginSuccess())
//             dispatch(loadToken(result))
//             dispatch(push('/todos'))
//         }
//     }
// }
