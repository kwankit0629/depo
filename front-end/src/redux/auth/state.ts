export interface IAuthState {
  isAuthenticate: boolean;
  msg: string;
  user: any;
  freelancer: any;
  error: any;
}

export interface JWTPayload {
  username: string;
}
