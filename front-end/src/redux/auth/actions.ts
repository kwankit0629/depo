export function test() {
  return {
    type: "TEST" as const,
  };
}

export function loadToken(token: string) {
  return {
    type: "@@Auth/LOAD_TOKEN" as const,
    token,
  };
}

export function registerSuccess() {
  return {
    type: "@@Auth/REGISTER" as const,
  };
}

export function authFail(errorMessage:string) {
  return {
    type: "@@Auth/FAIL" as const,
    errorMessage
  };
}

export function loginSuccess() {
  return {
    type: "@@Auth/LOGIN" as const,
  };
}

export function gotUserInfo(userInfo: any) {
  return {
    type: "@@Auth/GOT_USERINFO" as const,
    userInfo,
  };
}

export function gotFreelancerInfo(freelancerInfo: any) {
  return {
    type: "@@Auth/GOT_FREELANCER_INFO" as const,
    freelancerInfo,
  };
}

export type IAuthAction =
  | ReturnType<typeof test>
  | ReturnType<typeof loginSuccess>
  | ReturnType<typeof loadToken>
  | ReturnType<typeof gotUserInfo>
  | ReturnType<typeof gotFreelancerInfo>
  | ReturnType<typeof registerSuccess>
  | ReturnType<typeof authFail>
