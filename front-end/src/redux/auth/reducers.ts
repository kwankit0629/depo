import { IAuthState } from "./state";
import { IAuthAction } from "./actions";
import jwt from "jwt-decode";
import { JWTPayload } from "./state";
const initialState: IAuthState = {
  isAuthenticate: localStorage.getItem("token") != null,
  msg: "",
  user: {},
  freelancer: {},
  error:''
};

function loadToken() {
  const token = localStorage.getItem("token");
  if (token) {
    try {
      console.log("pre decode");

      const payload: JWTPayload = jwt(token);
      console.log("post decode");

      return payload.username;
    } catch (error) {
      console.log("decode error with token : ", token);
    }
  }
  return "";
}

export const authReducers = (
  state: IAuthState = initialState,
  action: IAuthAction
): IAuthState => {
  switch (action.type) {
    case "@@Auth/LOGIN":
      return {
        ...state,
        isAuthenticate: true,
      };
    case "@@Auth/GOT_USERINFO":
      const userInfo = action.userInfo;
      const { user, freelancer } = userInfo;
      console.log("user: ", userInfo);

      console.log("freelancer: ", freelancer);

      return {
        ...state,
        user,
        freelancer: freelancer,
      };
    case "@@Auth/REGISTER":
      return {...state, error :"", msg:"register success"}
    case "@@Auth/FAIL":
      const errorMessage = action.errorMessage;
      return {
        ...state,
        error: errorMessage,
      };
    case "@@Auth/GOT_FREELANCER_INFO":
      const freelancerInfo = action.freelancerInfo;
      return {
        ...state,
        freelancer: freelancerInfo,
      };
    case "@@Auth/LOAD_TOKEN":
      const payload: JWTPayload = jwt(action.token);
      const { username } = payload;
      return {
        ...state,
        user: {
          username: username,
        },
      };

    default:
      return state;
  }
};
