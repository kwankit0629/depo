import { IJobState, JobState } from "./state";
import { IJobAction } from "./actions";

const initialState: IJobState = {
  jobs: [],
  jobsPost: [],
  createFreelancerJob: [],
  createJobBookmark: [],
  gotFreelancerJobBookmark: [],
  gotClientJobBookmark: [],
  ownFreelancerPost: [],
  selectedItemId: null,
};

export const jobReducers = (
  state: IJobState = initialState,
  action: IJobAction
): IJobState => {
  switch (action.type) {
    case "@@Job/GOT_ALL_JOB":
      return {
        ...state,
        jobs: action.data,
      };
    case "@@Job/GOT_FREELANCE_POST_BY_ID":
      return {
        ...state,
        jobsPost: action.data,
      };
    case "@@Job/CREATE_FREELANCER_JOB":
      return {
        ...state,
      };
    case "@@Job/CREATE_JOB_BOOKMARK":
      return {
        ...state,
      };
    case "@@Job/GOT_FREELANCER_JOBBOOKMARK":
      return {
        ...state,
        gotFreelancerJobBookmark: action.data,
      };
    case "@@Job/CREATE_CLIENT_JOB_BOOKMARK":
      return {
        ...state,
      };
    case "@@Job/GOT_CLIENT_JOBBOOKMARK":
      return {
        ...state,
        gotClientJobBookmark: action.data,
      };
    case "@@Job/GOT_OWN_FREELANCER_POSTS":
      return {
        ...state,
        ownFreelancerPost: action.data,
      };

    case "@@Job/GOT_ALL_JOB_FAILED":
      return { ...state };
    default:
      return { ...state };
  }
};
