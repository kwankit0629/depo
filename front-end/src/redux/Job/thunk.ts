import { Dispatch } from "react";
import {
  IJobAction,
  gotAllJob,
  failed,
  gotFreelancePostById,
  createFreelancerJob,
  createJobBookmark,
  gotFreelancerJobBookmark,
  gotClientJobBookmark,
  gotOwnFreelancerPosts,
} from "./actions";
import {
  fetchGetAllJob,
  fetchFreelancerPostById,
  fetchCreateFreelancerJob,
  fetchCreateJobBookmark,
  fetchFreelancerJobBookmark,
  fetchCreateClientJobBookmark,
  fetchClientJobBookmark,
  fetchDeleteJobBookmark,
  fetchDeleteClientJobBookmark,
  fetchFreelancerOwnPostById,
} from "../../api/job";
import {
  IJobState,
  JobState,
  FreelancePostState,
  GotFreelancerJobBookmarkState,
  GotClientJobBookmarkState,
  OwnFreelancerPostState,
} from "./state";
import { IClientJobsAction } from "../clientJobs/actions";
import { IAuthState } from "../auth/state";
import { gotUserInfo, IAuthAction } from "../auth/actions";
import { fetchLogin } from "../../api/auth";
import { OwnClientJobsState } from "../clientJobs/state";

export function getAllJobThunk() {
  console.log("getAllJobThunk");
  // return {
  //   type: "@@Category/GOT_ALL_CATEGORIES" as const,
  //   data: [],
  // };

  return async (dispatch: Dispatch<IJobAction>) => {
    console.log("getAllJobThunk");
    const res = await fetchGetAllJob();
    console.log("!!!", res);
    const result: JobState[] = await res.json();
    // const items: JobState[] = result.items;

    // const categories: CategoryState[] = result;
    if (res.ok) {
      dispatch(gotAllJob(result));
    } else {
      // dispatch(failed("@@Job/GOT_ALL_JOB_FAILED", "failed to get all job"));
    }
  };
}

export function getFreelancerPostByIdThunk(id: number) {
  return async (dispatch: Dispatch<IJobAction>) => {
    const res = await fetchFreelancerPostById(id);
    console.log(res);
    const result: FreelancePostState[] = await res.json();
    if (res.ok) {
      dispatch(gotFreelancePostById(result));
    }
  };
}

export function createFreelancerJobThunk(
  freelancer_id: number,
  category_id: number,
  content: string,
  project_fee: number,
  photo: string
) {
  return async (dispatch: Dispatch<IJobAction>) => {
    const res = await fetchCreateFreelancerJob(
      freelancer_id,
      category_id,
      content,
      project_fee,
      photo
    );
    if (res.ok) {
      console.log("Surcess!!!");
      const res = await fetchFreelancerOwnPostById(freelancer_id);
      const result: OwnFreelancerPostState[] = await res.json();
      dispatch(gotOwnFreelancerPosts(result));
      // const res = await fetchGetAllClientJobs();
      // const result: ClientJobsState[] = await res.json();
      // dispatch(gotAllClientJobs(result));
      // dispatch(createFreelancerJob());
    }
  };
}

export function createJobBookmarkThunk(user_id: number, posts_id: number) {
  return async (dispatch: Dispatch<IJobAction>) => {
    const res = await fetchCreateJobBookmark(user_id, posts_id);
    if (res.ok) {
      console.log("Success!!!");
      const res = await fetchFreelancerJobBookmark(user_id);
      const result: GotFreelancerJobBookmarkState[] = await res.json();
      dispatch(gotFreelancerJobBookmark(result));
    }
  };
}

export function deleteJobBookmarkThunk(user_id: number, posts_id: number) {
  return async (dispatch: Dispatch<IJobAction>) => {
    const res = await fetchDeleteJobBookmark(user_id, posts_id);
    if (res.ok) {
      console.log("Success deleted!!!");
      const res = await fetchFreelancerJobBookmark(user_id);
      const result: GotFreelancerJobBookmarkState[] = await res.json();
      dispatch(gotFreelancerJobBookmark(result));
    }
  };
}

export function gotFreelancerJobBookmarkThunk(user_id: number) {
  return async (dispatch: Dispatch<IJobAction>) => {
    if (!user_id) {
      return;
    }
    console.log("get user_id", user_id);

    const res = await fetchFreelancerJobBookmark(user_id);
    const result: GotFreelancerJobBookmarkState[] = await res.json();
    if (res.ok) {
      dispatch(gotFreelancerJobBookmark(result));
    }
  };
}

export function gotClientJobBookmarkThunk(freelancer_id: number) {
  return async (dispatch: Dispatch<IJobAction>) => {
    if (!freelancer_id) {
      return;
    }
    console.log("get user_id", freelancer_id);

    const res = await fetchClientJobBookmark(freelancer_id);
    const result: GotClientJobBookmarkState[] = await res.json();
    if (res.ok) {
      dispatch(gotClientJobBookmark(result));
    }
  };
}

export function createClientJobBookmarkThunk(
  freelancer_id: number,
  id: number
) {
  return async (dispatch: Dispatch<IJobAction>) => {
    const res = await fetchCreateClientJobBookmark(freelancer_id, id);

    if (res.ok) {
      console.log("Success!!!");
      const res = await fetchClientJobBookmark(freelancer_id);
      const result: GotClientJobBookmarkState[] = await res.json();
      dispatch(gotClientJobBookmark(result));
    }
  };
}
export function deleteClientJobBookmarkThunk(
  freelancer_id: number,
  clientJobId: number
) {
  return async (dispatch: Dispatch<IJobAction>) => {
    const res = await fetchDeleteClientJobBookmark(freelancer_id, clientJobId);
    if (res.ok) {
      console.log("Success deleted!!!");
      const res = await fetchClientJobBookmark(freelancer_id);
      const result: GotClientJobBookmarkState[] = await res.json();
      dispatch(gotClientJobBookmark(result));
    }
  };
}

export function gotOwnFreelancerPostsThunk(freelancer_id: number) {
  return async (dispatch: Dispatch<IJobAction>) => {
    const res = await fetchFreelancerOwnPostById(freelancer_id);
    const result: OwnFreelancerPostState[] = await res.json();
    if (res.ok) {
      dispatch(gotOwnFreelancerPosts(result));
    }
  };
}
