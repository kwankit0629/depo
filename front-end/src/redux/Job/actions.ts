import {
  FreelancePostState,
  GotClientJobBookmarkState,
  GotFreelancerJobBookmarkState,
  JobState,
  OwnFreelancerPostState,
} from "./state";

export function gotAllJob(data: JobState[]) {
  return {
    type: "@@Job/GOT_ALL_JOB" as const,
    data,
  };
}

export function gotFreelancePostById(data: FreelancePostState[]) {
  return {
    type: "@@Job/GOT_FREELANCE_POST_BY_ID" as const,
    data,
  };
}

export function createFreelancerJob() {
  return {
    type: "@@Job/CREATE_FREELANCER_JOB" as const,
  };
}

export function createJobBookmark() {
  return {
    type: "@@Job/CREATE_JOB_BOOKMARK" as const,
  };
}

export function createClientJobBookmark() {
  return {
    type: "@@Job/CREATE_CLIENT_JOB_BOOKMARK" as const,
  };
}

export function gotFreelancerJobBookmark(
  data: GotFreelancerJobBookmarkState[]
) {
  return {
    type: "@@Job/GOT_FREELANCER_JOBBOOKMARK" as const,
    data,
  };
}

export function gotClientJobBookmark(data: GotClientJobBookmarkState[]) {
  return {
    type: "@@Job/GOT_CLIENT_JOBBOOKMARK" as const,
    data,
  };
}

export function gotOwnFreelancerPosts(data: OwnFreelancerPostState[]) {
  return {
    type: "@@Job/GOT_OWN_FREELANCER_POSTS" as const,
    data,
  };
}

type FAILED_INTENT = "@@Job/GOT_ALL_JOB_FAILED";

export function failed(type: FAILED_INTENT, msg: string) {
  return {
    type,
    msg,
  };
}

export type IJobAction =
  | ReturnType<typeof gotAllJob>
  | ReturnType<typeof gotFreelancePostById>
  | ReturnType<typeof createFreelancerJob>
  | ReturnType<typeof createJobBookmark>
  | ReturnType<typeof createClientJobBookmark>
  | ReturnType<typeof gotFreelancerJobBookmark>
  | ReturnType<typeof gotClientJobBookmark>
  | ReturnType<typeof gotOwnFreelancerPosts>
  | ReturnType<typeof failed>;
