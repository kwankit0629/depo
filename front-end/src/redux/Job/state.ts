export interface JobState {
  posts_id: number;
  content: string;
  postsImage: string;
  user_id: number;
  name: string;
  photo: string;
  freelancer_id: number;
  project_fee: number;
  created_at: string;
  updated_at: string;
  category_id: number;
  category_name: string;
}

export interface FreelancePostState {
  posts_id: number;
  content: string;
  postsImage: string;
  user_id: number;
  experience: string;
  phone_num: number;
  photo: string;
  country: string;
  ref_link: string;
  introduction: string;
  technology_sum: string;
  createAccount: number;
  name: string;
  email: string;
  freelancer_id: number;
  project_fee: number;
  created_at: string;
  updated_at: string;
  category_id: number;
  category_name: string;
}

export interface CreateFreelancerJobState {
  freelancer_id: number;
  category_id: number;
  content: string;
  project_fee: number;
  photo: string;
}
export interface CreateJobBookmark {
  user_id: number;
  posts_id: number;
}

export interface GotFreelancerJobBookmarkState {
  user_id: number;
  posts_id: number;
  content: string;
  postsImage: string;
  experience: string;
  phone_num: number;
  photo: string;
  country: string;
  ref_link: string;
  introduction: string;
  technology_sum: string;
  createAccount: number;
  name: string;
  email: string;
  freelancer_id: number;
  project_fee: number;
  created_at: string;
  updated_at: string;
  category_id: number;
  category_name: string;
}

export interface GotClientJobBookmarkState {
  id: number;
  phone_num: number;
  job_name: string;
  job_intro: string;
  job_requirement: string;
  budget: number;
  deadline: number;
  user_id: number;
  name: string;
  job_id: number;
  freelancer_id: number;
}

export interface JobBookmarkState {
  freelancer_id: number;
  id: number;
}

export interface OwnFreelancerPostState {
  id: number;
  content: string;
  postsImage: string;
  user_id: number;
  experience: string;
  phone_num: number | string;
  photo: string;
  country: string;
  ref_link: string;
  introduction: string;
  technology_sum: string;
  createAccount: string;
  name: string;
  email: string;
  freelancer_id: number;
  project_fee: number;
  created_at: string;
  updated_at: string;
  category_id: number;
  category_name: string;
}

export interface IJobState {
  jobs: JobState[];
  jobsPost: FreelancePostState[];
  createFreelancerJob: CreateFreelancerJobState[];
  createJobBookmark: CreateJobBookmark[];
  gotFreelancerJobBookmark: GotFreelancerJobBookmarkState[];
  gotClientJobBookmark: GotClientJobBookmarkState[];
  ownFreelancerPost: OwnFreelancerPostState[];
  selectedItemId: number | null;
}
