import { Dispatch } from "react";
import {
  ICategoryAction,
  gotAllCategories,
  failed,
  gotCatPostById,

} from "./actions";
import { fetchCategoryById, fetchGetAllCategories } from "../../api/category";
import { ICategoryState, CategoryState, CategoryPostState } from "./state";

export function getAllCategoriesThunk() {
  console.log("pre processign");
  // return {
  //   type: "@@Category/GOT_ALL_CATEGORIES" as const,
  //   data: [],
  // };

  return async (dispatch: Dispatch<ICategoryAction>) => {
    console.log("getAllCategories!!!");
    const res = await fetchGetAllCategories();
    console.log(res);
    const result: CategoryState[] = await res.json();
    // const categories: CategoryState[] = result;
    if (res.ok) {
      dispatch(gotAllCategories(result));
    } else {
      dispatch(
        failed(
          "@@Category/GOT_ALL_CATEGORIES_FAILED",
          "failed to get all categories"
        )
      );
    }
  };
}

export function getCatByIdThunk(id: number) {
  return async (dispatch: Dispatch<ICategoryAction>) => {
    const res = await fetchCategoryById(id);
    console.log(res);
    const result: CategoryPostState[] = await res.json();
    if (res.ok) {
      dispatch(gotCatPostById(result));
    }
  };
}


