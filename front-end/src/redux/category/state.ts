export interface CategoryState {
  id: number;
  name: string;
  photo: string;
}

export interface CategoryPostState {
  posts_id: number;
  content: string;
  postsImage: string;
  user_id: string;
  name: string;
  photo: string;
  freelancer_id: number;
  project_fee: number;
  created_at: string;
  updated_at: string;
  category_id: number;
  category_name: string;
  isLiked: boolean;
}

export interface ICategoryState {
  categories: CategoryState[];
  CategoryPosts: CategoryPostState[];
  selectedItemId: number | null;
}
