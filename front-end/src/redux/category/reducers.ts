import { ICategoryState, CategoryState } from "./state";
import { ICategoryAction } from "./actions";

const initialState: ICategoryState = {
  categories: [],
  CategoryPosts: [],
  selectedItemId: null,
};

export const categoryReducers = (
  state: ICategoryState = initialState,
  action: ICategoryAction
): ICategoryState => {
  switch (action.type) {
    case "@@Category/GOT_ALL_CATEGORIES":
      return {
        ...state,
        categories: action.data,
      };
    case "@@Category/GOT_ALL_CATEGORIES_BY_CAT_ID":
      return {
        ...state,
        CategoryPosts: action.data,
      };

    case "@@Category/GOT_ALL_CATEGORIES_FAILED":
      return { ...state };
    default:
      return { ...state };
  }
};
