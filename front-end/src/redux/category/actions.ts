import { CategoryPostState, CategoryState } from "./state";

export function gotAllCategories(data: CategoryState[]) {
  return {
    type: "@@Category/GOT_ALL_CATEGORIES" as const,
    data,
  };
}
export function gotCatPostById(data: CategoryPostState[]) {
  return {
    type: "@@Category/GOT_ALL_CATEGORIES_BY_CAT_ID" as const,
    data,
  };
}

// export function gotCatNameById(data: CategoryPostState[]) {
//   return {
//     type: "@@Category/GOT_CAT__BY_ID" as const,
//     data,
//   };
// }

type FAILED_INTENT = "@@Category/GOT_ALL_CATEGORIES_FAILED";

export function failed(type: FAILED_INTENT, msg: string) {
  return {
    type,
    msg,
  };
}

export type ICategoryAction =
  | ReturnType<typeof gotAllCategories>
  | ReturnType<typeof gotCatPostById>
  | ReturnType<typeof failed>;
