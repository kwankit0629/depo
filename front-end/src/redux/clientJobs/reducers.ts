import { ClientJobsState, IClientJobsState } from "./state";
import { IClientJobsAction } from "./actions";

const initialState: IClientJobsState = {
  ClientJobs: [],
  OwnClientJobs: [],
  AppliedJobs: [],
  JobsApplicants: [],
};

export const clientJobsReducers = (
  state: IClientJobsState = initialState,
  action: IClientJobsAction
): IClientJobsState => {
  switch (action.type) {
    case "@@ClientJobs/GOT_ALL_CLIENTJOBS":
      return {
        ...state,
        ClientJobs: action.data,
      };
    case "@@ClientJobs/GOT_OWN_CLIENT_JOBS":
      return {
        ...state,
        OwnClientJobs: action.data,
      };
    case "@@ClientJobs/GOT_ALL_JOBS_APPLICANTS":
      return {
        ...state,
        JobsApplicants: action.data,
      };
    case "@@ClientJobs/GOT_ALL_APPLIED_JOBS":
      return {
        ...state,
        AppliedJobs: action.data,
      };
    case "@@ClientJobs/CREATE_CLIENT_JOBS":
      return {
        ...state,
      };
    case "@@ClientJobs/GOT_ALL_CLIENTJOBS_FAILED":
      return { ...state };
    default:
      return { ...state };
  }
};
