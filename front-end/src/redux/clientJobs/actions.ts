import {
  AppliedClientJobsState,
  ClientJobsState,
  IClientJobsState,
  JobsApplicantsState,
  OwnClientJobsState,
} from "./state";

export function gotAllClientJobs(data: ClientJobsState[]) {
  return {
    type: "@@ClientJobs/GOT_ALL_CLIENTJOBS" as const,
    data,
  };
}

export function gotOwnClientJobs(data: OwnClientJobsState[]) {
  return {
    type: "@@ClientJobs/GOT_OWN_CLIENT_JOBS" as const,
    data,
  };
}

export function gotAllAppliedJobs(data: AppliedClientJobsState[]) {
  return {
    type: "@@ClientJobs/GOT_ALL_APPLIED_JOBS" as const,
    data,
  };
}

export function gotAllApplicants(data: JobsApplicantsState[]) {
  return {
    type: "@@ClientJobs/GOT_ALL_JOBS_APPLICANTS" as const,
    data,
  };
}

export function createClientJobs() {
  return {
    type: "@@ClientJobs/CREATE_CLIENT_JOBS" as const,
  };
}

type FAILED_INTENT = "@@ClientJobs/GOT_ALL_CLIENTJOBS_FAILED";

export function failed(type: FAILED_INTENT, msg: string) {
  return {
    type,
    msg,
  };
}

export type IClientJobsAction =
  | ReturnType<typeof gotAllClientJobs>
  | ReturnType<typeof gotOwnClientJobs>
  | ReturnType<typeof gotAllApplicants>
  | ReturnType<typeof gotAllAppliedJobs>
  | ReturnType<typeof createClientJobs>
  | ReturnType<typeof failed>;
