import { Dispatch } from "react";
import {
  IClientJobsAction,
  gotAllClientJobs,
  failed,
  createClientJobs,
  gotOwnClientJobs,
  gotAllApplicants,
  gotAllAppliedJobs,
} from "./actions";
import {
  deleteAppliedClientJobs,
  deleteClientJobsAssignee,
  fetchAllAppliedClientJobsInfo,
  fetchAllClientJobsApplicants,
  fetchApplyClientJobs,
  fetchCreateClientJob,
  fetchGetAllClientJobs,
  fetchOwnClientJobsById,
  rejectOtherClientJobsAssignee,
  updateClientJobsAssignee,
  updateClientJobsStatus,
} from "../../api/clientJobs";
import {
  IClientJobsState,
  ClientJobsState,
  OwnClientJobsState,
  JobsApplicantsState,
  AppliedClientJobsState,
} from "./state";

export function getAllClientJobsThunk() {
  return async (dispatch: Dispatch<IClientJobsAction>) => {
    const res = await fetchGetAllClientJobs();
    const result: ClientJobsState[] = await res.json();
    if (res.ok) {
      dispatch(gotAllClientJobs(result));
    } else {
      dispatch(
        failed(
          "@@ClientJobs/GOT_ALL_CLIENTJOBS_FAILED",
          "failed to get client Jobs"
        )
      );
    }
  };
}

export function createClientJobThunk(
  user_id: number,
  phone_num: string,
  job_name: string,
  job_intro: string,
  job_requirement: string,
  budget: number,
  deadline: number
) {
  return async (dispatch: Dispatch<IClientJobsAction>) => {
    console.log(user_id);
    const res = await fetchCreateClientJob(
      user_id,
      phone_num,
      job_name,
      job_intro,
      job_requirement,
      budget,
      deadline
    );
    if (res.ok) {
      const res = await fetchGetAllClientJobs();
      const result: ClientJobsState[] = await res.json();
      dispatch(gotAllClientJobs(result));
      dispatch(createClientJobs());
    }
  };
}

export function applyClientJobsThunk(
  client_jobs_id: number,
  freelancer_id: number
) {
  return async (dispatch: Dispatch<IClientJobsAction>) => {
    console.log("client_jobs_id =  ", client_jobs_id);
    console.log("freelancer_id =  ", freelancer_id);
    const res = await fetchApplyClientJobs(client_jobs_id, freelancer_id);
    if (res.ok) {
      const res = await fetchAllAppliedClientJobsInfo(freelancer_id);
      const result: AppliedClientJobsState[] = await res.json();
      dispatch(gotAllAppliedJobs(result));
    }
  };
}

export function getOwnClientJobsThunk(user_id: number) {
  return async (dispatch: Dispatch<IClientJobsAction>) => {
    if (!user_id) {
      return;
    }
    console.log("get own jobs user_id =  ", user_id);
    const res = await fetchOwnClientJobsById(user_id);
    const result: OwnClientJobsState[] = await res.json();
    if (res.ok) {
      dispatch(gotOwnClientJobs(result));
    }
  };
}

export function getOwnClientJobsApplicantsThunk(job_id: number) {
  return async (dispatch: Dispatch<IClientJobsAction>) => {
    console.log("Applicants:", job_id);
    const res = await fetchAllClientJobsApplicants(job_id);
    const result: JobsApplicantsState[] = await res.json();
    if (res.ok) {
      dispatch(gotAllApplicants(result));
    }
  };
}

export function getOwnAppliedClientJobsInfoThunk(freelancer_id: number) {
  return async (dispatch: Dispatch<IClientJobsAction>) => {
    console.log("Applied:", freelancer_id);
    const res = await fetchAllAppliedClientJobsInfo(freelancer_id);
    const result: AppliedClientJobsState[] = await res.json();
    if (res.ok) {
      dispatch(gotAllAppliedJobs(result));
    }
  };
}

export function updateClientJobsAssigneeThunk(
  client_jobs_id: number,
  freelancer_id: number
) {
  return async (dispatch: Dispatch<IClientJobsAction>) => {
    console.log("update client_jobs_id =  ", client_jobs_id);
    console.log("update freelancer_id =  ", freelancer_id);
    const res = await updateClientJobsAssignee(client_jobs_id, freelancer_id);
    if (res.ok) {
      const res = await fetchAllClientJobsApplicants(client_jobs_id);
      const result: JobsApplicantsState[] = await res.json();
      dispatch(gotAllApplicants(result));
    }
  };
}

export function deleteClientJobsAssigneeThunk(
  client_jobs_id: number,
  freelancer_id: number
) {
  return async (dispatch: Dispatch<IClientJobsAction>) => {
    console.log("delete client_jobs_id =  ", client_jobs_id);
    console.log("delete freelancer_id =  ", freelancer_id);
    const res = await deleteClientJobsAssignee(client_jobs_id, freelancer_id);
    if (res.ok) {
      const res = await fetchAllClientJobsApplicants(client_jobs_id);
      const result: JobsApplicantsState[] = await res.json();
      dispatch(gotAllApplicants(result));
    }
  };
}

export function deleteAppliedClientJobsThunk(
  client_jobs_id: number,
  freelancer_id: number
) {
  return async (dispatch: Dispatch<IClientJobsAction>) => {
    console.log("delete applied client_jobs_id =  ", client_jobs_id);
    console.log("delete applied freelancer_id =  ", freelancer_id);
    const res = await deleteAppliedClientJobs(client_jobs_id, freelancer_id);
    if (res.ok) {
      const res = await fetchAllAppliedClientJobsInfo(freelancer_id);
      const result: AppliedClientJobsState[] = await res.json();
      dispatch(gotAllAppliedJobs(result));
    }
  };
}

export function updateClientJobsStatusThunk(client_jobs_id: number) {
  return async (dispatch: Dispatch<IClientJobsAction>) => {
    console.log("client_jobs_id =  ", client_jobs_id);

    const res = await updateClientJobsStatus(client_jobs_id);
    if (res.ok) {
      return;
    }
  };
}

export function rejectOtherClientJobsAssigneeThunk(
  client_jobs_id: number,
  freelancer_id: number
) {
  return async (dispatch: Dispatch<IClientJobsAction>) => {
    console.log("reject applied client_jobs_id =  ", client_jobs_id);
    console.log("reject applied freelancer_id =  ", freelancer_id);
    const res = await rejectOtherClientJobsAssignee(
      client_jobs_id,
      freelancer_id
    );
    if (res.ok) {
      return;
    }
  };
}
