export interface ClientJobsState {
  id: number;
  photo_num: number;
  job_name: string;
  job_requirement: string;
  budget: number;
  deadline: number;
  user_id: number | string;
}
export interface OwnClientJobsState {
  id: number;
  photo_num: number;
  job_name: string;
  job_requirement: string;
  budget: number;
  deadline: number;
  user_id: number | string;
}

export interface AppliedClientJobsState {
  client_job_name: string;
  client_job_intro: string;
  client_job_phone: number;
  client_job_requirement: string;
  client_job_budget: number;
  client_job_id: number;
  client_job_deadline: number;
  client_job_status: string;
  application_status: string;
  client_job_assignee: string;
}
export interface JobsApplicantsState {
  freelancer_id: number;
  freelancer_name: string;
  freelancer_age: number;
  freelancer_email: string;
  freelancer_phone: string;
  freelancer_photo: string;
  freelancer_country: string;
  freelancer_experience: string;
  freelancer_introduction: string;
  freelancer_technology_sum: string;
  freelancer_ref_link: string;
  job_id: number;
}

export interface IClientJobsState {
  ClientJobs: ClientJobsState[];
  OwnClientJobs: OwnClientJobsState[];
  AppliedJobs: AppliedClientJobsState[];
  JobsApplicants: JobsApplicantsState[];
}
