import { ConnectedRouter } from "connected-react-router";
import { Switch } from "react-router";
import { Link, Route } from "react-router-dom";
import "./App.css";
import { history } from "./redux/store";
import FreelancersInfo from "./pages/FreelancersInfo";
import FreelancerProfile from "./pages/FreelancerProfilePage/FreelancerProfile";

import { Register } from "./pages/RegisterPage/register";
import Homepage from "./pages/Homepage/Homepage";
import JobsList from "./pages/JoblistPage/JobsList";
// import Button from "react-bootstrap/Button";
import CategoryDetails from "./pages/CategoryDetails/CategoryDetails";
import JobsDetails from "./pages/JosDetails/JobsDetails";
import ClientJobs from "./pages/ClientJobs/ClientJobs";
import Homepage2 from "./pages/HomePage2/Homepage2";
import FreelancerProfile2 from "./pages/FreelancerProfilePage2/FreelancerProfile2";
import RegToBeFreelancer from "./pages/RegToBeFreelancer/RegToBeFreelancer";
import CreateJobForm from "./pages/CreateJobForm/CreateJobForm";
import NavBar from "./pages/nav-bar/Navbar";
import Status from "./pages/Status/Statue";
import PrivateRoute from "./components/PrivateRoute";
import FreelancerJobsDatail from "./pages/FreelancerJobsDatail/FreelancerJobsDatail";
import SubApprovedFreelancers from "./pages/StatusSubPage/SubApprovedFreelancers/SubApprovedFreelancers";
// import "./pages/RegisterPage/register.styles.scss";
// import "./css/freelancerProfile.css";

function App() {
  return (
    <div>
      <ConnectedRouter history={history}>
        <div>
          <NavBar />
          {/* <nav className="nav-bar">
            <Link to="/" style={{ color: "black", textDecoration: "none" }}>
              Homepage
            </Link>
            <br />
            <Link to="/jobs" style={{ color: "black", textDecoration: "none" }}>
              Jobs
            </Link>
            <br />
            <Link
              to="/freelancerInfo"
              style={{ color: "black", textDecoration: "none" }}
              className="link"
            >
              Freelancers Info
            </Link>
            <br />
            <Link
              to="/freelancerProfile"
              style={{ color: "black", textDecoration: "none" }}
              className="link"
            >
              Freelancers Profile
            </Link>
            <br />
            <Link
              to="/clientJobs"
              style={{ color: "black", textDecoration: "none" }}
              className="link"
            >
              Client Jobs
            </Link>
            <br />
            <Link
              to="/login"
              style={{ color: "black", textDecoration: "none" }}
              className="link"
            >
              Login
            </Link>
            <br />
            <Link
              to="/register"
              style={{ color: "black", textDecoration: "none" }}
              className="link"
            >
              Register
            </Link>
            <br />

            <input type="text"></input>
          </nav> */}
          <div>
            <Switch>
              <Route path="/" exact={true} component={Homepage2}></Route>
              <Route path="/jobs" exact component={JobsList}></Route>
              <Route
                path="/jobs/jobdetails/:id"
                exact={true}
                component={JobsDetails}
              ></Route>
              <Route
                path="/jobs/freelancer-job-details"
                exact={true}
                component={FreelancerJobsDatail}
              ></Route>
              <Route
                path="/freelancerInfo"
                exact={true}
                component={FreelancersInfo}
              ></Route>
              <Route
                path="/jobs/category/:id"
                exact={true}
                component={CategoryDetails}
              ></Route>
              <Route
                path="/freelancerProfile"
                exact={true}
                component={FreelancerProfile}
              ></Route>
              <Route
                path="/clientJobs"
                exact={true}
                component={ClientJobs}
              ></Route>
              <Route
                path="/client-jobs-application/:id"
                exact={true}
                component={SubApprovedFreelancers}
              ></Route>
              <Route path="/register" exact={true} component={Register}></Route>
              <Route
                path="/homepage2"
                exact={true}
                component={Homepage2}
              ></Route>
              <PrivateRoute
                path="/freelancer-profile"
                exact={true}
                component={FreelancerProfile2}
              />
              <PrivateRoute
                path="/reg-to-be-freelancer"
                exact={true}
                component={RegToBeFreelancer}
              />
              <PrivateRoute
                path="/create-job-form"
                exact={true}
                component={CreateJobForm}
              />
              <PrivateRoute path="/status" exact={true} component={Status} />
            </Switch>
          </div>
        </div>
      </ConnectedRouter>
    </div>
  );
}

export default App;
