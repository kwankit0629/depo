import React, { useState } from "react";
import bookmark from "./bookmark-svgrepo-com.svg"
import bookmarked from "./bookmark-shapes-svgrepo-com.svg"
interface Props{
  toggleLike : ()=>void
  isLiked : boolean
}
function Like(props : Props) {
  
  const {toggleLike, isLiked} = props

 
    return (
      // <div className="">
        
          
          <div
            className=""
            style={{ width: "3%" }}
            onClick={() => toggleLike()}
          >
            {!isLiked  ? (
              
              // <p>good</p>
              <img src={bookmarked} alt="" />
              
              
            ) : (
              
              // <p>no  good</p>
              <img src={bookmark} alt="" />
            )}
          </div>
        
      // </div>
    );
  
}

export default Like;




