import { Application } from "express";
import { CategoryController } from "../controllers/CategoryController";
import { ClientJobsController } from "../controllers/ClientJobsController";
import { FreelancerController } from "../controllers/FreelancerController";
import { JobController } from "../controllers/JobController";
import { UserController } from "../controllers/UserController";

export interface CreateRoutesOptions {
  app: Application;
  userController: UserController;
  freelancerController: FreelancerController;
  jobController: JobController;
  categoryController: CategoryController;
  clientJobsController: ClientJobsController;
}

export interface User {
  id: number;
  username: string;
  password: string;
}
