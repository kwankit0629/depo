import multer from "multer";
import path from "path";
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.resolve("./uploads/"));
  },
  filename: function (req, file, cb) {
    cb(
      null,
      `${file.fieldname}-${file.originalname.split(".")[0]}-${Date.now()}.${
        file.mimetype.split("/")[1]
      }`
    );
  },
});
const upload = multer({ storage });

export const multerSingleImage: any = upload.single("image");
export const multerNone: any = upload.none();
