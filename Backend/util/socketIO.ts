import socketIO from "socket.io";
import express from 'express'
export let io: socketIO.Server;
export let onlineUserListObj = {}
export function setSocketIO(value: socketIO.Server) {
    let io = value;
    io.on("connection", function (socket) {
        console.log(`有client 駁左上黎${socket.id}`);

        const req = socket.request as express.Request;
        let user = req.session['user']
        if (req.session['user']  && req.session['dog']){
            let userId = user['id']
            console.log('io user:',userId );
            console.log('io dog:',req.session['dog']['name'] );

            let userToInsert = {
                userId : req.session['user']['id'],
                dogId : req.session['dog']['id'],
                socketId : socket.id
            }

            let userInOnlineObj = onlineUserListObj[userId]
            if (userInOnlineObj){
                clearTimeout(userInOnlineObj.logoutTimer)
                console.log('Cleared timer');
                
            }
            console.log('Updated online user info');

                onlineUserListObj[req.session['user']['id']]  = userToInsert

            
        }else{
            console.log('not login');
            
        }
        socket.on("disconnect", function () {
            console.log('!!!!1 disconnect now ', socket.id);
            let user = req.session['user']
            if (req.session['user']  && req.session['dog']){
                let userId = user['id']
                console.log('disconnect io user:',userId );
                console.log('disconnect io dog:',req.session['dog']['name'] );
                
                let logoutTimer  = setTimeout(() => {
                   console.log('DELETE from online list');
                   
                    delete onlineUserListObj[req.session['user']['id']]
                    
                }, 10000);
                console.log('logoutTimer = ', logoutTimer);
                
                onlineUserListObj[userId]['logoutTimer'] = logoutTimer
                
            }
    
        })
          
    });
    
}