import express from 'express'
import { Bearer } from 'permit'
import jwtSimple from 'jwt-simple';
import jwt from './jwt';
import { UserService } from '../services/UserService';
const permit = new Bearer({
})

export async function isLoggedIn(
    req: express.Request,
    res: express.Response, 
    next: express.NextFunction) {

    try {
        const token = permit.check(req)
        if (!token) {
            return res.status(200).json({ message: "Permission denied" })
        }
        const payload = jwtSimple.decode(token, jwt.jwtSecret)
        const userId = payload.id
        const user = await UserService.getUserById(userId)
        console.log({ user })
        if ({user}) {
            req['user'] = {user}
            return next()
        }
        return res.status(401).json({ message: "Permission Denied" })
    } catch (e) {
        return res.status(401).json({ message: e })
    }
}