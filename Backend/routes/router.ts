import { createUserRoutes } from "./userRoutes";
// import { createDogRoutes } from "./DogRoutes";

import { CreateRoutesOptions } from "../util/models";
// import { createPostRoutes } from "./PostRoutes";
import { onlineUserListObj } from "../util/socketIO";
import { createFreelancerRoutes } from "./freelancerRoutes";
import { createJobRoutes } from "./jobRoutes";
import { createCategoryRoutes } from "./categoriesRoutes";
import { createClientJobsRoutes } from "./clientJobsRoutes";
// import express from "express";

export function createRouter(createRoutesOptions: CreateRoutesOptions) {
  const {
    app,
    userController,
    freelancerController,
    jobController,
    categoryController,
    clientJobsController,
  } = createRoutesOptions;

  app.use("/api/users", createUserRoutes(userController));
  app.use("/", createFreelancerRoutes(freelancerController));
  app.use("/api/jobs", createJobRoutes(jobController));
  app.use("/", createCategoryRoutes(categoryController));
  app.use("/", createClientJobsRoutes(clientJobsController));
  // app.use('/', createPostRoutes(postController))
  app.use("/online-user", (req, res) => {
    res.json(`online user : ${Object.keys(onlineUserListObj).length}`);
  });
}
