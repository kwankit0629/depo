import express from "express";
import { FreelancerController } from "../controllers/FreelancerController";
import { UserController } from "../controllers/UserController";
import { multerSingleImage } from "../util/multer";

export function createFreelancerRoutes(
  freelancerController: FreelancerController
) {
  const freelancerRouter = express.Router();
  freelancerRouter.get("/freelancer", freelancerController.getAllFreelancer);
  freelancerRouter.get(
    "/freelancerPost",
    freelancerController.getAllFreelancerPosts
  );
  freelancerRouter.get("/ownFreelancerPosts", freelancerController.getOwnPosts);
  freelancerRouter.get(
    "/otherFreelancerPosts",
    freelancerController.getOtherFreelancerPosts
  );
  freelancerRouter.post(
    "/register",
    multerSingleImage,
    freelancerController.regToBeAFreelancer
  );
  freelancerRouter.get(
    "/freelancerId/:userId",
    freelancerController.getFreelancerById
  );
  return freelancerRouter;
}
