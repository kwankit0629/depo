// routes

import express from "express";
import { UserController } from "../controllers/UserController";
import { knex } from "../db";
import { UserService } from "../services/UserService";
import { multerSingleImage } from "../util/multer";
export const userService = new UserService(knex);
const userController = new UserController(userService);
export function createUserRoutes(userController: UserController) {
  const userRouter = express.Router();

  userRouter.get("/me", userController.me);
  userRouter.get("/getalluser", userController.getAllUsers);
  // userRouter.get("/login/google", userController.loginGoogle);
  userRouter.post("/login", userController.login);
  userRouter.post("/logout", userController.logout);
  userRouter.post("/newuser", multerSingleImage, userController.createUser);
  userRouter.post("/jobsbookmark", userController.createJobBookmark);
  userRouter.post("/deletejobsbookmark", userController.removeJobBookmark);
  userRouter.get(
    "/freelancerjobbookmarked/:user_id",
    userController.getFreelancerBookmarkedPost
  );
  userRouter.get(
    "/clientjobbookmarked/:freelancer_id",
    userController.getClientBookmarkedPost
  );

  return userRouter;
}
