import express from "express";
import { JobController } from "../controllers/JobController";
import { multerSingleImage } from "../util/multer";

export function createJobRoutes(jobController: JobController) {
  const jobRouter = express.Router();
  // jobRouter.get("/jobsbookmark", jobController.getJobBookmarksById);
  jobRouter.get("/freelancerpost", jobController.getAllFreelancerPost);
  jobRouter.get("/freelancerpost/:id", jobController.getFreelancerPostById);
  jobRouter.post(
    "/createfreelancerjob",
    multerSingleImage,
    jobController.createFreelancerJobs
  );
  jobRouter.get(
    "/freelancerpostbyfreelancerid/:id",
    jobController.getFreelancerPostByFreelancerId
  );

  return jobRouter;
}
