import express from "express";
import { multerSingleImage } from "../util/multer";
import { CategoryController } from "../controllers/CategoryController";

export function createCategoryRoutes(categoryController: CategoryController) {
  const categoryRouter = express.Router();
  categoryRouter.get("/category", categoryController.getAllCategory);
  categoryRouter.get(
    "/category/:id",
    categoryController.getFreelancerPostByCat
  );
  
  return categoryRouter;
}
