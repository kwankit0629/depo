import express from "express";
import { ClientJobsController } from "../controllers/ClientJobsController";
import { multerSingleImage } from "../util/multer";

export function createClientJobsRoutes(
  ClientJobsController: ClientJobsController
) {
  const clientJobsRouter = express.Router();
  clientJobsRouter.get("/clientJobs", ClientJobsController.getAllJobs);
  clientJobsRouter.post("/newClientJob", ClientJobsController.createClientJobs);
  clientJobsRouter.post("/application", ClientJobsController.applyCLientJobs);
  clientJobsRouter.post(
    "/clientjobbookmark",
    ClientJobsController.createClientJobBookmark
  );
  clientJobsRouter.delete(
    "/deleteclientjobsbookmark",
    ClientJobsController.removeClientJobBookmark
  );
  clientJobsRouter.get(
    "/clientId/:userId",
    ClientJobsController.getClientJobsById
  );
  clientJobsRouter.get(
    "/client-jobs-application/:id",
    ClientJobsController.getAllClientJobsApplicationsInfoById
  );
  clientJobsRouter.get(
    "/client-jobs-applied/:id",
    ClientJobsController.getAllAppliedClientJobsInfoById
  );
  clientJobsRouter.put(
    "/client-jobs-assignee",
    ClientJobsController.updateClientJobsAssignee
  );
  clientJobsRouter.put(
    "/client-jobs-status",
    ClientJobsController.updateClientJobsStatus
  );
  clientJobsRouter.delete(
    "/client-jobs-assignee",
    ClientJobsController.deleteClientJobsAssignee
  );
  clientJobsRouter.delete(
    "/applied-client-jobs",
    ClientJobsController.deleteAppliedClientJobs
  );
  clientJobsRouter.put(
    "/reject-assignee",
    ClientJobsController.rejectOtherClientJobsAssignee
  );
  return clientJobsRouter;
}
