import express, { NextFunction } from "express";
import { Request, Response } from "express";
import expressSession from "express-session";

import { knex } from "./db";
import path from "path";

import { grantExpress } from "./util/middleware";
import env from "./util/env";
import { isLoggedIn } from "./util/guards";

import { CreateRoutesOptions } from "./util/models";
import { UserController } from "./controllers/UserController";
import { UserService } from "./services/UserService";
import { createRouter } from "./routes/router";
import http from "http";
import { Server as SocketIO } from "socket.io";
import { setSocketIO } from "./util/socketIO";
import { FreelancerService } from "./services/freelancerService";
import { FreelancerController } from "./controllers/FreelancerController";
import { JobService } from "./services/JobService";
import { JobController } from "./controllers/JobController";
import { CategoryController } from "./controllers/CategoryController";
import { CategoryService } from "./services/categoryService";
import { ClientJobsController } from "./controllers/ClientJobsController";
import { ClientJobsService } from "./services/clientJobsService";

import cors from "cors";
export const app = express();
const server = new http.Server(app);

const io = new SocketIO(server);
const sessionMiddleware = expressSession({
  secret: "Tecky Academy teaches typescript",
  resave: true,
  saveUninitialized: true,
  cookie: { secure: false },
});
io.use((socket, next) => {
  let req = socket.request as express.Request;
  let res = req.res as express.Response;
  sessionMiddleware(req, res, next as express.NextFunction);
});
// cookies
app.use(sessionMiddleware);
app.use(cors());

// // 教express 識睇 JSON 既req.body
app.use(express.json());

// app.get("/", (req: Request, res: Response) => {
//   res.sendFile(path.resolve(__dirname, "public/login.html"));
// });

app.use(grantExpress as express.RequestHandler);

setSocketIO(io);

let userService = new UserService(knex);
let userController = new UserController(userService);
let freelancerService = new FreelancerService(knex);
let freelancerController = new FreelancerController(freelancerService);
let jobService = new JobService(knex);
let jobController = new JobController(jobService);
let categoryService = new CategoryService(knex);
let categoryController = new CategoryController(categoryService);
let clientJobsService = new ClientJobsService(knex);
let clientJobsController = new ClientJobsController(clientJobsService);

let createRoutesOptions: CreateRoutesOptions = {
  app,
  userController,
  freelancerController,
  jobController,
  categoryController,
  clientJobsController,
};

// app.use((req: Express.Request, res: Express.Response, next: NextFunction) => {
//   req.session["user"] = {
//     id: "3",
//     name: "Alex",
//   };
//   req.session["freelancer"] = {};
//   next();
// });

createRouter(createRoutesOptions);

app.use(express.static("public"));
app.use("/upload", express.static("uploads"));

// app.use(isLoggedIn, express.static("protected"));

server.listen(env.PORT, () => {
  console.log(`Listening at http://localhost:${env.PORT}/`);
});
