import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
  // Deletes ALL existing entries
  // await knex("users").del();
  // await knex("category").del();
  // await knex("freelancers").del();
  // await knex("user_bookmark").del();
  // await knex("client_jobs").del();
  // await knex("jobs_bookmark").del();
  // await knex("freelancer_category").del();
  // await knex("freelancer_posts").del();
  // await knex("comments").del();
  // await knex("client_jobs_application").del();
  // await knex("direct_messages").del();

  await knex.raw(
    "delete from freelancer_jobs_application; ALTER SEQUENCE freelancer_jobs_application_id_seq RESTART WITH 1;"
  );
  await knex.raw(
    "delete from direct_messages; ALTER SEQUENCE direct_messages_id_seq RESTART WITH 1;"
  );
  await knex.raw(
    "delete from client_jobs_application; ALTER SEQUENCE client_jobs_application_id_seq RESTART WITH 1;"
  );
  await knex.raw(
    "delete from comments; ALTER SEQUENCE comments_id_seq RESTART WITH 1;"
  );
  await knex.raw(
    "delete from freelancer_posts; ALTER SEQUENCE freelancer_posts_id_seq RESTART WITH 1;"
  );
  await knex.raw(
    "delete from freelancer_category; ALTER SEQUENCE freelancer_category_id_seq RESTART WITH 1;"
  );
  await knex.raw(
    "delete from jobs_bookmark; ALTER SEQUENCE jobs_bookmark_id_seq RESTART WITH 1;"
  );
  await knex.raw(
    "delete from client_jobs; ALTER SEQUENCE client_jobs_id_seq RESTART WITH 1;"
  );
  await knex.raw(
    "delete from user_bookmark; ALTER SEQUENCE user_bookmark_id_seq RESTART WITH 1;"
  );

  await knex.raw(
    "delete from freelancers; ALTER SEQUENCE freelancers_id_seq RESTART WITH 1;"
  );
  await knex.raw(
    "delete from category; ALTER SEQUENCE category_id_seq RESTART WITH 1;"
  );

  await knex.raw(
    "delete from users; ALTER SEQUENCE users_id_seq RESTART WITH 1;"
  );

  // Inserts seed entries

  await knex("users").insert([
    {
      name: "Nathan",
      password: "$2a$10$ED4VS16SNGg5MjTu7aZBnuDWvhdAFD5ZWl/p7JytxiRv77226T.a.",
      email: "tecky1@abc.com",
    },
    {
      name: "Joe",
      password: "$2a$10$ED4VS16SNGg5MjTu7aZBnuDWvhdAFD5ZWl/p7JytxiRv77226T.a.",
      email: "tecky2@abc.com",
    },
    {
      name: "Alex",
      password: "$2a$10$ED4VS16SNGg5MjTu7aZBnuDWvhdAFD5ZWl/p7JytxiRv77226T.a.",
      email: "tecky3@abc.com",
    },
    {
      name: "Michael",
      password: "$2a$10$ED4VS16SNGg5MjTu7aZBnuDWvhdAFD5ZWl/p7JytxiRv77226T.a.",
      email: "tecky4@abc.com",
    },
    {
      name: "Dickson",
      password: "$2a$10$ED4VS16SNGg5MjTu7aZBnuDWvhdAFD5ZWl/p7JytxiRv77226T.a.",
      email: "tecky5@abc.com",
    },
    {
      name: "Aaron",
      password: "$2a$10$ED4VS16SNGg5MjTu7aZBnuDWvhdAFD5ZWl/p7JytxiRv77226T.a.",
      email: "aaron@abc.com",
    },
    {
      name: "Jacky",
      password: "$2a$10$ED4VS16SNGg5MjTu7aZBnuDWvhdAFD5ZWl/p7JytxiRv77226T.a.",
      email: "jacky@abc.com",
    },
    {
      name: "Tom",
      password: "$2a$10$ED4VS16SNGg5MjTu7aZBnuDWvhdAFD5ZWl/p7JytxiRv77226T.a.",
      email: "tom@abc.com",
    },
    {
      name: "Mary",
      password: "$2a$10$ED4VS16SNGg5MjTu7aZBnuDWvhdAFD5ZWl/p7JytxiRv77226T.a.",
      email: "mary@abc.com",
    },
    {
      name: "Apple",
      password: "$2a$10$ED4VS16SNGg5MjTu7aZBnuDWvhdAFD5ZWl/p7JytxiRv77226T.a.",
      email: "apple@abc.com",
    },
  ]);

  await knex("category").insert([
    {
      name: "WordPress",
      photo: "catwordpress.png",
    },
    {
      name: "Website Builders & CMS",
      photo: "catcms.jpg",
    },
    {
      name: "Game Development",
      photo: "catgame.jpg",
    },
    {
      name: "Development for Streamers",
      photo: "catstream.jpg",
    },
    {
      name: "Web Programming",
      photo: "catwebprogramming.jpg",
    },
    {
      name: "E-Commerce Development",
      photo: "catecommerce.jpg",
    },
    {
      name: "Mobile Apps",
      photo: "catmobileapp.jpg",
    },
    {
      name: "Desktop Applications",
      photo: "catdesktopapp.jpg",
    },
    {
      name: "Chatbots",
      photo: "catchatbox.jpg",
    },
    {
      name: "Support & IT",
      photo: "catitsupport.png",
    },
    {
      name: "Cybersecurity & Data Protection",
      photo: "catcybersecurity.jpg",
    },
    {
      name: "Electronics Engineering",
      photo: "catelectronic.jpg",
    },
    {
      name: "Convert Files",
      photo: "catconvertfiles.jpg",
    },
    {
      name: "User Testing",
      photo: "catusertesting.png",
    },
    {
      name: "QA & Review",
      photo: "catqa.jpg",
    },
    {
      name: "Blockchain & Cryptocurrency",
      photo: "catblockchain.png",
    },
    {
      name: "Databases",
      photo: "catdatabase.png",
    },
    {
      name: "Data Processing",
      photo: "catdataprocessing.jpg",
    },
    {
      name: "Data Engineering",
      photo: "catdataengineering.png",
    },
    {
      name: "Data Science",
      photo: "catdatascience.jpg",
    },
  ]);

  await knex("freelancers").insert([
    {
      age: 30,
      email: "nathan190@gmail.com",
      phone_num: 72345678,
      photo: "Nathan.jpg",
      country: "Hong Kong",
      experience:
        "Graduated at HKUST computer science, work at Facebook before , focusing on UI design and UX design",
      introduction:
        "I will complete your project to your absolute satisfaction and ON-TIME. If you are interested in working with me I am available to contact 24/7.",
      technology_sum:
        "JS, TS, React , ReactNative, Java , HTML , CSS , Node Js",
      ref_link: "https://gitlab.com/nathanlochunho",
      user_id: 1,
      created_at: "2021-03-17 15:42:34.019 +0800",
    },
    {
      age: 35,
      email: "joe@yahoo.com",
      phone_num: 52345678,
      photo: "joe.jpg",
      country: "Hong Kong",
      experience:
        "Graduated at Tecky, only 4 months experiences on programming",
      introduction:
        "Although I only have 4 months experiences on programming , but full of passion , can finish your project with lower price than others, please give me a chance.",
      technology_sum: "JS, TS, React , Postgres ,HTML , CSS , Node Js",
      ref_link: "https://gitlab.com/joekit",
      user_id: 2,
      created_at: "2022-01-17 15:42:34.019 +0800",
    },
    {
      age: 36,
      email: "alex@abc.com",
      phone_num: 22345678,
      photo: "alex.jpg",
      country: "Hong Kong",
      experience:
        "Self-study programming , become programming freelancer for five years, finished more than 200 projects!",
      introduction:
        " I have more than 5 years of experience in WordPress and web development. Today everyone needs a professional website and I'm here to know your business and make it grow up together!",
      technology_sum:
        "JS, TS, React, WordPress, ReactNative , Jave , PHP ,HTML , CSS , Node Js",
      ref_link: "https://gitlab.com/alexwong",
      user_id: 3,
      created_at: "2021-09-17 15:42:34.019 +0800",
    },
    {
      age: 25,
      email: "micheal@abc.com",
      phone_num: 52345678,
      photo: "micheal.jpg",
      country: "Hong Kong",
      experience:
        "Graduated at HKU computer science, work at google before, focusing mobile application",
      introduction:
        "Developing iOS and Android application for about 8 Years and counting. With additional expertise in web-services, Windows Form, WPF and so much more :P",
      technology_sum:
        "JS, TS, React, TensorFlow , Python ,HTML , CSS , Node Js",
      ref_link: "https://gitlab.com/nathanlochunho",
      user_id: 4,
      created_at: "2019-03-17 15:42:34.019 +0800",
    },
    {
      age: 26,
      email: "dickson@gmail.com",
      phone_num: 12345678,
      photo: "dickson.jpg",
      country: "Hong Kong",
      experience:
        " Five years of full-stack programming experience in the banking and FinTech industries after graduating from Computer Science in HKUST.",
      introduction:
        "Tecky instrutor, Full of experience , participate a lot of projects finish the job on-time no matter how diffcult the project is.",
      technology_sum:
        "JS, TS, React, TensorFlow , Python ,ReactNative, TensorFlow, PHP, AWS , SOCKET_IO, MongoDB, HTML , CSS , Node Js",
      ref_link: "https://gitlab.com/dickson",
      user_id: 5,
      created_at: "2018-03-17 15:42:34.019 +0800",
    },
    {
      age: 48,
      email: "aaron@gmail.com",
      phone_num: 23345678,
      photo: "aaron.jpg",
      country: "UK",
      experience:
        " More than 20 years experience in Computer Science.Now focusing on developing Mobile apps!",
      introduction:
        "If you are looking for a Top-Notch Developer to build an Android/iOS Mobile App? You are at the right place.",
      technology_sum:
        "JS, TS, React, TensorFlow , Python ,ReactNative, TensorFlow, PHP, AWS , SOCKET_IO, MongoDB",
      ref_link: "https://gitlab.com/aaron",
      user_id: 6,
      created_at: "2022-02-17 15:42:34.019 +0800",
    },
    {
      age: 38,
      email: "jacky@gmail.com",
      phone_num: 24345678,
      photo: "jacky.jpg",
      country: "USA",
      experience:
        " Hello Dear Customer. I have 11 years of Experience in Game Designing and Developing, Pixel Art, and Graphic Design.",
      introduction:
        " I can work fast and professionally with Unity3d, Aftereffects, Photoshop, Illustrator. Your satisfaction is always my goal and I will work hard to produce good stuff for you. :)",
      technology_sum: " Unity , Js , C+ , Mysql , ReactNative",
      ref_link: "https://gitlab.com/jacky",
      user_id: 7,
      created_at: "2022-03-16 15:42:34.019 +0800",
    },
    {
      age: 27,
      email: "tom@gmail.com",
      phone_num: 54345678,
      photo: "tom.jpg",
      country: "USA",
      experience:
        " I'm working in UAE Cloud based company to manage hosting servers since past three years.",
      introduction:
        "This is me Tom cheung, I can provide you the best domain and web hosting solution for your business website. Feel free to contact now!",
      technology_sum:
        " Unity , Js , C+ , Mysql , ReactNative , Mongo DB , AWS , JAVA ,",
      ref_link: "https://gitlab.com/tom",
      user_id: 8,
      created_at: "2021-09-17 01:42:34.019 +0800",
    },
    {
      age: 22,
      email: "mary@gmail.com",
      phone_num: 74345678,
      photo: "mary.jpg",
      country: "HONG KONG",
      experience: " HKU Computer Science Year2 Student.",
      introduction:
        "A HKU Computer Science student who want to grow up in freelance activity. Ask me if you have a project !!",
      technology_sum:
        " Java , C+ , Mysql , ReactNative , Mongo DB , AWS , JS ,",
      ref_link: "https://gitlab.com/mary",
      user_id: 9,
      created_at: "2021-08-01 15:42:34.019 +0800",
    },
    {
      age: 30,
      email: "apple@gmail.com",
      phone_num: 74345678,
      photo: "apple.jpg",
      country: "HONG KONG",
      experience:
        "I'm a software engineer with more than 4 years of professional experience.",
      introduction:
        "I have worked for various renowned companies and am currently working as a software engineer for Fenix and Emumba.",
      technology_sum:
        " Java , C+ , Mysql , ReactNative , Mongo DB , AWS , JS ,HTML , CSS , Node Js",
      ref_link: "https://gitlab.com/mary",
      user_id: 10,
      created_at: "2022-03-17 08:42:34.019 +0800",
    },
  ]);

  await knex("user_bookmark").insert([
    {
      users_id: 1,
      freelancer_id: 2,
    },
    {
      users_id: 1,
      freelancer_id: 3,
    },
    {
      users_id: 2,
      freelancer_id: 3,
    },
    {
      users_id: 2,
      freelancer_id: 4,
    },
    {
      users_id: 3,
      freelancer_id: 2,
    },
    {
      users_id: 3,
      freelancer_id: 1,
    },
  ]);

  await knex("client_jobs").insert([
    {
      user_id: 1,
      phone_num: 62565678,
      job_name: "Looking a programmer who are good at build apps",
      job_intro:
        "I want to build a dating app , it must be universal at ios and android systems. ",
      job_requirement: "Similar to Tinder, and it must have a chatroom! ",
      budget: 200000,
      deadline: 60,
      status: "open",
    },
    {
      user_id: 2,
      phone_num: 92345778,
      job_name: "Looking for programmer who are good at Database",
      job_intro:
        "I'm trying to build a stock website , but I'm not really good at database design",
      job_requirement:
        "Design a database diagram for me, and prepare the sql for me",
      budget: 1000,
      deadline: 4,
      status: "open",
    },
    {
      user_id: 3,
      phone_num: 92335678,
      job_name: "Looking for a python teacher",
      job_intro:
        "I'm learning python, it is difficult to learn it by using english",
      job_requirement: "More than 50hrs video and teach it by Cantonese",
      budget: 800,
      deadline: 14,
      status: "open",
    },
    {
      user_id: 4,
      phone_num: 72345678,
      job_name: "Finding someone to build a e-commerce website",
      job_intro:
        "Our company focus on selling mask, and we want to build our own e-commerce website",
      job_requirement: "No shopify!!! ",
      budget: 200000,
      deadline: 21,
      status: "open",
    },
    {
      user_id: 5,
      phone_num: 82345678,
      job_name: "Finding someone to develop a NFT project",
      job_intro:
        "I want to develop a NFT Card game , I will provide the card for you",
      job_requirement: "Fullstack Talent",
      budget: 200000,
      deadline: 100,
      status: "open",
    },
    {
      user_id: 6,
      phone_num: 92345678,
      job_name: "Finding someone to develop a multiplayer game project",
      job_intro:
        "I want to develop a online arpg game , I will provide the design for you",
      job_requirement: "Who are good at Unity",
      budget: 100000,
      deadline: 100,
      status: "open",
    },
  ]);

  await knex("jobs_bookmark").insert([
    {
      job_id: 1,
      freelancer_id: 2,
    },
    {
      job_id: 1,
      freelancer_id: 3,
    },
    {
      job_id: 2,
      freelancer_id: 3,
    },
    {
      job_id: 2,
      freelancer_id: 1,
    },
    {
      job_id: 3,
      freelancer_id: 2,
    },
    {
      job_id: 3,
      freelancer_id: 1,
    },
  ]);

  await knex("freelancer_category").insert([
    {
      freelancer_id: 1,
      category_id: 1,
    },
    {
      freelancer_id: 1,
      category_id: 2,
    },
    {
      freelancer_id: 1,
      category_id: 3,
    },
    {
      freelancer_id: 2,
      category_id: 1,
    },
    {
      freelancer_id: 2,
      category_id: 2,
    },
    {
      freelancer_id: 2,
      category_id: 3,
    },
    {
      freelancer_id: 3,
      category_id: 3,
    },
    {
      freelancer_id: 3,
      category_id: 4,
    },
    {
      freelancer_id: 3,
      category_id: 5,
    },
    {
      freelancer_id: 4,
      category_id: 5,
    },
    {
      freelancer_id: 4,
      category_id: 6,
    },
  ]);

  await knex("freelancer_posts").insert([
    {
      freelancer_id: 1,
      content: "I can design a grate Wordpress website for you with any topic",
      photo: "wordpress.jpg",
      category_id: 1,
      project_fee: 2000,
    },
    {
      freelancer_id: 7,
      content:
        "I will build modern wordpress website and ecommerce online store",
      photo: "wordpress2.png",
      category_id: 1,
      project_fee: 1500,
    },
    {
      freelancer_id: 3,
      content:
        "I will build a modern wordpress website with a unique web design",
      photo: "wordpress3.png",
      category_id: 1,
      project_fee: 500,
    },
    {
      freelancer_id: 8,
      content:
        "I will design professional and responsive wordpress website in 24hr",
      photo: "wordpress4.png",
      category_id: 1,
      project_fee: 500,
    },
    {
      freelancer_id: 6,
      content: "I will create a unique and responsive website",
      photo: "Website_Builders_CMS1.png",
      category_id: 2,
      project_fee: 700,
    },
    {
      freelancer_id: 2,
      content: "I will edit and redesign your squarespace website",
      photo: "Website_Builders_CMS2.jpg",
      category_id: 2,
      project_fee: 500,
    },
    {
      freelancer_id: 5,
      content:
        "I will create high converting custom unbounce landing page, wix, shopify page,wordpress",
      photo: "Website_Builders_CMS3.png",
      category_id: 2,
      project_fee: 850,
    },
    {
      freelancer_id: 3,
      content: "I will design and develop fully responsive website",
      photo: "Website_Builders_CMS4.jpg",
      category_id: 2,
      project_fee: 850,
    },
    {
      freelancer_id: 5,
      content:
        "I will provide unity game development, monetization and multiplayer.",
      photo: "GameDevelopment1.jpeg",
      category_id: 3,
      project_fee: 50000,
    },
    {
      freelancer_id: 4,
      content: "I will develop and design 2d games",
      photo: "GameDevelopment2.jpeg",
      category_id: 3,
      project_fee: 10000,
    },
    {
      freelancer_id: 10,
      content: "I will develop and design money making games",
      photo: "GameDevelopment3.jpeg",
      category_id: 3,
      project_fee: 20000,
    },
    {
      freelancer_id: 7,
      content: "I will develop and design 2d games",
      photo: "GameDevelopment4.jpg",
      category_id: 3,
      project_fee: 5000,
    },
    {
      freelancer_id: 3,
      content:
        "I will sell and design 3 match candy crush unity game development",
      photo: "GameDevelopment5.jpg",
      category_id: 3,
      project_fee: 15000,
    },
    {
      freelancer_id: 2,
      content:
        "I will give you a pro twitch stream setup and get more viewers.",
      photo: "DevelopmentforStreamers1.png",
      category_id: 4,
      project_fee: 500,
    },
    {
      freelancer_id: 4,
      content: "I will develop lg TV, live, android TV, streaming TV app.",
      photo: "DevelopmentforStreamers2.png",
      category_id: 4,
      project_fee: 5000,
    },
    {
      freelancer_id: 7,
      content:
        "I will develop live streaming app live streaming app live streaming website.",
      photo: "DevelopmentforStreamers3.png",
      category_id: 4,
      project_fee: 2000,
    },
    {
      freelancer_id: 5,
      content:
        "I will help you set up and learn how to live stream on streamelements obs",
      photo: "DevelopmentforStreamers4.png",
      category_id: 4,
      project_fee: 500,
    },
    {
      freelancer_id: 2,
      content: "I will develop complete software or web applications",
      photo: "WebProgramming1.jpg",
      category_id: 5,
      project_fee: 10000,
    },
    {
      freelancer_id: 4,
      content: "I will develop and design PHP website or web applications",
      photo: "WebProgramming2.jpeg",
      category_id: 5,
      project_fee: 20000,
    },
    {
      freelancer_id: 6,
      content: "I will develop a web application with a mysql database",
      photo: "WebProgramming3.png",
      category_id: 5,
      project_fee: 8000,
    },
    {
      freelancer_id: 8,
      content: "I will create any web service in python, go or java",
      photo: "WebProgramming4.jpg",
      category_id: 5,
      project_fee: 12000,
    },
    {
      freelancer_id: 10,
      content: "I will do web development in react js, javascript and node js",
      photo: "WebProgramming5.jpg",
      category_id: 5,
      project_fee: 5000,
    },
    {
      freelancer_id: 1,
      content: "I will integrate any api or payment gateway for you",
      photo: "WebProgramming6.jpg",
      category_id: 5,
      project_fee: 30000,
    },
    {
      freelancer_id: 3,
      content: "I will be your front end web developer, html css bootstrap",
      photo: "WebProgramming7.jpg",
      category_id: 5,
      project_fee: 10000,
    },
    {
      freelancer_id: 5,
      content:
        "I will be your full stack web developer in angular, react, nodejs express, php laravel",
      photo: "WebProgramming8.jpg",
      category_id: 5,
      project_fee: 5000,
    },
    {
      freelancer_id: 7,
      content:
        "I will convert xd to html, sketch to html, psd to html responsive bootstrap 4",
      photo: "WebProgramming9.jpg",
      category_id: 5,
      project_fee: 4000,
    },
    {
      freelancer_id: 9,
      content: "I will be your web UI designer and developer",
      photo: "WebProgramming10.jpg",
      category_id: 5,
      project_fee: 4000,
    },

    {
      freelancer_id: 8,
      content:
        "I will design ecommerce website online store with wordpress woocommerce",
      photo: "ECommerceDevelopment1.jpg",
      category_id: 6,
      project_fee: 5000,
    },
    {
      freelancer_id: 2,
      content: "I will build a shopify store for your brand",
      photo: "ECommerceDevelopment2.jpg",
      category_id: 6,
      project_fee: 12000,
    },
    {
      freelancer_id: 6,
      content: "I will design ecommerce website online store without shopify",
      photo: "ECommerceDevelopment3.jpg",
      category_id: 6,
      project_fee: 10000,
    },
    {
      freelancer_id: 8,
      content: "I will build an outstanding ecommerce website for you",
      photo: "ECommerceDevelopment4.png",
      category_id: 6,
      project_fee: 50000,
    },
    {
      freelancer_id: 1,
      content:
        "I will design android mobile app, ios mobile app, and mobile app development",
      photo: "MobileApps1.jpg",
      category_id: 7,
      project_fee: 50000,
    },
    {
      freelancer_id: 4,
      content: "I will develop a high quality mobile android and ios app",
      photo: "MobileApps2.jpg",
      category_id: 7,
      project_fee: 20000,
    },
    {
      freelancer_id: 8,
      content: "I will build an outstanding ecommerce website for you",
      photo: "MobileApps3.jpeg",
      category_id: 7,
      project_fee: 50000,
    },
    {
      freelancer_id: 9,
      content:
        "I will develop cross platform mobile apps with flutter and dart",
      photo: "MobileApps4.jpg",
      category_id: 7,
      project_fee: 50000,
    },
    {
      freelancer_id: 3,
      content: "I will develop cloud and desktop applications for you",
      photo: "DesktopApplications1.png",
      category_id: 8,
      project_fee: 30000,
    },
    {
      freelancer_id: 4,
      content: "I will do java programming task,projects and softwares",
      photo: "DesktopApplications2.png",
      category_id: 8,
      project_fee: 20000,
    },
    {
      freelancer_id: 6,
      content:
        "I will code your linux c cpp python java csharp programming projects",
      photo: "DesktopApplications3.jpg",
      category_id: 8,
      project_fee: 15000,
    },
    {
      freelancer_id: 8,
      content: "I will develop python trading bot",
      photo: "DesktopApplications4.jpg",
      category_id: 8,
      project_fee: 30000,
    },
    {
      freelancer_id: 3,
      content: "I will create airdrop telegram bot for your company",
      photo: "Chatbots1.png",
      category_id: 9,
      project_fee: 5000,
    },
    {
      freelancer_id: 8,
      content: "I will create auto type discord bot",
      photo: "Chatbots2.png",
      category_id: 9,
      project_fee: 8000,
    },
    {
      freelancer_id: 5,
      content: "I will create facebook messenger chatbot in manychat, chatfuel",
      photo: "Chatbots3.png",
      category_id: 9,
      project_fee: 20000,
    },
    {
      freelancer_id: 7,
      content:
        "I will create a discord bot in python and pass you the source codes",
      photo: "Chatbots4.png",
      category_id: 9,
      project_fee: 5000,
    },
    {
      freelancer_id: 4,
      content: "I will install wowza engine with ffmpeg hls streaming rtmp",
      photo: "ITSupport1.jpg",
      category_id: 10,
      project_fee: 500,
    },
    {
      freelancer_id: 5,
      content: "I will fix cpanel whm plesk server vps issues",
      photo: "ITSupport2.jpg",
      category_id: 10,
      project_fee: 200,
    },
    {
      freelancer_id: 7,
      content:
        "I will do full hubspot CRM setup for starter, pro, enterprise plan",
      photo: "ITSupport3.png",
      category_id: 10,
      project_fee: 700,
    },
    {
      freelancer_id: 6,
      content: "I will do VA,penetration testing for your website IP address",
      photo: "ITSupport4.png",
      category_id: 10,
      project_fee: 500,
    },
    {
      freelancer_id: 3,
      content:
        "I will conduct penetration testing and vulnerability assessment ",
      photo: "CybersecurityDataProtection1.jpg",
      category_id: 11,
      project_fee: 50000,
    },
    {
      freelancer_id: 10,
      content: "I will review or create an incident response runbook",
      photo: "CybersecurityDataProtection2.jpeg",
      category_id: 11,
      project_fee: 7000,
    },
    {
      freelancer_id: 9,
      content:
        "I will help you to build your cybersecurity infrastructure strategy at optimal cost",
      photo: "CybersecurityDataProtection3.jpg",
      category_id: 11,
      project_fee: 6000,
    },
    {
      freelancer_id: 8,
      content:
        "I will perform a security audit, eliminate vulnerabilities and protect infrastructure",
      photo: "CybersecurityDataProtection4.png",
      category_id: 11,
      project_fee: 5500,
    },
    {
      freelancer_id: 5,
      content: "I will design schematics, pcb boards, gerber files",
      photo: "ElectronicsEngineering1.png",
      category_id: 12,
      project_fee: 800,
    },
    {
      freelancer_id: 4,
      content: "I will design pcb , schematic circuit, gerber file",
      photo: "ElectronicsEngineering2.jpg",
      category_id: 12,
      project_fee: 700,
    },
    {
      freelancer_id: 8,
      content:
        "I will design professional pcb and schematic using altium or eagle",
      photo: "ElectronicsEngineering3.jpg",
      category_id: 12,
      project_fee: 900,
    },
    {
      freelancer_id: 1,
      content: "I will help you in vhdl and verilog programming",
      photo: "ElectronicsEngineering4.jpg",
      category_id: 12,
      project_fee: 300,
    },
    {
      freelancer_id: 5,
      content: "I will convert, recreate and format PDF to indesign",
      photo: "ConvertFiles1.jpeg",
      category_id: 13,
      project_fee: 500,
    },
    {
      freelancer_id: 4,
      content: "I will create dynamic fillable PDF form using adobe livecycle",
      photo: "ConvertFiles2.jpg",
      category_id: 13,
      project_fee: 700,
    },
    {
      freelancer_id: 6,
      content: "I will manually convert PDF, word to adobe indesign",
      photo: "ConvertFiles3.jpg",
      category_id: 13,
      project_fee: 400,
    },
    {
      freelancer_id: 8,
      content: "I will create, format, and modify ebook for you",
      photo: "ConvertFiles4.jpg",
      category_id: 13,
      project_fee: 300,
    },
    {
      freelancer_id: 9,
      content: "I will test your website, app for bugs, ux, UI experience",
      photo: "UserTesting1.png",
      category_id: 14,
      project_fee: 300,
    },
    {
      freelancer_id: 4,
      content:
        "I will test your website or apps functionality, usability and more",
      photo: "UserTesting2.png",
      category_id: 14,
      project_fee: 500,
    },
    {
      freelancer_id: 8,
      content:
        "I will have 50 users test the experience of website or mobile app",
      photo: "UserTesting3.png",
      category_id: 14,
      project_fee: 700,
    },
    {
      freelancer_id: 10,
      content: "I will user test and review your website, ios or android app",
      photo: "UserTesting4.png",
      category_id: 14,
      project_fee: 500,
    },
    {
      freelancer_id: 5,
      content:
        "I will have a live consultation about your website user experience",
      photo: "QAReview1.jpg",
      category_id: 15,
      project_fee: 600,
    },
    {
      freelancer_id: 8,
      content:
        "I will take your XML content and convert it with xslt, validate with schematron or xsd",
      photo: "QAReview2.jpg",
      category_id: 15,
      project_fee: 700,
    },
    {
      freelancer_id: 2,
      content: "I will do web automation testing using selenium",
      photo: "QAReview3.png",
      category_id: 15,
      project_fee: 500,
    },
    {
      freelancer_id: 6,
      content: "I will do software quality assurance tasks",
      photo: "QAReview4.jpg",
      category_id: 15,
      project_fee: 300,
    },
    {
      freelancer_id: 2,
      content: "I will build nft minting engine and nft staking",
      photo: "Blockchain1.png",
      category_id: 16,
      project_fee: 30000,
    },
    {
      freelancer_id: 3,
      content: "I will do pancake swap fork and pancakeswap clone dex website",
      photo: "Blockchain2.png",
      category_id: 16,
      project_fee: 50000,
    },
    {
      freelancer_id: 4,
      content:
        "I will design and develop stunning nft minting website mint engine,nft marketplace",
      photo: "Blockchain3.png",
      category_id: 16,
      project_fee: 15000,
    },
    {
      freelancer_id: 5,
      content:
        "I will develop complete nft marketplace, nft website, bsc, solana, polygon nft website",
      photo: "Blockchain4.png",
      category_id: 16,
      project_fee: 32000,
    },
    {
      freelancer_id: 5,
      content:
        "I will design sql database, erd, sql queries on oracle, mysql, mssql",
      photo: "Databases1.png",
      category_id: 17,
      project_fee: 1000,
    },
    {
      freelancer_id: 7,
      content:
        "I will develop access database, microsoft access, ms access vba",
      photo: "Databases2.png",
      category_id: 17,
      project_fee: 800,
    },
    {
      freelancer_id: 2,
      content: "I will build a database with ms access",
      photo: "Databases3.png",
      category_id: 17,
      project_fee: 500,
    },
    {
      freelancer_id: 1,
      content: "I will advanced access database development and customization",
      photo: "Databases4.jpg",
      category_id: 17,
      project_fee: 600,
    },
    {
      freelancer_id: 3,
      content:
        "I will program google sheets and excel formulas, macros, vba, apps script",
      photo: "DataProcessing1.jpg",
      category_id: 18,
      project_fee: 400,
    },
    {
      freelancer_id: 5,
      content: "I will automate your workspace with google apps script",
      photo: "DataProcessing2.jpg",
      category_id: 18,
      project_fee: 600,
    },
    {
      freelancer_id: 8,
      content:
        "I will do web scraping, data mining, extraction, scrapers in python",
      photo: "DataProcessing3.jpg",
      category_id: 18,
      project_fee: 800,
    },
    {
      freelancer_id: 10,
      content:
        "I will be your google sheets formula, script, and automation expert",
      photo: "DataProcessing4.jpg",
      category_id: 18,
      project_fee: 300,
    },
    {
      freelancer_id: 4,
      content:
        "I will do PDF data extraction, image ocr, web scraping in python",
      photo: "DataEngineering1.jpeg",
      category_id: 19,
      project_fee: 500,
    },
    {
      freelancer_id: 2,
      content:
        "I will nft collections from opensea data mining scraping floor price",
      photo: "DataEngineering2.jpeg",
      category_id: 19,
      project_fee: 300,
    },
    {
      freelancer_id: 8,
      content: "I will do your python work, scripts, dataanalysis, csv, api",
      photo: "DataEngineering3.jpg",
      category_id: 19,
      project_fee: 200,
    },
    {
      freelancer_id: 1,
      content:
        "I will develop data engineering, devops, mlops applications for you",
      photo: "DataEngineering4.jpg",
      category_id: 19,
      project_fee: 400,
    },
    {
      freelancer_id: 1,
      content: "I will help in machine learning and data analysis in rstudio",
      photo: "DataScience1.png",
      category_id: 20,
      project_fee: 400,
    },
    {
      freelancer_id: 4,
      content: "I will model and forecast time series using prophet or arima",
      photo: "DataScience2.jpg",
      category_id: 20,
      project_fee: 300,
    },
    {
      freelancer_id: 5,
      content: "I will create your machine learning web application",
      photo: "DataScience3.jpg",
      category_id: 20,
      project_fee: 700,
    },
    {
      freelancer_id: 10,
      content:
        "I will do computer vision 2d 3d, deep learning, CNN with pytorch ",
      photo: "DataScience4.png",
      category_id: 20,
      project_fee: 800,
    },
  ]);

  await knex("client_jobs_application").insert([
    {
      status: "pending",
      client_jobs_id: 1,
      freelancer_id: 2,
    },
    {
      status: "pending",
      client_jobs_id: 1,
      freelancer_id: 3,
    },
    {
      status: "pending",
      client_jobs_id: 1,
      freelancer_id: 4,
    },
    {
      status: "pending",
      client_jobs_id: 1,
      freelancer_id: 5,
    },
    {
      status: "pending",
      client_jobs_id: 2,
      freelancer_id: 1,
    },
    {
      status: "pending",
      client_jobs_id: 2,
      freelancer_id: 3,
    },
    {
      status: "pending",
      client_jobs_id: 2,
      freelancer_id: 4,
    },
    {
      status: "pending",
      client_jobs_id: 2,
      freelancer_id: 5,
    },
    {
      status: "pending",
      client_jobs_id: 2,
      freelancer_id: 6,
    },
    {
      status: "pending",
      client_jobs_id: 3,
      freelancer_id: 1,
    },
    {
      status: "pending",
      client_jobs_id: 3,
      freelancer_id: 2,
    },
    {
      status: "pending",
      client_jobs_id: 3,
      freelancer_id: 4,
    },
    {
      status: "pending",
      client_jobs_id: 3,
      freelancer_id: 5,
    },
    {
      status: "pending",
      client_jobs_id: 3,
      freelancer_id: 6,
    },
    {
      status: "pending",
      client_jobs_id: 3,
      freelancer_id: 8,
    },
    {
      status: "pending",
      client_jobs_id: 4,
      freelancer_id: 1,
    },
    {
      status: "pending",
      client_jobs_id: 4,
      freelancer_id: 2,
    },
    {
      status: "pending",
      client_jobs_id: 4,
      freelancer_id: 3,
    },
    {
      status: "pending",
      client_jobs_id: 4,
      freelancer_id: 5,
    },
    {
      status: "pending",
      client_jobs_id: 4,
      freelancer_id: 6,
    },
  ]);

  await knex("comments").insert([
    {
      user_id: 1,
      freelancer_id: 2,
      comments: "good UI design",
      ranking: 4,
    },
    {
      user_id: 3,
      freelancer_id: 2,
      comments: "On time and well design",
      ranking: 5,
    },
    {
      user_id: 2,
      freelancer_id: 1,
      comments: "rubbish!",
      ranking: 1,
    },
    {
      user_id: 1,
      freelancer_id: 3,
      comments: "design is good , but didn’t on time!",
      ranking: 3,
    },
    {
      user_id: 2,
      freelancer_id: 3,
      comments: "nice communication",
      ranking: 4,
    },
  ]);

  await knex("direct_messages").insert([
    {
      from: 1,
      to: 2,
      contents: "Hello",
    },
    {
      from: 2,
      to: 1,
      contents: "what can I help you?",
    },
    {
      from: 3,
      to: 1,
      contents: "Can you finish it on time?",
    },
    {
      from: 1,
      to: 3,
      contents: "I will try my best?",
    },
    {
      from: 4,
      to: 2,
      contents: "Can you show me your UI designs?",
    },
    {
      from: 2,
      to: 4,
      contents: "Sure?",
    },
  ]);
  await knex("freelancer_jobs_application").insert([
    {
      status: "pending",
      freelancer_post_id: 1,
      client_id: 2,
    },
    {
      status: "pending",
      freelancer_post_id: 2,
      client_id: 1,
    },
    {
      status: "pending",
      freelancer_post_id: 3,
      client_id: 1,
    },
    {
      status: "pending",
      freelancer_post_id: 4,
      client_id: 1,
    },
    {
      status: "pending",
      freelancer_post_id: 3,
      client_id: 2,
    },
    {
      status: "pending",
      freelancer_post_id: 3,
      client_id: 4,
    },
    {
      freelancer_post_id: 4,
      client_id: 2,
    },
  ]);

  await knex("client_jobs_bookmark").insert([
    {
      job_id: 1,
      user_id: 2,
    },
    {
      job_id: 1,
      user_id: 3,
    },
    {
      job_id: 2,
      user_id: 3,
    },
    {
      job_id: 2,
      user_id: 1,
    },
    {
      job_id: 3,
      user_id: 2,
    },
    {
      job_id: 3,
      user_id: 1,
    },
  ]);
}
