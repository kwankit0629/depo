"ClientJobsController
"
sendClientPostRequest = async (req: Request, res: Response) => {
try {
const freelancerPostId = parseInt(req.params.freelancerPostId);
const userId = parseInt(req.params.userId);
if (!freelancerPostId) {
res.status(400).json({ msg: "Invalid freelancer post ID" });
}
if (!userId) {
res.status(400).json({ msg: "Invalid userId" });
}
let result = await this.clientJobSService.createFreelancerJobApplication(
freelancerPostId,
userId
);
res.json({ msg: "apply success" });
} catch (err) {
console.log(err, "sendClientPostRequest error");
res.status(500).json({ err });
}
};

freelancerAcceptPostRequest = async (req: Request, res: Response) => {
try {
const freelancerJobApplicationId = parseInt(
req.params.freelancerJobApplicationId
);
await this.clientJobSService.freelancerAcceptMatching(
freelancerJobApplicationId
);
res.json({ msg: "accept success" });
} catch (err) {
console.log(err, "freelancerAcceptPostRequest error");
res.status(500).json({ err });
}
};

"clientJobService"

// sendClientPostRequest
async createFreelancerJobApplication(
freelancerPostId: number,
userId: number
) {
return await this.knex("freelancer_jobs_application").insert({
freelancer_post_id: freelancerPostId,
client_id: userId,
status: "pending",
});
}

// freelancerAcceptPostRequest
async freelancerAcceptMatching(freelancerJobApplicationId:number) {
return await this.knex("freelancer_jobs_application")
.where({
id: freelancerJobApplicationId,
}).update({
status:"success"
})  
 }
