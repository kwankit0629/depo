import { Knex } from "knex";
// import { Request, Response } from "express";

export class ClientJobsService {
  constructor(private knex: Knex) {}

  async getAllClientJobs() {
    return await this.knex
      .select("*")
      .from("client_jobs")
      .where("status", "open")
      .orderBy("client_jobs.created_at", "desc");
  }

  async createClientJobs(
    phone_num: string,
    job_name: string,
    job_intro: string,
    job_requirement: string,
    budget: number,
    deadline: number,
    user_id: number | string
  ) {
    console.log({
      phone_num,
      job_name,
      job_intro,
      job_requirement,
      budget,
      deadline,
      user_id,
    });
    return await this.knex
      .insert({
        phone_num,
        job_name,
        job_intro,
        job_requirement,
        budget,
        deadline,
        user_id,
      })
      .into("client_jobs");
  }
  async getClientJobBookmarksById(freelancer_id: number, job_id: number) {
    return await this.knex
      .select("freelancer_id", "job_id")
      .from("jobs_bookmark")
      .where("freelancer_id", freelancer_id)
      .andWhere("job_id", job_id)
      .first();
  }
  async deleteClientJobBookmark(freelancer_id: number, job_id: number) {
    return await this.knex
      .from("jobs_bookmark")
      .where("freelancer_id", freelancer_id)
      .andWhere("job_id", job_id)
      .delete();
  }
  async createClientJobBookmark(freelancer_id: number, job_id: number) {
    return await this.knex
      .insert({
        freelancer_id,
        job_id,
      })
      .into("jobs_bookmark");
  }

  async applyClientJobs(client_jobs_id: number, freelancer_id: number) {
    console.log(
      "Applying client jobs service!!!!",
      client_jobs_id,
      freelancer_id
    );
    return await this.knex
      .insert({
        client_jobs_id,
        freelancer_id,
      })
      .into("client_jobs_application");
  }

  async getClientJobsById(user_id: number) {
    return await this.knex
      .select("*")
      .from("client_jobs")
      .where("user_id", user_id)
      .orderBy("client_jobs.created_at", "desc");
  }

  async getAllClientJobsApplicationsInfo(job_id: number) {
    const query = this.knex
      .select(
        "users.name as freelancer_name",
        "freelancers.id as freelancer_id",
        "freelancers.age as freelancer_age",
        "freelancers.email as freelancer_email",
        "freelancers.phone_num as freelancer_phone",
        "freelancers.photo as freelancer_photo",
        "freelancers.country as freelancer_country",
        "freelancers.experience as freelancer_experience",
        "freelancers.introduction as freelancer_introduction",
        "freelancers.technology_sum as freelancer_technology_sum",
        "freelancers.ref_link as freelancer_ref_link",
        "client_jobs_application.status as application_status",
        "client_jobs_application.client_jobs_id as job_id"
      )
      .from("freelancers")
      .leftJoin(
        "client_jobs_application",
        "freelancers.id",
        "client_jobs_application.freelancer_id"
      )
      .leftJoin("users", "user_id", "users.id")
      .where("client_jobs_application.client_jobs_id", job_id)
      .whereIn("client_jobs_application.status", ["pending", "Success"]);
    // .andWhere((q) => {
    //   q.where("client_jobs_application.status", "pending").orWhere(
    //     "client_jobs_application.status",
    //     "Success"
    //   );
    // });

    console.log("begin ");
    console.log(query.toSQL());
    console.log("print end");
    return await query;
  }
  async updateClientJobsAssignee(job_id: number, assignee: number) {
    return await this.knex("client_jobs")
      .update({
        assignee,
        status: "closed",
      })
      .where("id", job_id);
  }

  async updateClientJobsStatus(job_id: number) {
    return await this.knex("client_jobs_application")
      .update({ status: "Success" })
      .where("client_jobs_id", job_id);
  }

  async rejectOtherClientJobsAssignee(job_id: number, freelancer_id: number) {
    return await this.knex("client_jobs_application")
      .where("client_jobs_id", job_id)
      .whereNot("freelancer_id", freelancer_id)
      .update({ status: "Rejected" });
  }

  async deleteClientJobsAssignee(job_id: number, freelancer_id: number) {
    return await this.knex("client_jobs_application")
      .where("client_jobs_id", job_id)
      .andWhere("freelancer_id", freelancer_id)
      .update({ status: "Rejected" });
  }

  async deleteAppliedClientJobs(job_id: number, freelancer_id: number) {
    return await this.knex("client_jobs_application")
      .where("client_jobs_id", job_id)
      .andWhere("freelancer_id", freelancer_id)
      .del();
  }

  async getAllAppliedClientJobsInfo(freelancer_id: number) {
    return await this.knex
      .select(
        "client_jobs.job_name as client_job_name",
        "client_jobs.job_intro as client_job_intro",
        "client_jobs.phone_num as client_job_phone",
        "client_jobs.job_requirement as client_job_requirement",
        "client_jobs.budget as client_job_budget",
        "client_jobs.id as client_job_id",
        "client_jobs.deadline as client_job_deadline",
        "client_jobs.status as client_job_status",
        "client_jobs_application.status as application_status",
        "client_jobs.assignee as client_job_assignee"
      )
      .from("client_jobs")
      .leftJoin(
        "client_jobs_application",
        "client_jobs.id",
        "client_jobs_application.client_jobs_id"
      )
      .leftJoin(
        "freelancers",
        "client_jobs_application.freelancer_id",
        "freelancers.id"
      )
      .where("client_jobs_application.freelancer_id", freelancer_id);
  }
}
