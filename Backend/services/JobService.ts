import { Knex } from "knex";

export class JobService {
  constructor(private knex: Knex) {}

  async getAllFreelancerPost() {
    // return await this.knex.select("*").from("freelancer_posts");

    return await this.knex
      .select(
        "freelancer_posts.id as posts_id",
        "freelancer_posts.content",
        "freelancer_posts.photo as postsImage",
        "freelancers.user_id",
        "users.name",
        "freelancers.photo",
        "freelancer_posts.freelancer_id",
        "freelancer_posts.project_fee",
        "freelancer_posts.created_at",
        "freelancer_posts.updated_at ",
        "freelancer_posts.category_id",
        "category.name as category_name"
      )
      .from("freelancer_posts")
      .leftJoin("freelancers", "freelancer_id", "freelancers.id")
      .leftJoin("users", "freelancers.user_id", "users.id")
      .leftJoin("category", "freelancer_posts.category_id", "category.id")
      .orderBy("posts_id", "desc");
  }
  async getAllFreelancerPostById(posts_id: number) {
    // return await this.knex.select("*").from("freelancer_posts");

    return await this.knex
      .select(
        "freelancer_posts.id",
        "freelancer_posts.content",
        "freelancer_posts.photo as postsImage",
        "freelancers.user_id",
        "freelancers.experience",
        "freelancers.phone_num",
        "freelancers.photo",
        "freelancers.country",
        "freelancers.ref_link",
        "freelancers.introduction",
        "freelancers.technology_sum",
        "freelancers.created_at as createAccount",
        "users.name",
        "users.email",
        "freelancer_posts.freelancer_id",
        "freelancer_posts.project_fee",
        "freelancer_posts.created_at",
        "freelancer_posts.updated_at ",
        "freelancer_posts.category_id",
        "category.name as category_name"
      )
      .from("freelancer_posts")
      .leftJoin("freelancers", "freelancer_id", "freelancers.id")
      .leftJoin("users", "freelancers.user_id", "users.id")
      .leftJoin("category", "freelancer_posts.category_id", "category.id")
      .where("freelancer_posts.id", posts_id);
  }
  async createFreelancerJobs(
    content: string,
    photo: string,
    category_id: number | string,
    project_fee: number | string,
    freelancer_id: number | string
  ) {
    return await this.knex
      .insert({
        content,
        photo,
        category_id,
        project_fee,
        freelancer_id,
      })
      .into("freelancer_posts");
  }

  async getFreelancerPostByFreelancerId(freelancer_id: number) {
    return await this.knex
      .select(
        "freelancer_posts.id",
        "freelancer_posts.content",
        "freelancer_posts.photo as postsImage",
        "freelancers.user_id",
        "freelancers.experience",
        "freelancers.phone_num",
        "freelancers.photo",
        "freelancers.country",
        "freelancers.ref_link",
        "freelancers.introduction",
        "freelancers.technology_sum",
        "freelancers.created_at as createAccount",
        "users.name",
        "users.email",
        "freelancer_posts.freelancer_id",
        "freelancer_posts.project_fee",
        "freelancer_posts.created_at",
        "freelancer_posts.updated_at ",
        "freelancer_posts.category_id",
        "category.name as category_name"
      )
      .from("freelancer_posts")
      .leftJoin("freelancers", "freelancer_id", "freelancers.id")
      .leftJoin("users", "freelancers.user_id", "users.id")
      .leftJoin("category", "freelancer_posts.category_id", "category.id")
      .where("freelancer_posts.freelancer_id", freelancer_id);
  }
}
