import { Knex } from "knex";

export class FreelancerService {
  constructor(private knex: Knex) {}

  async getAllFreelancer() {
    return await this.knex.select("*").from("freelancers");
  }

  async getFreelancerbyId(userId: number | string) {
    console.log("Freelancer ID!!!", userId);
    return await this.knex
      .select("*")
      .from("freelancers")
      .where("user_id", userId)
      .first();
  }
  async getAllFreelancerPosts() {
    return await this.knex
      .select("*")
      .from("freelancer_posts")
      .orderBy("freelancer_posts.created_at", "desc");
  }

  async getIndividualFreelancerPost(freelancerId: number | string) {
    return await this.knex
      .select("*")
      .from("freelancer_posts")
      .where("id", freelancerId)
      .orderBy("freelancer_posts.created_at", "desc");
  }

  async getOtherFreelancerPosts(OtherId: number | string) {
    return await this.knex
      .select("*")
      .from("freelancer_posts")
      .where("id", OtherId)
      .orderBy("freelancer_posts.created_at", "desc");
  }

  async regToBeAFreelancer(
    age: number,
    email: string,
    phone_num: string,
    photo: string,
    country: string,
    experience: string,
    introduction: string,
    technology_sum: string,
    ref_link: string,
    user_id: number | string
  ) {
    console.log(
      "Insert into Freelancer!!!testing",
      age,
      email,
      phone_num,
      photo,
      country,
      experience,
      introduction,
      technology_sum,
      ref_link,
      user_id
    );

    return await this.knex
      .insert({
        age,
        email,
        phone_num,
        photo,
        country,
        experience,
        introduction,
        technology_sum,
        ref_link,
        user_id,
      })
      .into("freelancers");
  }
}
