import { Knex } from "knex";
import { checkPassword, hashPassword } from "../util/hash";
import crypto from "crypto";
import { User } from "../util/models";
export class UserService {
  static getUserById(userId: any) {
    console.log("userId :", userId);
    throw new Error("Method not implemented.");
  }
  constructor(private knex: Knex) {}

  async login(name: string, password: string) {
    let foundUser = await this.getUserByName(name);
    // console.log('email = ', email);

    console.log("foundUser = ", foundUser);

    if (!foundUser) {
      throw new Error("Email incorrect");
    }
    let isPasswordValid = await checkPassword(password, foundUser.password);
    if (isPasswordValid) {
      return foundUser;
    } else {
      throw new Error("Password incorrect");
    }
  }

  // get username start //
  async getUserById(id: number) {
    const user = await this.knex<User>("users").where({ id }).first();
    return user;
  }

  // for testing insomnia //
  async getAllUsers() {
    return await this.knex.select("*").from("users");
  }

  async getUserByName(name: string) {
    return await this.knex
      .select("*")
      .from("users")
      .where("name", name)
      .first();
  }

  async getUserByEmailOrName(name: string, email: string) {
    const user = await this.knex
      .select("*")
      .from("users")
      .where("email", email)
      .orWhere("name", name)
      .first();
    return user;
  }

  async createUser(name: string, email: string, rawPassword: string | null) {
    let hashedPw = !rawPassword
      ? crypto.randomBytes(20).toString("hex")
      : await hashPassword(rawPassword);
    const user = await this.knex
      .insert({
        name,
        email,
        password: hashedPw,
      })
      .into("users")
      .returning("*");
    return user;
  }

  async createPost(photo: string, content: string, dogs_id: number | string) {
    return await this.knex
      .insert({
        photo,
        content,
        dogs_id,
      })
      .into("posts");
  }
  async createJobBookmark(user_id: number, job_id: number) {
    return await this.knex
      .insert({
        user_id,
        job_id,
      })
      .into("client_jobs_bookmark");
  }

  async getJobBookmarksById(user_id: number, job_id: number) {
    return await this.knex
      .select("user_id", "job_id")
      .from("client_jobs_bookmark")
      .where("user_id", user_id)
      .andWhere("job_id", job_id)
      .first();
  }
  async deleteJobBookmark(user_id: number, job_id: number) {
    return await this.knex
      .from("client_jobs_bookmark")
      .where("user_id", user_id)
      .andWhere("job_id", job_id)
      .delete();
  }

  async getFreelancerBookmarkedPost(user_id: number | string) {
    return await this.knex
      .select(
        "freelancer_posts.id as posts_id",
        "freelancer_posts.content",
        "freelancer_posts.photo as postsImage",
        "freelancers.user_id",
        "users.name",
        "freelancers.photo",
        "freelancer_posts.freelancer_id",
        "freelancer_posts.project_fee",
        "freelancer_posts.created_at",
        "freelancer_posts.updated_at ",
        "freelancer_posts.category_id",
        "category.name as category_name",
        "client_jobs_bookmark.user_id as userBookmarked"
      )
      .from("freelancer_posts")
      .leftJoin("freelancers", "freelancer_id", "freelancers.id")
      .leftJoin("users", "freelancers.user_id", "users.id")
      .leftJoin("category", "freelancer_posts.category_id", "category.id")
      .leftJoin(
        "client_jobs_bookmark",
        "freelancer_posts.id",
        "client_jobs_bookmark.job_id"
      )
      .where("client_jobs_bookmark.user_id", user_id);
  }

  async getClientBookmarkedPost(freelancer_id: number | string) {
    return await this.knex
      .select(
        "client_jobs.id",
        "client_jobs.phone_num",
        "client_jobs.job_name",
        "client_jobs.job_intro",
        "client_jobs.job_requirement",
        "client_jobs.budget",
        "client_jobs.deadline",
        "client_jobs.user_id",
        "users.name",
        "jobs_bookmark.job_id",
        "jobs_bookmark.freelancer_id"
      )
      .from("client_jobs")
      .leftJoin("jobs_bookmark", "client_jobs.id", "jobs_bookmark.job_id")
      .leftJoin("users", "users.id", "client_jobs.user_id")
      .where("jobs_bookmark.freelancer_id", freelancer_id);
  }
}
