import { Knex } from "knex";

export class CategoryService {
  constructor(private knex: Knex) {}

  async getAllCategory() {
    return await this.knex.select("*").from("category");
  }
  async getFreelancerJobsByCatId(catId: number | string) {
    return await this.knex
      .select(
        "freelancer_posts.id as posts_id",
        "freelancer_posts.content",
        "freelancer_posts.photo as postsImage",
        "freelancers.user_id",
        "users.name",
        "freelancers.photo",
        "freelancer_posts.freelancer_id",
        "freelancer_posts.project_fee",
        "freelancer_posts.created_at",
        "freelancer_posts.updated_at ",
        "freelancer_posts.category_id",
        "category.name as category_name"
        // "client_jobs_bookmark.user_id as userBookmarked"
      )
      .from("freelancer_posts")
      .leftJoin("freelancers", "freelancer_id", "freelancers.id")
      .leftJoin("users", "freelancers.user_id", "users.id")
      .leftJoin("category", "freelancer_posts.category_id", "category.id")
      // .leftJoin(
      //   "client_jobs_bookmark",
      //   "freelancer_posts.id",
      //   "client_jobs_bookmark.job_id"
      // )
      .where("freelancer_posts.category_id", catId);
  }

  async getCategoryById(catId: number | string) {
    return await this.knex.select("*").from("category").where("id", catId);
  }
}
