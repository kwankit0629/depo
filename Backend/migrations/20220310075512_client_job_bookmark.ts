import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  const hasTable = await knex.schema.hasTable("client_jobs_bookmark");
  if (!hasTable) {
    await knex.schema.createTable("client_jobs_bookmark", (table) => {
      table.increments();
      table.integer("job_id").unsigned();
      table.foreign("job_id").references("freelancer_posts.id");
      table.integer("user_id").unsigned();
      table.foreign("user_id").references("users.id");
      table.timestamps(false, true);
    });
  }
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists("client_jobs_bookmark");
}
