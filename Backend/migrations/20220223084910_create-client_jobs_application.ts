import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  const hasTable = await knex.schema.hasTable("client_jobs_application");
  if (!hasTable) {
    await knex.schema.createTable("client_jobs_application", (table) => {
      table.increments();
      table.string("status").defaultTo("pending");
      table.integer("client_jobs_id").unsigned();
      table.foreign("client_jobs_id").references("client_jobs.id");
      table.integer("freelancer_id").unsigned();
      table.foreign("freelancer_id").references("freelancers.id");
      table.timestamps(false, true);
    });
  }
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists("client_jobs_application");
}
