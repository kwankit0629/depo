import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  const hasTable = await knex.schema.hasTable("user_bookmark");
  if (!hasTable) {
    await knex.schema.createTable("user_bookmark", (table) => {
      table.increments();
      table.integer("users_id").unsigned();
      table.foreign("users_id").references("users.id");
      table.integer("freelancer_id").unsigned();
      table.foreign("freelancer_id").references("freelancers.id");
      table.timestamps(false, true);
    });
  }
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists("user_bookmark");
}
