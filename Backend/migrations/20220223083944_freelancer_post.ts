import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  const hasTable = await knex.schema.hasTable("freelancer_posts");
  if (!hasTable) {
    await knex.schema.createTable("freelancer_posts", (table) => {
      table.increments();
      table.string("content").notNullable();
      table.string("photo");
      table.integer("category_id").unsigned();
      table.foreign("category_id").references("category.id");
      table.integer("freelancer_id").unsigned();
      table.foreign("freelancer_id").references("freelancers.id");
      table.integer("project_fee").notNullable();
      table.timestamps(false, true);
    });
  }
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists("freelancer_posts");
}
