import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  const hasTable = await knex.schema.hasTable("direct_messages");
  if (!hasTable) {
    await knex.schema.createTable("direct_messages", (table) => {
      table.increments();
      table.integer("from").unsigned();
      table.foreign("from").references("users.id");
      table.integer("to").unsigned();
      table.foreign("to").references("users.id");
      table.string("contents").notNullable;
      table.timestamps(false, true);
    });
  }
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists("direct_messages");
}
