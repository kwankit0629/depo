import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  const hasTable = await knex.schema.hasTable("category");
  if (!hasTable) {
    await knex.schema.createTable("category", (table) => {
      table.increments();
      table.string("name").notNullable();
      table.string("photo").notNullable();
      table.timestamps(false, true);
    });
  }
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists("category");
}
