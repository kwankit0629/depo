import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  const hasTable = await knex.schema.hasTable("client_jobs");
  if (!hasTable) {
    await knex.schema.createTable("client_jobs", (table) => {
      table.increments();
      table.string("phone_num").notNullable();
      table.string("job_name").notNullable();
      table.text("job_intro").notNullable();
      table.text("job_requirement").notNullable;
      table.integer("budget").notNullable();
      table.integer("deadline").notNullable();
      table.string("status").defaultTo("open");
      table.integer("assignee").unsigned().nullable();
      table.foreign("assignee").references("freelancers.id");
      table.integer("user_id").unsigned();
      table.foreign("user_id").references("users.id");
      table.timestamps(false, true);
    });
  }
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists("client_jobs");
}
