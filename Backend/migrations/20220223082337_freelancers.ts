import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  const hasTable = await knex.schema.hasTable("freelancers");
  if (!hasTable) {
    await knex.schema.createTable("freelancers", (table) => {
      table.increments();
      table.integer("age");
      table.string("email");
      table.string("phone_num");
      table.string("photo").notNullable();
      table.string("country");
      table.string("experience").notNullable();
      table.string("introduction").notNullable();
      table.string("technology_sum").notNullable();
      table.string("ref_link");
      table.integer("user_id").unsigned();
      table.foreign("user_id").references("users.id");
      table.timestamps(false, true);
    });
  }
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists("freelancers");
}
