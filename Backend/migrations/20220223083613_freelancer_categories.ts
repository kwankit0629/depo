import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  const hasTable = await knex.schema.hasTable("freelancer_category");
  if (!hasTable) {
    await knex.schema.createTable("freelancer_category", (table) => {
      table.increments();
      table.integer("category_id").unsigned();
      table.foreign("category_id").references("category.id");
      table.integer("freelancer_id").unsigned();
      table.foreign("freelancer_id").references("freelancers.id");
      table.timestamps(false, true);
    });
  }
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists("freelancer_category");
}
