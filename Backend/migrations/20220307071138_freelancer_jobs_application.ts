import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  const hasTable = await knex.schema.hasTable("freelancer_jobs_application");
  if (!hasTable) {
    await knex.schema.createTable("freelancer_jobs_application", (table) => {
      table.increments();
      table.string("status").defaultTo("pending");
      table.integer("freelancer_post_id").unsigned();
      table.foreign("freelancer_post_id").references("freelancer_posts.id");
      table.integer("client_id").unsigned();
      table.foreign("client_id").references("users.id");
      table.timestamps(false, true);
    });
  }
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists("freelancer_jobs_application");
}
