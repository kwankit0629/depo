import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  const hasTable = await knex.schema.hasTable("jobs_bookmark");
  if (!hasTable) {
    await knex.schema.createTable("jobs_bookmark", (table) => {
      table.increments();
      table.integer("job_id").unsigned();
      table.foreign("job_id").references("client_jobs.id");
      table.integer("freelancer_id").unsigned();
      table.foreign("freelancer_id").references("freelancers.id");
      table.timestamps(false, true);
    });
  }
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists("jobs_bookmark");
}
