import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  const hasTable = await knex.schema.hasTable("comments");
  if (!hasTable) {
    await knex.schema.createTable("comments", (table) => {
      table.increments();
      table.integer("user_id").unsigned();
      table.foreign("user_id").references("users.id");
      table.integer("freelancer_id").unsigned();
      table.foreign("freelancer_id").references("freelancers.id");
      table.integer("ranking");
      table.string("comments").notNullable();
      table.timestamps(false, true);
    });
  }
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists("comments");
}
