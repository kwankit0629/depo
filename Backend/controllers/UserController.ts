import { Request, Response } from "express";
import { UserService } from "../services/UserService";
import fetch from "node-fetch";
import { checkPassword } from "../util/hash";
import { knex } from "../db";
import { FreelancerService } from "../services/freelancerService";

export class UserController {
  constructor(private userService: UserService) {}

  me = async (req: Request, res: Response) => {
    res.json({
      user: req.session["users"],
      freelancer: req.session["freelancers"],
    });
  };
  get = async (req: Request, res: Response) => {
    let user_id = req.session["users"]["id"];
    console.log("session = ", req.session["users"]);
    let userResult = await this.userService.getUserById(user_id);

    res.json(userResult);
  };

  getAllUsers = async (req: Request, res: Response) => {
    let allUsers = await this.userService.getAllUsers();
    res.json(allUsers);
  };

  getUserId = async (req: Request, res: Response) => {
    res.end("getUserId");
  };

  login = async (req: Request, res: Response) => {
    try {
      const { name, password } = req.body;

      if (!name || !password) {
        res.status(400).json({ message: "Invalid input" });
        return;
      }

      let foundUser = await this.userService.login(name, password);
      let freelancerService = new FreelancerService(knex);

      let defaultFreelancer = await freelancerService.getFreelancerbyId(
        foundUser["id"]
      );

      delete foundUser["password"];
      req.session["users"] = foundUser;
      req.session["freelancer"] = defaultFreelancer;
      console.log("login users check :", req.session["users"]);
      console.log("login freelancer check :", req.session["freelancer"]);

      let userInfo = { user: foundUser, freelancer: defaultFreelancer };
      res.json(userInfo);
    } catch (error) {
      console.log(error);
      res.status(400).json({ message: error });
    }
  };

  logout = async (req: Request, res: Response) => {
    try {
      req.session.destroy(() => {});
      res.json({ message: "Successfully logout" });
    } catch (error) {
      console.log(error);
      res.json({ message: "Fail to logout" });
    }
  };

  // loginGoogle = async (req: Request, res: Response) => {
  //   const accessToken = req.session?.["grant"].response.access_token;

  //   console.log("accessToken = ", accessToken);

  //   const fetchRes = await fetch(
  //     "https://www.googleapis.com/oauth2/v2/userinfo",
  //     {
  //       method: "get",
  //       headers: {
  //         Authorization: `Bearer ${accessToken}`,
  //       },
  //     }
  //   );

  //   const googleUserInfo = await fetchRes.json();
  //   const user = await this.userService.getUserByEmail(googleUserInfo.email);

  //   // 如果DB 係無 google login 既果個user,  幫佢開一隻record
  //   if (!user) {
  //     await this.userService.createUser(
  //       googleUserInfo.name,
  //       googleUserInfo.email,
  //       null
  //     );
  //   }
  //   if (req.session) {
  //     req.session["user"] = googleUserInfo;
  //   }
  //   return res.redirect("/photowall.html");
  // };
  // register logic

  createUser = async (req: Request, res: Response) => {
    // const file = req.file;
    // let filename = file ? file.filename : "";
    console.log("creating user");
    const { name, email, password } = req.body;
    // console.log(name, email, password);
    // console.log(req.body);
    let result = await this.userService.getUserByEmailOrName(name, email);
    console.log(result);
    let foundUser = result;
    if (foundUser) {
      if (foundUser.name === name) {
        res.json({
          success: false,
          message: "This username already exists, please use another username",
        });
        return;
      }
      if (foundUser.email === email) {
        res.json({
          success: false,
          message: "This email already exists, please use another email",
        });
        return;
      }
    }

    let newUser = await this.userService.createUser(name, email, password);
    delete newUser["password"];
    // delete foundUser.password;
    req.session["users"] = newUser;
    res.json({
      success: true,
      message: "Create successful",
    });
  };

  createPost = async (req: Request, res: Response) => {
    const users_id = parseInt(req.session["users"]["id"]);
    const image = req.file?.filename;
    const content = req.body.content;
    let createPostResult = await this.userService.createPost(
      image!,
      content,
      users_id
    );
    res.json(createPostResult);
  };

  createJobBookmark = async (req: Request, res: Response) => {
    try {
      const user_id = parseInt(req.body.user_id);
      const job_id = parseInt(req.body.posts_id);

      let checkBookmarkResult = await this.userService.getJobBookmarksById(
        user_id,
        job_id
      );

      // Add bookmark record
      if (!checkBookmarkResult) {
        await this.userService.createJobBookmark(user_id, job_id);
        res.json("like Success");
        return;
      }

      // Remove bookmark record
      // await this.userService.deleteJobBookmark(user_id, job_id);
      // res.json("deleted");
    } catch (err) {
      console.log(err);
    }
  };

  removeJobBookmark = async (req: Request, res: Response) => {
    try {
      const user_id = parseInt(req.body.user_id);
      const job_id = parseInt(req.body.posts_id);
      let checkBookmarkResult = await this.userService.getJobBookmarksById(
        user_id,
        job_id
      );
      if (checkBookmarkResult) {
        await this.userService.deleteJobBookmark(user_id, job_id);
        res.json("deleted");
        console.log("delete success");
      }
    } catch (error) {
      console.log("!!!!", error);
    }
  };

  getFreelancerBookmarkedPost = async (req: Request, res: Response) => {
    try {
      const { user_id } = req.params;
      if (!user_id) {
        res.status(400).json({ msg: "Invalid user_id" });
        return;
      }
      let result = await this.userService.getFreelancerBookmarkedPost(
        Number(user_id)
      );
      res.json(result);
    } catch (err) {
      console.log(err);
    }
  };

  getClientBookmarkedPost = async (req: Request, res: Response) => {
    try {
      const { freelancer_id } = req.params;
      if (!freelancer_id) {
        res.status(400).json({ msg: "Invalid user_id" });
        return;
      }
      let result = await this.userService.getClientBookmarkedPost(
        Number(freelancer_id)
      );
      res.json(result);
    } catch (err) {
      console.log(err);
    }
  };
}
