import { Request, Response } from "express";
import fetch from "node-fetch";
import { JobService } from "../services/JobService";

export class JobController {
  constructor(private jobService: JobService) {}

  getAllFreelancerPost = async (req: Request, res: Response) => {
    let allJobs = await this.jobService.getAllFreelancerPost();
    res.json(allJobs);
  };

  getFreelancerPostById = async (req: Request, res: Response) => {
    try {
      const posts_id = parseInt(req.params.id);
      let post = await this.jobService.getAllFreelancerPostById(posts_id);
      console.log(post);
      res.json(post);
    } catch (error) {
      console.log(error);
    }
  };
  createFreelancerJobs = async (req: Request, res: Response) => {
    try {
      const freelancer_id = parseInt(req.body.freelancer_id);
      const { content, category_id, project_fee } = req.body;
      const photo = req.file?.filename;
      let createJobResult = await this.jobService.createFreelancerJobs(
        content,
        photo!,
        category_id,
        project_fee,
        freelancer_id
      );
      res.json(createJobResult);
    } catch (error) {
      console.log(error);
    }
  };
  getFreelancerPostByFreelancerId = async (req: Request, res: Response) => {
    try {
      const freelancer_id = parseInt(req.params.id);
      let result = await this.jobService.getFreelancerPostByFreelancerId(
        freelancer_id
      );
      res.json(result);
    } catch (error) {
      console.log(error);
    }
  };
}
