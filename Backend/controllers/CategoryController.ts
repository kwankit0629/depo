import { Request, Response } from "express";
import fetch from "node-fetch";
import { CategoryService } from "../services/categoryService";

export class CategoryController {
  constructor(private categoryService: CategoryService) {}

  getAllCategory = async (req: Request, res: Response) => {
    try {
      let allCategory = await this.categoryService.getAllCategory();
      res.json(allCategory);
    } catch (error) {
      console.log(error);
    }
  };
  getFreelancerPostByCat = async (req: Request, res: Response) => {
    try {
      const catId = parseInt(req.params.id);
      let jobs = await this.categoryService.getFreelancerJobsByCatId(catId);

      res.json(jobs);
      console.log(jobs);
    } catch (error) {
      console.log(error);
    }
  };

  getCatById = async (req: Request, res: Response) => {
    try {
      const catId = parseInt(req.params.id);
      let cat = await this.categoryService.getCategoryById(catId);

      res.json(cat);
    } catch (error) {
      console.log(error);
    }
  };
}
