import { Request, Response } from "express";
import { ClientJobsService } from "../services/clientJobsService";

export class ClientJobsController {
  constructor(private clientJobSService: ClientJobsService) {}

  getAllJobs = async (req: Request, res: Response) => {
    try {
      let allClientJobs = await this.clientJobSService.getAllClientJobs();
      res.json(allClientJobs);
    } catch (error) {
      console.log(error);
    }
  };

  createClientJobs = async (req: Request, res: Response) => {
    const user_id = req.body.user_id;
    const phone_num = req.body.phone_num;
    const job_name = req.body.job_name;
    const job_intro = req.body.job_intro;
    const job_requirement = req.body.job_requirement;
    const budget = parseInt(req.body.budget);
    const deadline = parseInt(req.body.deadline);

    try {
      let createJobResult = await this.clientJobSService.createClientJobs(
        phone_num,
        job_name,
        job_intro,
        job_requirement,
        budget,
        deadline,
        user_id
      );
      res.json(createJobResult);
    } catch (error) {
      console.log(error);
    }
  };

  createClientJobBookmark = async (req: Request, res: Response) => {
    try {
      const freelancer_id = parseInt(req.body.freelancer_id);
      const job_id = parseInt(req.body.id);

      let bookmarkResult =
        await this.clientJobSService.getClientJobBookmarksById(
          freelancer_id,
          job_id
        );

      if (!bookmarkResult) {
        await this.clientJobSService.createClientJobBookmark(
          freelancer_id,
          job_id
        );
        res.json("like Success");
        return;
      }
      // await this.clientJobSService.deleteClientJobBookmark(
      //   freelancer_id,
      //   job_id
      // );
      // res.json("deleted");
    } catch (error) {
      console.log(error);
    }
  };

  removeClientJobBookmark = async (req: Request, res: Response) => {
    try {
      const freelancer_id = parseInt(req.body.freelancer_id);
      const job_id = parseInt(req.body.clientJobId);

      console.log("freelancer_id=", freelancer_id);
      console.log("job_id=", job_id);

      let checkBookmarkResult =
        await this.clientJobSService.getClientJobBookmarksById(
          freelancer_id,
          job_id
        );
      if (checkBookmarkResult) {
        await this.clientJobSService.deleteClientJobBookmark(
          freelancer_id,
          job_id
        );
        res.json("deleted");
        console.log("delete success");
      }
    } catch (error) {
      console.log("!!!!!!abc", error);
    }
  };

  applyCLientJobs = async (req: Request, res: Response) => {
    const client_jobs_id = parseInt(req.body.client_jobs_id);
    const freelancer_id = parseInt(req.body.freelancer_id);

    try {
      let application = await this.clientJobSService.applyClientJobs(
        client_jobs_id,
        freelancer_id
      );
      res.json(application);
    } catch (error) {
      console.log(error);
    }
  };

  getClientJobsById = async (req: Request, res: Response) => {
    const { userId } = req.params;
    if (!userId) {
      res.status(400).json({ msg: "Invalid userId" });
      return;
    }
    console.log("getClientJobsById userId = ", userId);
    const result = await this.clientJobSService.getClientJobsById(
      Number(userId)
    );
    res.json(result);
  };

  getAllClientJobsApplicationsInfoById = async (
    req: Request,
    res: Response
  ) => {
    try {
      const jobId = parseInt(req.params.id);
      console.log("application controller id: ", jobId);
      let jobs = await this.clientJobSService.getAllClientJobsApplicationsInfo(
        jobId
      );
      res.json(jobs);
    } catch (error) {
      console.log(error);
    }
  };

  updateClientJobsAssignee = async (req: Request, res: Response) => {
    try {
      const client_jobs_id = parseInt(req.body.client_jobs_id);
      const freelancer_id = parseInt(req.body.freelancer_id);
      let assignee = await this.clientJobSService.updateClientJobsAssignee(
        client_jobs_id,
        freelancer_id
      );
      res.json(assignee);
    } catch (error) {
      console.log(error);
    }
  };

  deleteClientJobsAssignee = async (req: Request, res: Response) => {
    try {
      const client_jobs_id = parseInt(req.body.client_jobs_id);
      const freelancer_id = parseInt(req.body.freelancer_id);
      let assignee = await this.clientJobSService.deleteClientJobsAssignee(
        client_jobs_id,
        freelancer_id
      );
      res.json(assignee);
    } catch (error) {
      console.log(error);
    }
  };

  updateClientJobsStatus = async (req: Request, res: Response) => {
    try {
      const client_jobs_id = parseInt(req.body.client_jobs_id);
      let status = await this.clientJobSService.updateClientJobsStatus(
        client_jobs_id
      );
      res.json(status);
    } catch (error) {
      console.log(error);
    }
  };

  rejectOtherClientJobsAssignee = async (req: Request, res: Response) => {
    try {
      const client_jobs_id = parseInt(req.body.client_jobs_id);
      const freelancer_id = parseInt(req.body.freelancer_id);
      let assignee = await this.clientJobSService.rejectOtherClientJobsAssignee(
        client_jobs_id,
        freelancer_id
      );
      res.json(assignee);
    } catch (error) {
      console.log(error);
    }
  };

  getAllAppliedClientJobsInfoById = async (req: Request, res: Response) => {
    try {
      const freelancerId = parseInt(req.params.id);
      console.log("Applied Job controller id: ", freelancerId);
      let jobs = await this.clientJobSService.getAllAppliedClientJobsInfo(
        freelancerId
      );
      res.json(jobs);
    } catch (error) {
      console.log(error);
    }
  };

  deleteAppliedClientJobs = async (req: Request, res: Response) => {
    try {
      const client_jobs_id = parseInt(req.body.client_jobs_id);
      const freelancer_id = parseInt(req.body.freelancer_id);
      let appliedJobs = await this.clientJobSService.deleteAppliedClientJobs(
        client_jobs_id,
        freelancer_id
      );
      res.json(appliedJobs);
    } catch (error) {
      console.log(error);
    }
  };
}
