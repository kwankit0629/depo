import { Request, Response } from "express";
import fetch from "node-fetch";
import { FreelancerService } from "../services/freelancerService";

export class FreelancerController {
  constructor(private freelancerService: FreelancerService) {}

  getAllFreelancer = async (req: Request, res: Response) => {
    let allFreelancer = await this.freelancerService.getAllFreelancer();
    res.json(allFreelancer);
  };

  getAllFreelancerPosts = async (req: Request, res: Response) => {
    const freelancerPosts =
      await this.freelancerService.getAllFreelancerPosts();
    res.json(freelancerPosts);
  };

  getOwnPosts = async (req: Request, res: Response) => {
    let user_Id = parseInt(req.session["user"]["id"]);
    const ownPosts = await this.freelancerService.getIndividualFreelancerPost(
      user_Id
    );
    res.json(ownPosts);
  };

  getOtherFreelancerPosts = async (req: Request, res: Response) => {
    let user_id = req.params.id;
    const otherPosts = await this.freelancerService.getOtherFreelancerPosts(
      user_id
    );
    res.json(otherPosts);
  };

  getFreelancerById = async (req: Request, res: Response) => {
    const { userId } = req.params;
    const freelancer = await this.freelancerService.getFreelancerbyId(
      Number(userId)
    );
    res.json(freelancer);
  };

  regToBeAFreelancer = async (req: Request, res: Response) => {
    const age = parseInt(req.body.age);
    const email = req.body.email;
    const phone_num = req.body.phone_num;
    const photo = req.file?.filename;
    const country = req.body.country;
    const experience = req.body.experience;
    const introduction = req.body.introduction;
    const technology_sum = req.body.technology_sum;
    const ref_link = req.body.ref_link;
    const user_id = parseInt(req.body.user_id);
    try {
      let registerResult = await this.freelancerService.regToBeAFreelancer(
        age,
        email,
        phone_num,
        photo!,
        country,
        experience,
        introduction,
        technology_sum,
        ref_link,
        user_id
      );
      res.json(registerResult);
    } catch (error) {
      console.log(error);
    }
  };
}
